
/********************************************************************
 *
 * Standard register and bit definitions for the Texas Instruments
 * MSP430 microcontroller.
 *
 * This file supports assembler and C/EC++ development for
 * msp430f5152 devices.
 *
 * Copyright 2007-2011 IAR Systems AB.
 *
 *
 *
 ********************************************************************/

#ifndef __IO430F5152
#define __IO430F5152

#ifdef  __IAR_SYSTEMS_ICC__

#include "intrinsics.h"
#ifndef _SYSTEM_BUILD
#pragma system_include
#endif
#endif

#if (((__TID__ >> 8) & 0x7F) != 0x2b)     /* 0x2b = 43 dec */
#error io430f5152.h file for use with ICC430/A430 only
#endif

#define __MSP430_HAS_MSP430XV2_CPU__  /* Definition to show that it has MSP430XV2 CPU */


#ifdef __IAR_SYSTEMS_ICC__
#pragma language=save
#pragma language=extended


#ifdef __cplusplus
#define __READ    /* not supported */
#else
#define __READ    const
#endif


/*-------------------------------------------------------------------------
 *   Standard Bits
 *-------------------------------------------------------------------------*/

#define BIT0                (0x0001)
#define BIT1                (0x0002)
#define BIT2                (0x0004)
#define BIT3                (0x0008)
#define BIT4                (0x0010)
#define BIT5                (0x0020)
#define BIT6                (0x0040)
#define BIT7                (0x0080)
#define BIT8                (0x0100)
#define BIT9                (0x0200)
#define BITA                (0x0400)
#define BITB                (0x0800)
#define BITC                (0x1000)
#define BITD                (0x2000)
#define BITE                (0x4000)
#define BITF                (0x8000)

/*-------------------------------------------------------------------------
 *   Status register bits
 *-------------------------------------------------------------------------*/

#define C                   (0x0001)
#define Z                   (0x0002)
#define N                   (0x0004)
#define V                   (0x0100)
#define GIE                 (0x0008)
#define CPUOFF              (0x0010)
#define OSCOFF              (0x0020)
#define SCG0                (0x0040)
#define SCG1                (0x0080)

/* Low Power Modes coded with Bits 4-7 in SR */

#define LPM0_bits           (CPUOFF)
#define LPM1_bits           (SCG0+CPUOFF)
#define LPM2_bits           (SCG1+CPUOFF)
#define LPM3_bits           (SCG1+SCG0+CPUOFF)
#define LPM4_bits           (SCG1+SCG0+OSCOFF+CPUOFF)


#define LPM0      __bis_SR_register(LPM0_bits)     /* Enter Low Power Mode 0 */
#define LPM0_EXIT __bic_SR_register_on_exit(LPM0_bits) /* Exit Low Power Mode 0 */
#define LPM1      __bis_SR_register(LPM1_bits)     /* Enter Low Power Mode 1 */
#define LPM1_EXIT __bic_SR_register_on_exit(LPM1_bits) /* Exit Low Power Mode 1 */
#define LPM2      __bis_SR_register(LPM2_bits)     /* Enter Low Power Mode 2 */
#define LPM2_EXIT __bic_SR_register_on_exit(LPM2_bits) /* Exit Low Power Mode 2 */
#define LPM3      __bis_SR_register(LPM3_bits)     /* Enter Low Power Mode 3 */
#define LPM3_EXIT __bic_SR_register_on_exit(LPM3_bits) /* Exit Low Power Mode 3 */
#define LPM4      __bis_SR_register(LPM4_bits)     /* Enter Low Power Mode 4 */
#define LPM4_EXIT __bic_SR_register_on_exit(LPM4_bits) /* Exit Low Power Mode 4 */



/*-------------------------------------------------------------------------
 *   ADC10_A
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short ADC10CTL0;   /* ADC10 Control 0 */

  struct
  {
    unsigned short ADC10SC         : 1; /* ADC10 Start Conversion */
    unsigned short ADC10ENC        : 1; /* ADC10 Enable Conversion */
    unsigned short                : 2;
    unsigned short ADC10ON         : 1; /* ADC10 On/enable */
    unsigned short                : 2;
    unsigned short ADC10MSC        : 1; /* ADC10 Multiple SampleConversion */
    unsigned short ADC10SHT0       : 1; /* ADC10 Sample Hold Select Bit: 0 */
    unsigned short ADC10SHT1       : 1; /* ADC10 Sample Hold Select Bit: 1 */
    unsigned short ADC10SHT2       : 1; /* ADC10 Sample Hold Select Bit: 2 */
    unsigned short ADC10SHT3       : 1; /* ADC10 Sample Hold Select Bit: 3 */
  }ADC10CTL0_bit;
} @0x0740;


enum {
  ADC10SC         = 0x0001,
  ADC10ENC        = 0x0002,
  ADC10ON         = 0x0010,
  ADC10MSC        = 0x0080,
  ADC10SHT0       = 0x0100,
  ADC10SHT1       = 0x0200,
  ADC10SHT2       = 0x0400,
  ADC10SHT3       = 0x0800
};


__no_init volatile union
{
  unsigned short ADC10CTL1;   /* ADC10 Control 1 */

  struct
  {
    unsigned short ADC10BUSY       : 1; /* ADC10 Busy */
    unsigned short ADC10CONSEQ0    : 1; /* ADC10 Conversion Sequence Select 0 */
    unsigned short ADC10CONSEQ1    : 1; /* ADC10 Conversion Sequence Select 1 */
    unsigned short ADC10SSEL0      : 1; /* ADC10 Clock Source Select 0 */
    unsigned short ADC10SSEL1      : 1; /* ADC10 Clock Source Select 1 */
    unsigned short ADC10DIV0       : 1; /* ADC10 Clock Divider Select 0 */
    unsigned short ADC10DIV1       : 1; /* ADC10 Clock Divider Select 1 */
    unsigned short ADC10DIV2       : 1; /* ADC10 Clock Divider Select 2 */
    unsigned short ADC10ISSH       : 1; /* ADC10 Invert Sample Hold Signal */
    unsigned short ADC10SHP        : 1; /* ADC10 Sample/Hold Pulse Mode */
    unsigned short ADC10SHS0       : 1; /* ADC10 Sample/Hold Source 0 */
    unsigned short ADC10SHS1       : 1; /* ADC10 Sample/Hold Source 1 */
  }ADC10CTL1_bit;
} @0x0742;


enum {
  ADC10BUSY       = 0x0001,
  ADC10CONSEQ0    = 0x0002,
  ADC10CONSEQ1    = 0x0004,
  ADC10SSEL0      = 0x0008,
  ADC10SSEL1      = 0x0010,
  ADC10DIV0       = 0x0020,
  ADC10DIV1       = 0x0040,
  ADC10DIV2       = 0x0080,
  ADC10ISSH       = 0x0100,
  ADC10SHP        = 0x0200,
  ADC10SHS0       = 0x0400,
  ADC10SHS1       = 0x0800
};


__no_init volatile union
{
  unsigned short ADC10CTL2;   /* ADC10 Control 2 */

  struct
  {
    unsigned short ADC10REFBURST   : 1; /* ADC10 Reference Burst */
    unsigned short                : 1;
    unsigned short ADC10SR         : 1; /* ADC10 Sampling Rate */
    unsigned short ADC10DF         : 1; /* ADC10 Data Format */
    unsigned short ADC10RES        : 1; /* ADC10 Resolution Bit */
    unsigned short                : 3;
    unsigned short ADC10PDIV0      : 1; /* ADC10 predivider Bit: 0 */
    unsigned short ADC10PDIV1      : 1; /* ADC10 predivider Bit: 1 */
  }ADC10CTL2_bit;
} @0x0744;


enum {
  ADC10REFBURST   = 0x0001,
  ADC10SR         = 0x0004,
  ADC10DF         = 0x0008,
  ADC10RES        = 0x0010,
  ADC10PDIV0      = 0x0100,
  ADC10PDIV1      = 0x0200
};


  /* ADC10 Window Comparator High Threshold */
__no_init volatile unsigned short ADC10LO @ 0x0746;


  /* ADC10 Window Comparator High Threshold */
__no_init volatile unsigned short ADC10HI @ 0x0748;


__no_init volatile union
{
  unsigned short ADC10MCTL0;   /* ADC10 Memory Control 0 */

  struct
  {
    unsigned short ADC10INCH0      : 1; /* ADC10 Input Channel Select Bit 0 */
    unsigned short ADC10INCH1      : 1; /* ADC10 Input Channel Select Bit 1 */
    unsigned short ADC10INCH2      : 1; /* ADC10 Input Channel Select Bit 2 */
    unsigned short ADC10INCH3      : 1; /* ADC10 Input Channel Select Bit 3 */
    unsigned short ADC10SREF0      : 1; /* ADC10 Select Reference Bit 0 */
    unsigned short ADC10SREF1      : 1; /* ADC10 Select Reference Bit 1 */
    unsigned short ADC10SREF2      : 1; /* ADC10 Select Reference Bit 2 */
  }ADC10MCTL0_bit;
} @0x074A;


enum {
  ADC10INCH0      = 0x0001,
  ADC10INCH1      = 0x0002,
  ADC10INCH2      = 0x0004,
  ADC10INCH3      = 0x0008,
  ADC10SREF0      = 0x0010,
  ADC10SREF1      = 0x0020,
  ADC10SREF2      = 0x0040
};


  /* ADC10 Conversion Memory 0 */
__no_init volatile unsigned short ADC10MEM0 @ 0x0752;


__no_init volatile union
{
  unsigned short ADC10IE;   /* ADC10 Interrupt Enable */

  struct
  {
    unsigned short ADC10IE0        : 1; /* ADC10_A Interrupt enable */
    unsigned short ADC10INIE       : 1; /* ADC10_A Interrupt enable for the inside of window of the Window comparator */
    unsigned short ADC10LOIE       : 1; /* ADC10_A Interrupt enable for lower threshold of the Window comparator */
    unsigned short ADC10HIIE       : 1; /* ADC10_A Interrupt enable for upper threshold of the Window comparator */
    unsigned short ADC10OVIE       : 1; /* ADC10_A ADC10MEM overflow Interrupt enable */
    unsigned short ADC10TOVIE      : 1; /* ADC10_A conversion-time-overflow Interrupt enable */
  }ADC10IE_bit;
} @0x075A;


enum {
  ADC10IE0        = 0x0001,
  ADC10INIE       = 0x0002,
  ADC10LOIE       = 0x0004,
  ADC10HIIE       = 0x0008,
  ADC10OVIE       = 0x0010,
  ADC10TOVIE      = 0x0020
};


__no_init volatile union
{
  unsigned short ADC10IFG;   /* ADC10 Interrupt Flag */

  struct
  {
    unsigned short ADC10IFG0       : 1; /* ADC10_A Interrupt Flag */
    unsigned short ADC10INIFG      : 1; /* ADC10_A Interrupt Flag for the inside of window of the Window comparator */
    unsigned short ADC10LOIFG      : 1; /* ADC10_A Interrupt Flag for lower threshold of the Window comparator */
    unsigned short ADC10HIIFG      : 1; /* ADC10_A Interrupt Flag for upper threshold of the Window comparator */
    unsigned short ADC10OVIFG      : 1; /* ADC10_A ADC10MEM overflow Interrupt Flag */
    unsigned short ADC10TOVIFG     : 1; /* ADC10_A conversion-time-overflow Interrupt Flag */
  }ADC10IFG_bit;
} @0x075C;


enum {
  ADC10IFG0       = 0x0001,
  ADC10INIFG      = 0x0002,
  ADC10LOIFG      = 0x0004,
  ADC10HIIFG      = 0x0008,
  ADC10OVIFG      = 0x0010,
  ADC10TOVIFG     = 0x0020
};


  /* ADC10 Interrupt Vector Word */
__no_init volatile unsigned short ADC10IV @ 0x075E;


#define __MSP430_HAS_ADC10_A__        /* Definition to show that Module is available */


/* ADC10CTL0 Control Bits */
#define ADC10SC_L           (0x0001u)  /* ADC10 Start Conversion */
#define ADC10ENC_L          (0x0002u)  /* ADC10 Enable Conversion */
#define ADC10ON_L           (0x0010u)  /* ADC10 On/enable */
#define ADC10MSC_L          (0x0080u)  /* ADC10 Multiple SampleConversion */

/* ADC10CTL0 Control Bits */
#define ADC10SHT0_H         (0x0001u)  /* ADC10 Sample Hold Select Bit: 0 */
#define ADC10SHT1_H         (0x0002u)  /* ADC10 Sample Hold Select Bit: 1 */
#define ADC10SHT2_H         (0x0004u)  /* ADC10 Sample Hold Select Bit: 2 */
#define ADC10SHT3_H         (0x0008u)  /* ADC10 Sample Hold Select Bit: 3 */

#define ADC10SHT_0          (0*0x100u) /* ADC10 Sample Hold Select 0 */
#define ADC10SHT_1          (1*0x100u) /* ADC10 Sample Hold Select 1 */
#define ADC10SHT_2          (2*0x100u) /* ADC10 Sample Hold Select 2 */
#define ADC10SHT_3          (3*0x100u) /* ADC10 Sample Hold Select 3 */
#define ADC10SHT_4          (4*0x100u) /* ADC10 Sample Hold Select 4 */
#define ADC10SHT_5          (5*0x100u) /* ADC10 Sample Hold Select 5 */
#define ADC10SHT_6          (6*0x100u) /* ADC10 Sample Hold Select 6 */
#define ADC10SHT_7          (7*0x100u) /* ADC10 Sample Hold Select 7 */
#define ADC10SHT_8          (8*0x100u) /* ADC10 Sample Hold Select 8 */
#define ADC10SHT_9          (9*0x100u) /* ADC10 Sample Hold Select 9 */
#define ADC10SHT_10         (10*0x100u) /* ADC10 Sample Hold Select 10 */
#define ADC10SHT_11         (11*0x100u) /* ADC10 Sample Hold Select 11 */
#define ADC10SHT_12         (12*0x100u) /* ADC10 Sample Hold Select 12 */
#define ADC10SHT_13         (13*0x100u) /* ADC10 Sample Hold Select 13 */
#define ADC10SHT_14         (14*0x100u) /* ADC10 Sample Hold Select 14 */
#define ADC10SHT_15         (15*0x100u) /* ADC10 Sample Hold Select 15 */

/* ADC10CTL1 Control Bits */
#define ADC10BUSY_L         (0x0001u)    /* ADC10 Busy */
#define ADC10CONSEQ0_L      (0x0002u)    /* ADC10 Conversion Sequence Select 0 */
#define ADC10CONSEQ1_L      (0x0004u)    /* ADC10 Conversion Sequence Select 1 */
#define ADC10SSEL0_L        (0x0008u)    /* ADC10 Clock Source Select 0 */
#define ADC10SSEL1_L        (0x0010u)    /* ADC10 Clock Source Select 1 */
#define ADC10DIV0_L         (0x0020u)    /* ADC10 Clock Divider Select 0 */
#define ADC10DIV1_L         (0x0040u)    /* ADC10 Clock Divider Select 1 */
#define ADC10DIV2_L         (0x0080u)    /* ADC10 Clock Divider Select 2 */

/* ADC10CTL1 Control Bits */
#define ADC10ISSH_H         (0x0001u)    /* ADC10 Invert Sample Hold Signal */
#define ADC10SHP_H          (0x0002u)    /* ADC10 Sample/Hold Pulse Mode */
#define ADC10SHS0_H         (0x0004u)    /* ADC10 Sample/Hold Source 0 */
#define ADC10SHS1_H         (0x0008u)    /* ADC10 Sample/Hold Source 1 */

#define ADC10CONSEQ_0        (0*2u)      /* ADC10 Conversion Sequence Select: 0 */
#define ADC10CONSEQ_1        (1*2u)      /* ADC10 Conversion Sequence Select: 1 */
#define ADC10CONSEQ_2        (2*2u)      /* ADC10 Conversion Sequence Select: 2 */
#define ADC10CONSEQ_3        (3*2u)      /* ADC10 Conversion Sequence Select: 3 */

#define ADC10SSEL_0          (0*8u)      /* ADC10 Clock Source Select: 0 */
#define ADC10SSEL_1          (1*8u)      /* ADC10 Clock Source Select: 1 */
#define ADC10SSEL_2          (2*8u)      /* ADC10 Clock Source Select: 2 */
#define ADC10SSEL_3          (3*8u)      /* ADC10 Clock Source Select: 3 */

#define ADC10DIV_0           (0*0x20u)   /* ADC10 Clock Divider Select: 0 */
#define ADC10DIV_1           (1*0x20u)   /* ADC10 Clock Divider Select: 1 */
#define ADC10DIV_2           (2*0x20u)   /* ADC10 Clock Divider Select: 2 */
#define ADC10DIV_3           (3*0x20u)   /* ADC10 Clock Divider Select: 3 */
#define ADC10DIV_4           (4*0x20u)   /* ADC10 Clock Divider Select: 4 */
#define ADC10DIV_5           (5*0x20u)   /* ADC10 Clock Divider Select: 5 */
#define ADC10DIV_6           (6*0x20u)   /* ADC10 Clock Divider Select: 6 */
#define ADC10DIV_7           (7*0x20u)   /* ADC10 Clock Divider Select: 7 */

#define ADC10SHS_0           (0*0x400u)  /* ADC10 Sample/Hold Source: 0 */
#define ADC10SHS_1           (1*0x400u)  /* ADC10 Sample/Hold Source: 1 */
#define ADC10SHS_2           (2*0x400u)  /* ADC10 Sample/Hold Source: 2 */
#define ADC10SHS_3           (3*0x400u)  /* ADC10 Sample/Hold Source: 3 */

/* ADC10CTL2 Control Bits */
#define ADC10REFBURST_L     (0x0001u)    /* ADC10 Reference Burst */
#define ADC10SR_L           (0x0004u)    /* ADC10 Sampling Rate */
#define ADC10DF_L           (0x0008u)    /* ADC10 Data Format */
#define ADC10RES_L          (0x0010u)    /* ADC10 Resolution Bit */

/* ADC10CTL2 Control Bits */
#define ADC10PDIV0_H        (0x0001u)    /* ADC10 predivider Bit: 0 */
#define ADC10PDIV1_H        (0x0002u)    /* ADC10 predivider Bit: 1 */

#define ADC10PDIV_0         (0x0000u)    /* ADC10 predivider /1 */
#define ADC10PDIV_1         (0x0100u)    /* ADC10 predivider /2 */
#define ADC10PDIV_2         (0x0200u)    /* ADC10 predivider /64 */
#define ADC10PDIV_3         (0x0300u)    /* ADC10 predivider reserved */

#define ADC10PDIV__1        (0x0000u)    /* ADC10 predivider /1 */
#define ADC10PDIV__4        (0x0100u)    /* ADC10 predivider /2 */
#define ADC10PDIV__64       (0x0200u)    /* ADC10 predivider /64 */

/* ADC10MCTL0 Control Bits */
#define ADC10INCH0_L        (0x0001u)    /* ADC10 Input Channel Select Bit 0 */
#define ADC10INCH1_L        (0x0002u)    /* ADC10 Input Channel Select Bit 1 */
#define ADC10INCH2_L        (0x0004u)    /* ADC10 Input Channel Select Bit 2 */
#define ADC10INCH3_L        (0x0008u)    /* ADC10 Input Channel Select Bit 3 */
#define ADC10SREF0_L        (0x0010u)    /* ADC10 Select Reference Bit 0 */
#define ADC10SREF1_L        (0x0020u)    /* ADC10 Select Reference Bit 1 */
#define ADC10SREF2_L        (0x0040u)    /* ADC10 Select Reference Bit 2 */

#define ADC10INCH_0         (0)         /* ADC10 Input Channel 0 */
#define ADC10INCH_1         (1)         /* ADC10 Input Channel 1 */
#define ADC10INCH_2         (2)         /* ADC10 Input Channel 2 */
#define ADC10INCH_3         (3)         /* ADC10 Input Channel 3 */
#define ADC10INCH_4         (4)         /* ADC10 Input Channel 4 */
#define ADC10INCH_5         (5)         /* ADC10 Input Channel 5 */
#define ADC10INCH_6         (6)         /* ADC10 Input Channel 6 */
#define ADC10INCH_7         (7)         /* ADC10 Input Channel 7 */
#define ADC10INCH_8         (8)         /* ADC10 Input Channel 8 */
#define ADC10INCH_9         (9)         /* ADC10 Input Channel 9 */
#define ADC10INCH_10        (10)        /* ADC10 Input Channel 10 */
#define ADC10INCH_11        (11)        /* ADC10 Input Channel 11 */
#define ADC10INCH_12        (12)        /* ADC10 Input Channel 12 */
#define ADC10INCH_13        (13)        /* ADC10 Input Channel 13 */
#define ADC10INCH_14        (14)        /* ADC10 Input Channel 14 */
#define ADC10INCH_15        (15)        /* ADC10 Input Channel 15 */

#define ADC10SREF_0         (0*0x10u)    /* ADC10 Select Reference 0 */
#define ADC10SREF_1         (1*0x10u)    /* ADC10 Select Reference 1 */
#define ADC10SREF_2         (2*0x10u)    /* ADC10 Select Reference 2 */
#define ADC10SREF_3         (3*0x10u)    /* ADC10 Select Reference 3 */
#define ADC10SREF_4         (4*0x10u)    /* ADC10 Select Reference 4 */
#define ADC10SREF_5         (5*0x10u)    /* ADC10 Select Reference 5 */
#define ADC10SREF_6         (6*0x10u)    /* ADC10 Select Reference 6 */
#define ADC10SREF_7         (7*0x10u)    /* ADC10 Select Reference 7 */

/* ADC10IE Interrupt Enable Bits */
#define ADC10IE0_L          (0x0001u)    /* ADC10_A Interrupt enable */
#define ADC10INIE_L         (0x0002u)    /* ADC10_A Interrupt enable for the inside of window of the Window comparator */
#define ADC10LOIE_L         (0x0004u)    /* ADC10_A Interrupt enable for lower threshold of the Window comparator */
#define ADC10HIIE_L         (0x0008u)    /* ADC10_A Interrupt enable for upper threshold of the Window comparator */
#define ADC10OVIE_L         (0x0010u)    /* ADC10_A ADC10MEM overflow Interrupt enable */
#define ADC10TOVIE_L        (0x0020u)    /* ADC10_A conversion-time-overflow Interrupt enable */

/* ADC10IFG Interrupt Flag Bits */
#define ADC10IFG0_L         (0x0001u)    /* ADC10_A Interrupt Flag */
#define ADC10INIFG_L        (0x0002u)    /* ADC10_A Interrupt Flag for the inside of window of the Window comparator */
#define ADC10LOIFG_L        (0x0004u)    /* ADC10_A Interrupt Flag for lower threshold of the Window comparator */
#define ADC10HIIFG_L        (0x0008u)    /* ADC10_A Interrupt Flag for upper threshold of the Window comparator */
#define ADC10OVIFG_L        (0x0010u)    /* ADC10_A ADC10MEM overflow Interrupt Flag */
#define ADC10TOVIFG_L       (0x0020u)    /* ADC10_A conversion-time-overflow Interrupt Flag */

/* ADC10IV Definitions */
#define ADC10IV_NONE        (0x0000u)    /* No Interrupt pending */
#define ADC10IV_ADC10OVIFG  (0x0002u)    /* ADC10OVIFG */
#define ADC10IV_ADC10TOVIFG (0x0004u)    /* ADC10TOVIFG */
#define ADC10IV_ADC10HIIFG  (0x0006u)    /* ADC10HIIFG */
#define ADC10IV_ADC10LOIFG  (0x0008u)    /* ADC10LOIFG */
#define ADC10IV_ADC10INIFG  (0x000Au)    /* ADC10INIFG */
#define ADC10IV_ADC10IFG    (0x000Cu)    /* ADC10IFG */

/*-------------------------------------------------------------------------
 *   Comparator B
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short CBCTL0;   /* Comparator B Control Register 0 */

  struct
  {
    unsigned short CBIPSEL0        : 1; /* Comp. B Pos. Channel Input Select 0 */
    unsigned short CBIPSEL1        : 1; /* Comp. B Pos. Channel Input Select 1 */
    unsigned short CBIPSEL2        : 1; /* Comp. B Pos. Channel Input Select 2 */
    unsigned short CBIPSEL3        : 1; /* Comp. B Pos. Channel Input Select 3 */
    unsigned short                : 3;
    unsigned short CBIPEN          : 1; /* Comp. B Pos. Channel Input Enable */
    unsigned short CBIMSEL0        : 1; /* Comp. B Neg. Channel Input Select 0 */
    unsigned short CBIMSEL1        : 1; /* Comp. B Neg. Channel Input Select 1 */
    unsigned short CBIMSEL2        : 1; /* Comp. B Neg. Channel Input Select 2 */
    unsigned short CBIMSEL3        : 1; /* Comp. B Neg. Channel Input Select 3 */
    unsigned short                : 3;
    unsigned short CBIMEN          : 1; /* Comp. B Neg. Channel Input Enable */
  }CBCTL0_bit;
} @0x08C0;


enum {
  CBIPSEL0        = 0x0001,
  CBIPSEL1        = 0x0002,
  CBIPSEL2        = 0x0004,
  CBIPSEL3        = 0x0008,
  CBIPEN          = 0x0080,
  CBIMSEL0        = 0x0100,
  CBIMSEL1        = 0x0200,
  CBIMSEL2        = 0x0400,
  CBIMSEL3        = 0x0800,
  CBIMEN          = 0x8000
};


__no_init volatile union
{
  unsigned short CBCTL1;   /* Comparator B Control Register 1 */

  struct
  {
    unsigned short CBOUT           : 1; /* Comp. B Output */
    unsigned short CBOUTPOL        : 1; /* Comp. B Output Polarity */
    unsigned short CBF             : 1; /* Comp. B Enable Output Filter */
    unsigned short CBIES           : 1; /* Comp. B Interrupt Edge Select */
    unsigned short CBSHORT         : 1; /* Comp. B Input Short */
    unsigned short CBEX            : 1; /* Comp. B Exchange Inputs */
    unsigned short CBFDLY0         : 1; /* Comp. B Filter delay Bit 0 */
    unsigned short CBFDLY1         : 1; /* Comp. B Filter delay Bit 1 */
    unsigned short CBPWRMD0        : 1; /* Comp. B Power Mode Bit 0 */
    unsigned short CBPWRMD1        : 1; /* Comp. B Power Mode Bit 1 */
    unsigned short CBON            : 1; /* Comp. B enable */
    unsigned short CBMRVL          : 1; /* Comp. B CBMRV Level */
    unsigned short CBMRVS          : 1; /* Comp. B Output selects between VREF0 or VREF */
  }CBCTL1_bit;
} @0x08C2;


enum {
  CBOUT           = 0x0001,
  CBOUTPOL        = 0x0002,
  CBF             = 0x0004,
  CBIES           = 0x0008,
  CBSHORT         = 0x0010,
  CBEX            = 0x0020,
  CBFDLY0         = 0x0040,
  CBFDLY1         = 0x0080,
  CBPWRMD0        = 0x0100,
  CBPWRMD1        = 0x0200,
  CBON            = 0x0400,
  CBMRVL          = 0x0800,
  CBMRVS          = 0x1000
};


__no_init volatile union
{
  unsigned short CBCTL2;   /* Comparator B Control Register 2 */

  struct
  {
    unsigned short CBREF00         : 1; /* Comp. B Reference 0 Resistor Select Bit : 0 */
    unsigned short CBREF01         : 1; /* Comp. B Reference 0 Resistor Select Bit : 1 */
    unsigned short CBREF02         : 1; /* Comp. B Reference 0 Resistor Select Bit : 2 */
    unsigned short CBREF03         : 1; /* Comp. B Reference 0 Resistor Select Bit : 3 */
    unsigned short CBREF04         : 1; /* Comp. B Reference 0 Resistor Select Bit : 4 */
    unsigned short CBRSEL          : 1; /* Comp. B Reference select */
    unsigned short CBRS0           : 1; /* Comp. B Reference Source Bit : 0 */
    unsigned short CBRS1           : 1; /* Comp. B Reference Source Bit : 1 */
    unsigned short CBREF10         : 1; /* Comp. B Reference 1 Resistor Select Bit : 0 */
    unsigned short CBREF11         : 1; /* Comp. B Reference 1 Resistor Select Bit : 1 */
    unsigned short CBREF12         : 1; /* Comp. B Reference 1 Resistor Select Bit : 2 */
    unsigned short CBREF13         : 1; /* Comp. B Reference 1 Resistor Select Bit : 3 */
    unsigned short CBREF14         : 1; /* Comp. B Reference 1 Resistor Select Bit : 4 */
    unsigned short CBREFL0         : 1; /* Comp. B Reference voltage level Bit : 0 */
    unsigned short CBREFL1         : 1; /* Comp. B Reference voltage level Bit : 1 */
    unsigned short CBREFACC        : 1; /* Comp. B Reference Accuracy */
  }CBCTL2_bit;
} @0x08C4;


enum {
  CBREF00         = 0x0001,
  CBREF01         = 0x0002,
  CBREF02         = 0x0004,
  CBREF03         = 0x0008,
  CBREF04         = 0x0010,
  CBRSEL          = 0x0020,
  CBRS0           = 0x0040,
  CBRS1           = 0x0080,
  CBREF10         = 0x0100,
  CBREF11         = 0x0200,
  CBREF12         = 0x0400,
  CBREF13         = 0x0800,
  CBREF14         = 0x1000,
  CBREFL0         = 0x2000,
  CBREFL1         = 0x4000,
  CBREFACC        = 0x8000
};


__no_init volatile union
{
  unsigned short CBCTL3;   /* Comparator B Control Register 3 */

  struct
  {
    unsigned short CBPD0           : 1; /* Comp. B Disable Input Buffer of Port Register .0 */
    unsigned short CBPD1           : 1; /* Comp. B Disable Input Buffer of Port Register .1 */
    unsigned short CBPD2           : 1; /* Comp. B Disable Input Buffer of Port Register .2 */
    unsigned short CBPD3           : 1; /* Comp. B Disable Input Buffer of Port Register .3 */
    unsigned short CBPD4           : 1; /* Comp. B Disable Input Buffer of Port Register .4 */
    unsigned short CBPD5           : 1; /* Comp. B Disable Input Buffer of Port Register .5 */
    unsigned short CBPD6           : 1; /* Comp. B Disable Input Buffer of Port Register .6 */
    unsigned short CBPD7           : 1; /* Comp. B Disable Input Buffer of Port Register .7 */
    unsigned short CBPD8           : 1; /* Comp. B Disable Input Buffer of Port Register .8 */
    unsigned short CBPD9           : 1; /* Comp. B Disable Input Buffer of Port Register .9 */
    unsigned short CBPD10          : 1; /* Comp. B Disable Input Buffer of Port Register .10 */
    unsigned short CBPD11          : 1; /* Comp. B Disable Input Buffer of Port Register .11 */
    unsigned short CBPD12          : 1; /* Comp. B Disable Input Buffer of Port Register .12 */
    unsigned short CBPD13          : 1; /* Comp. B Disable Input Buffer of Port Register .13 */
    unsigned short CBPD14          : 1; /* Comp. B Disable Input Buffer of Port Register .14 */
    unsigned short CBPD15          : 1; /* Comp. B Disable Input Buffer of Port Register .15 */
  }CBCTL3_bit;
} @0x08C6;


enum {
  CBPD0           = 0x0001,
  CBPD1           = 0x0002,
  CBPD2           = 0x0004,
  CBPD3           = 0x0008,
  CBPD4           = 0x0010,
  CBPD5           = 0x0020,
  CBPD6           = 0x0040,
  CBPD7           = 0x0080,
  CBPD8           = 0x0100,
  CBPD9           = 0x0200,
  CBPD10          = 0x0400,
  CBPD11          = 0x0800,
  CBPD12          = 0x1000,
  CBPD13          = 0x2000,
  CBPD14          = 0x4000,
  CBPD15          = 0x8000
};


__no_init volatile union
{
  unsigned short CBINT;   /* Comparator B Interrupt Register */

  struct
  {
    unsigned short CBIFG           : 1; /* Comp. B Interrupt Flag */
    unsigned short CBIIFG          : 1; /* Comp. B Interrupt Flag Inverted Polarity */
    unsigned short                : 6;
    unsigned short CBIE            : 1; /* Comp. B Interrupt Enable */
    unsigned short CBIIE           : 1; /* Comp. B Interrupt Enable Inverted Polarity */
  }CBINT_bit;
} @0x08CC;


enum {
  CBIFG           = 0x0001,
  CBIIFG          = 0x0002,
  CBIE            = 0x0100,
  CBIIE           = 0x0200
};


  /* Comparator B Interrupt Vector Word */
__no_init volatile unsigned short CBIV @ 0x08CE;


#define __MSP430_HAS_COMPB__          /* Definition to show that Module is available */


/* CBCTL0 Control Bits */
#define CBIPSEL0_L          (0x0001u)  /* Comp. B Pos. Channel Input Select 0 */
#define CBIPSEL1_L          (0x0002u)  /* Comp. B Pos. Channel Input Select 1 */
#define CBIPSEL2_L          (0x0004u)  /* Comp. B Pos. Channel Input Select 2 */
#define CBIPSEL3_L          (0x0008u)  /* Comp. B Pos. Channel Input Select 3 */
#define CBIPEN_L            (0x0080u)  /* Comp. B Pos. Channel Input Enable */
#define CBIMSEL0_H          (0x0001u)  /* Comp. B Neg. Channel Input Select 0 */
#define CBIMSEL1_H          (0x0002u)  /* Comp. B Neg. Channel Input Select 1 */
#define CBIMSEL2_H          (0x0004u)  /* Comp. B Neg. Channel Input Select 2 */
#define CBIMSEL3_H          (0x0008u)  /* Comp. B Neg. Channel Input Select 3 */
#define CBIMEN_H            (0x0080u)  /* Comp. B Neg. Channel Input Enable */

#define CBIPSEL_0           (0x0000u)  /* Comp. B V+ terminal Input Select: Channel 0 */
#define CBIPSEL_1           (0x0001u)  /* Comp. B V+ terminal Input Select: Channel 1 */
#define CBIPSEL_2           (0x0002u)  /* Comp. B V+ terminal Input Select: Channel 2 */
#define CBIPSEL_3           (0x0003u)  /* Comp. B V+ terminal Input Select: Channel 3 */
#define CBIPSEL_4           (0x0004u)  /* Comp. B V+ terminal Input Select: Channel 4 */
#define CBIPSEL_5           (0x0005u)  /* Comp. B V+ terminal Input Select: Channel 5 */
#define CBIPSEL_6           (0x0006u)  /* Comp. B V+ terminal Input Select: Channel 6 */
#define CBIPSEL_7           (0x0007u)  /* Comp. B V+ terminal Input Select: Channel 7 */
#define CBIPSEL_8           (0x0008u)  /* Comp. B V+ terminal Input Select: Channel 8 */
#define CBIPSEL_9           (0x0009u)  /* Comp. B V+ terminal Input Select: Channel 9 */
#define CBIPSEL_10          (0x000Au)  /* Comp. B V+ terminal Input Select: Channel 10 */
#define CBIPSEL_11          (0x000Bu)  /* Comp. B V+ terminal Input Select: Channel 11 */
#define CBIPSEL_12          (0x000Cu)  /* Comp. B V+ terminal Input Select: Channel 12 */
#define CBIPSEL_13          (0x000Du)  /* Comp. B V+ terminal Input Select: Channel 13 */
#define CBIPSEL_14          (0x000Eu)  /* Comp. B V+ terminal Input Select: Channel 14 */
#define CBIPSEL_15          (0x000Fu)  /* Comp. B V+ terminal Input Select: Channel 15 */

#define CBIMSEL_0           (0x0000u)  /* Comp. B V- Terminal Input Select: Channel 0 */
#define CBIMSEL_1           (0x0100u)  /* Comp. B V- Terminal Input Select: Channel 1 */
#define CBIMSEL_2           (0x0200u)  /* Comp. B V- Terminal Input Select: Channel 2 */
#define CBIMSEL_3           (0x0300u)  /* Comp. B V- Terminal Input Select: Channel 3 */
#define CBIMSEL_4           (0x0400u)  /* Comp. B V- Terminal Input Select: Channel 4 */
#define CBIMSEL_5           (0x0500u)  /* Comp. B V- Terminal Input Select: Channel 5 */
#define CBIMSEL_6           (0x0600u)  /* Comp. B V- Terminal Input Select: Channel 6 */
#define CBIMSEL_7           (0x0700u)  /* Comp. B V- Terminal Input Select: Channel 7 */
#define CBIMSEL_8           (0x0800u)  /* Comp. B V- terminal Input Select: Channel 8 */
#define CBIMSEL_9           (0x0900u)  /* Comp. B V- terminal Input Select: Channel 9 */
#define CBIMSEL_10          (0x0A00u)  /* Comp. B V- terminal Input Select: Channel 10 */
#define CBIMSEL_11          (0x0B00u)  /* Comp. B V- terminal Input Select: Channel 11 */
#define CBIMSEL_12          (0x0C00u)  /* Comp. B V- terminal Input Select: Channel 12 */
#define CBIMSEL_13          (0x0D00u)  /* Comp. B V- terminal Input Select: Channel 13 */
#define CBIMSEL_14          (0x0E00u)  /* Comp. B V- terminal Input Select: Channel 14 */
#define CBIMSEL_15          (0x0F00u)  /* Comp. B V- terminal Input Select: Channel 15 */

/* CBCTL1 Control Bits */
#define CBOUT_L             (0x0001u)  /* Comp. B Output */
#define CBOUTPOL_L          (0x0002u)  /* Comp. B Output Polarity */
#define CBF_L               (0x0004u)  /* Comp. B Enable Output Filter */
#define CBIES_L             (0x0008u)  /* Comp. B Interrupt Edge Select */
#define CBSHORT_L           (0x0010u)  /* Comp. B Input Short */
#define CBEX_L              (0x0020u)  /* Comp. B Exchange Inputs */
#define CBFDLY0_L           (0x0040u)  /* Comp. B Filter delay Bit 0 */
#define CBFDLY1_L           (0x0080u)  /* Comp. B Filter delay Bit 1 */

/* CBCTL1 Control Bits */
#define CBPWRMD0_H          (0x0001u)  /* Comp. B Power Mode Bit 0 */
#define CBPWRMD1_H          (0x0002u)  /* Comp. B Power Mode Bit 1 */
#define CBON_H              (0x0004u)  /* Comp. B enable */
#define CBMRVL_H            (0x0008u)  /* Comp. B CBMRV Level */
#define CBMRVS_H            (0x0010u)  /* Comp. B Output selects between VREF0 or VREF1*/

#define CBFDLY_0           (0x0000u)  /* Comp. B Filter delay 0 : 450ns */
#define CBFDLY_1           (0x0040u)  /* Comp. B Filter delay 1 : 900ns */
#define CBFDLY_2           (0x0080u)  /* Comp. B Filter delay 2 : 1800ns */
#define CBFDLY_3           (0x00C0u)  /* Comp. B Filter delay 3 : 3600ns */

#define CBPWRMD_0           (0x0000u)  /* Comp. B Power Mode 0 : High speed */
#define CBPWRMD_1           (0x0100u)  /* Comp. B Power Mode 1 : Normal */
#define CBPWRMD_2           (0x0200u)  /* Comp. B Power Mode 2 : Ultra-Low*/
#define CBPWRMD_3           (0x0300u)  /* Comp. B Power Mode 3 : Reserved */

/* CBCTL2 Control Bits */
#define CBREF00_L           (0x0001u)  /* Comp. B Reference 0 Resistor Select Bit : 0 */
#define CBREF01_L           (0x0002u)  /* Comp. B Reference 0 Resistor Select Bit : 1 */
#define CBREF02_L           (0x0004u)  /* Comp. B Reference 0 Resistor Select Bit : 2 */
#define CBREF03_L           (0x0008u)  /* Comp. B Reference 0 Resistor Select Bit : 3 */
#define CBREF04_L           (0x0010u)  /* Comp. B Reference 0 Resistor Select Bit : 4 */
#define CBRSEL_L            (0x0020u)  /* Comp. B Reference select */
#define CBRS0_L             (0x0040u)  /* Comp. B Reference Source Bit : 0 */
#define CBRS1_L             (0x0080u)  /* Comp. B Reference Source Bit : 1 */

/* CBCTL2 Control Bits */
#define CBREF10_H           (0x0001u)  /* Comp. B Reference 1 Resistor Select Bit : 0 */
#define CBREF11_H           (0x0002u)  /* Comp. B Reference 1 Resistor Select Bit : 1 */
#define CBREF12_H           (0x0004u)  /* Comp. B Reference 1 Resistor Select Bit : 2 */
#define CBREF13_H           (0x0008u)  /* Comp. B Reference 1 Resistor Select Bit : 3 */
#define CBREF14_H           (0x0010u)  /* Comp. B Reference 1 Resistor Select Bit : 4 */
#define CBREFL0_H           (0x0020u)  /* Comp. B Reference voltage level Bit : 0 */
#define CBREFL1_H           (0x0040u)  /* Comp. B Reference voltage level Bit : 1 */
#define CBREFACC_H          (0x0080u)  /* Comp. B Reference Accuracy */

#define CBREF0_0            (0x0000u)  /* Comp. B Int. Ref.0 Select 0 : 1/32 */
#define CBREF0_1            (0x0001u)  /* Comp. B Int. Ref.0 Select 1 : 2/32 */
#define CBREF0_2            (0x0002u)  /* Comp. B Int. Ref.0 Select 2 : 3/32 */
#define CBREF0_3            (0x0003u)  /* Comp. B Int. Ref.0 Select 3 : 4/32 */
#define CBREF0_4            (0x0004u)  /* Comp. B Int. Ref.0 Select 4 : 5/32 */
#define CBREF0_5            (0x0005u)  /* Comp. B Int. Ref.0 Select 5 : 6/32 */
#define CBREF0_6            (0x0006u)  /* Comp. B Int. Ref.0 Select 6 : 7/32 */
#define CBREF0_7            (0x0007u)  /* Comp. B Int. Ref.0 Select 7 : 8/32 */
#define CBREF0_8            (0x0008u)  /* Comp. B Int. Ref.0 Select 0 : 9/32 */
#define CBREF0_9            (0x0009u)  /* Comp. B Int. Ref.0 Select 1 : 10/32 */
#define CBREF0_10           (0x000Au)  /* Comp. B Int. Ref.0 Select 2 : 11/32 */
#define CBREF0_11           (0x000Bu)  /* Comp. B Int. Ref.0 Select 3 : 12/32 */
#define CBREF0_12           (0x000Cu)  /* Comp. B Int. Ref.0 Select 4 : 13/32 */
#define CBREF0_13           (0x000Du)  /* Comp. B Int. Ref.0 Select 5 : 14/32 */
#define CBREF0_14           (0x000Eu)  /* Comp. B Int. Ref.0 Select 6 : 15/32 */
#define CBREF0_15           (0x000Fu)  /* Comp. B Int. Ref.0 Select 7 : 16/32 */
#define CBREF0_16           (0x0010u)  /* Comp. B Int. Ref.0 Select 0 : 17/32 */
#define CBREF0_17           (0x0011u)  /* Comp. B Int. Ref.0 Select 1 : 18/32 */
#define CBREF0_18           (0x0012u)  /* Comp. B Int. Ref.0 Select 2 : 19/32 */
#define CBREF0_19           (0x0013u)  /* Comp. B Int. Ref.0 Select 3 : 20/32 */
#define CBREF0_20           (0x0014u)  /* Comp. B Int. Ref.0 Select 4 : 21/32 */
#define CBREF0_21           (0x0015u)  /* Comp. B Int. Ref.0 Select 5 : 22/32 */
#define CBREF0_22           (0x0016u)  /* Comp. B Int. Ref.0 Select 6 : 23/32 */
#define CBREF0_23           (0x0017u)  /* Comp. B Int. Ref.0 Select 7 : 24/32 */
#define CBREF0_24           (0x0018u)  /* Comp. B Int. Ref.0 Select 0 : 25/32 */
#define CBREF0_25           (0x0019u)  /* Comp. B Int. Ref.0 Select 1 : 26/32 */
#define CBREF0_26           (0x001Au)  /* Comp. B Int. Ref.0 Select 2 : 27/32 */
#define CBREF0_27           (0x001Bu)  /* Comp. B Int. Ref.0 Select 3 : 28/32 */
#define CBREF0_28           (0x001Cu)  /* Comp. B Int. Ref.0 Select 4 : 29/32 */
#define CBREF0_29           (0x001Du)  /* Comp. B Int. Ref.0 Select 5 : 30/32 */
#define CBREF0_30           (0x001Eu)  /* Comp. B Int. Ref.0 Select 6 : 31/32 */
#define CBREF0_31           (0x001Fu)  /* Comp. B Int. Ref.0 Select 7 : 32/32 */

#define CBRS_0              (0x0000u)  /* Comp. B Reference Source 0 : Off */
#define CBRS_1              (0x0040u)  /* Comp. B Reference Source 1 : Vcc */
#define CBRS_2              (0x0080u)  /* Comp. B Reference Source 2 : Shared Ref. */
#define CBRS_3              (0x00C0u)  /* Comp. B Reference Source 3 : Shared Ref. / Off */

#define CBREF1_0            (0x0000u)  /* Comp. B Int. Ref.1 Select 0 : 1/32 */
#define CBREF1_1            (0x0100u)  /* Comp. B Int. Ref.1 Select 1 : 2/32 */
#define CBREF1_2            (0x0200u)  /* Comp. B Int. Ref.1 Select 2 : 3/32 */
#define CBREF1_3            (0x0300u)  /* Comp. B Int. Ref.1 Select 3 : 4/32 */
#define CBREF1_4            (0x0400u)  /* Comp. B Int. Ref.1 Select 4 : 5/32 */
#define CBREF1_5            (0x0500u)  /* Comp. B Int. Ref.1 Select 5 : 6/32 */
#define CBREF1_6            (0x0600u)  /* Comp. B Int. Ref.1 Select 6 : 7/32 */
#define CBREF1_7            (0x0700u)  /* Comp. B Int. Ref.1 Select 7 : 8/32 */
#define CBREF1_8            (0x0800u)  /* Comp. B Int. Ref.1 Select 0 : 9/32 */
#define CBREF1_9            (0x0900u)  /* Comp. B Int. Ref.1 Select 1 : 10/32 */
#define CBREF1_10           (0x0A00u)  /* Comp. B Int. Ref.1 Select 2 : 11/32 */
#define CBREF1_11           (0x0B00u)  /* Comp. B Int. Ref.1 Select 3 : 12/32 */
#define CBREF1_12           (0x0C00u)  /* Comp. B Int. Ref.1 Select 4 : 13/32 */
#define CBREF1_13           (0x0D00u)  /* Comp. B Int. Ref.1 Select 5 : 14/32 */
#define CBREF1_14           (0x0E00u)  /* Comp. B Int. Ref.1 Select 6 : 15/32 */
#define CBREF1_15           (0x0F00u)  /* Comp. B Int. Ref.1 Select 7 : 16/32 */
#define CBREF1_16           (0x1000u)  /* Comp. B Int. Ref.1 Select 0 : 17/32 */
#define CBREF1_17           (0x1100u)  /* Comp. B Int. Ref.1 Select 1 : 18/32 */
#define CBREF1_18           (0x1200u)  /* Comp. B Int. Ref.1 Select 2 : 19/32 */
#define CBREF1_19           (0x1300u)  /* Comp. B Int. Ref.1 Select 3 : 20/32 */
#define CBREF1_20           (0x1400u)  /* Comp. B Int. Ref.1 Select 4 : 21/32 */
#define CBREF1_21           (0x1500u)  /* Comp. B Int. Ref.1 Select 5 : 22/32 */
#define CBREF1_22           (0x1600u)  /* Comp. B Int. Ref.1 Select 6 : 23/32 */
#define CBREF1_23           (0x1700u)  /* Comp. B Int. Ref.1 Select 7 : 24/32 */
#define CBREF1_24           (0x1800u)  /* Comp. B Int. Ref.1 Select 0 : 25/32 */
#define CBREF1_25           (0x1900u)  /* Comp. B Int. Ref.1 Select 1 : 26/32 */
#define CBREF1_26           (0x1A00u)  /* Comp. B Int. Ref.1 Select 2 : 27/32 */
#define CBREF1_27           (0x1B00u)  /* Comp. B Int. Ref.1 Select 3 : 28/32 */
#define CBREF1_28           (0x1C00u)  /* Comp. B Int. Ref.1 Select 4 : 29/32 */
#define CBREF1_29           (0x1D00u)  /* Comp. B Int. Ref.1 Select 5 : 30/32 */
#define CBREF1_30           (0x1E00u)  /* Comp. B Int. Ref.1 Select 6 : 31/32 */
#define CBREF1_31           (0x1F00u)  /* Comp. B Int. Ref.1 Select 7 : 32/32 */

#define CBREFL_0            (0x0000u)  /* Comp. B Reference voltage level 0 : None */
#define CBREFL_1            (0x2000u)  /* Comp. B Reference voltage level 1 : 1.5V */
#define CBREFL_2            (0x4000u)  /* Comp. B Reference voltage level 2 : 2.0V  */
#define CBREFL_3            (0x6000u)  /* Comp. B Reference voltage level 3 : 2.5V  */

#define CBPD0_L             (0x0001u)  /* Comp. B Disable Input Buffer of Port Register .0 */
#define CBPD1_L             (0x0002u)  /* Comp. B Disable Input Buffer of Port Register .1 */
#define CBPD2_L             (0x0004u)  /* Comp. B Disable Input Buffer of Port Register .2 */
#define CBPD3_L             (0x0008u)  /* Comp. B Disable Input Buffer of Port Register .3 */
#define CBPD4_L             (0x0010u)  /* Comp. B Disable Input Buffer of Port Register .4 */
#define CBPD5_L             (0x0020u)  /* Comp. B Disable Input Buffer of Port Register .5 */
#define CBPD6_L             (0x0040u)  /* Comp. B Disable Input Buffer of Port Register .6 */
#define CBPD7_L             (0x0080u)  /* Comp. B Disable Input Buffer of Port Register .7 */

#define CBPD8_H             (0x0001u)  /* Comp. B Disable Input Buffer of Port Register .8 */
#define CBPD9_H             (0x0002u)  /* Comp. B Disable Input Buffer of Port Register .9 */
#define CBPD10_H            (0x0004u)  /* Comp. B Disable Input Buffer of Port Register .10 */
#define CBPD11_H            (0x0008u)  /* Comp. B Disable Input Buffer of Port Register .11 */
#define CBPD12_H            (0x0010u)  /* Comp. B Disable Input Buffer of Port Register .12 */
#define CBPD13_H            (0x0020u)  /* Comp. B Disable Input Buffer of Port Register .13 */
#define CBPD14_H            (0x0040u)  /* Comp. B Disable Input Buffer of Port Register .14 */
#define CBPD15_H            (0x0080u)  /* Comp. B Disable Input Buffer of Port Register .15 */

/* CBINT Control Bits */
#define CBIFG_L             (0x0001u)  /* Comp. B Interrupt Flag */
#define CBIIFG_L            (0x0002u)  /* Comp. B Interrupt Flag Inverted Polarity */
#define CBIE_H              (0x0001u)  /* Comp. B Interrupt Enable */
#define CBIIE_H             (0x0002u)  /* Comp. B Interrupt Enable Inverted Polarity */

/* CBIV Definitions */
#define CBIV_NONE           (0x0000u)    /* No Interrupt pending */
#define CBIV_CBIFG          (0x0002u)    /* CBIFG */
#define CBIV_CBIIFG         (0x0004u)    /* CBIIFG */

/*-------------------------------------------------------------------------
 *   CRC16
 *-------------------------------------------------------------------------*/


  /* CRC Data In Register */
__no_init volatile unsigned short CRCDI @ 0x0150;


  /* CRC data in reverse byte Register */
__no_init volatile unsigned short CRCDIRB @ 0x0152;


  /* CRC Initialisation Register and Result Register */
__no_init volatile unsigned short CRCINIRES @ 0x0154;


  /* CRC reverse result Register */
__no_init volatile unsigned short CRCRESR @ 0x0156;


#define __MSP430_HAS_CRC__            /* Definition to show that Module is available */


/*-------------------------------------------------------------------------
 *   DMA
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short DMACTL0;   /* DMA Module Control 0 */

  struct
  {
    unsigned short DMA0TSEL0       : 1; /* DMA channel 0 transfer select bit 0 */
    unsigned short DMA0TSEL1       : 1; /* DMA channel 0 transfer select bit 1 */
    unsigned short DMA0TSEL2       : 1; /* DMA channel 0 transfer select bit 2 */
    unsigned short DMA0TSEL3       : 1; /* DMA channel 0 transfer select bit 3 */
    unsigned short DMA0TSEL4       : 1; /* DMA channel 0 transfer select bit 4 */
    unsigned short                : 3;
    unsigned short DMA1TSEL0       : 1; /* DMA channel 1 transfer select bit 0 */
    unsigned short DMA1TSEL1       : 1; /* DMA channel 1 transfer select bit 1 */
    unsigned short DMA1TSEL2       : 1; /* DMA channel 1 transfer select bit 2 */
    unsigned short DMA1TSEL3       : 1; /* DMA channel 1 transfer select bit 3 */
    unsigned short DMA1TSEL4       : 1; /* DMA channel 1 transfer select bit 4 */
  }DMACTL0_bit;
} @0x0500;


enum {
  DMA0TSEL0       = 0x0001,
  DMA0TSEL1       = 0x0002,
  DMA0TSEL2       = 0x0004,
  DMA0TSEL3       = 0x0008,
  DMA0TSEL4       = 0x0010,
  DMA1TSEL0       = 0x0100,
  DMA1TSEL1       = 0x0200,
  DMA1TSEL2       = 0x0400,
  DMA1TSEL3       = 0x0800,
  DMA1TSEL4       = 0x1000
};


__no_init volatile union
{
  unsigned short DMACTL1;   /* DMA Module Control 1 */

  struct
  {
    unsigned short DMA2TSEL0       : 1; /* DMA channel 2 transfer select bit 0 */
    unsigned short DMA2TSEL1       : 1; /* DMA channel 2 transfer select bit 1 */
    unsigned short DMA2TSEL2       : 1; /* DMA channel 2 transfer select bit 2 */
    unsigned short DMA2TSEL3       : 1; /* DMA channel 2 transfer select bit 3 */
    unsigned short DMA2TSEL4       : 1; /* DMA channel 2 transfer select bit 4 */
  }DMACTL1_bit;
} @0x0502;


enum {
  DMA2TSEL0       = 0x0001,
  DMA2TSEL1       = 0x0002,
  DMA2TSEL2       = 0x0004,
  DMA2TSEL3       = 0x0008,
  DMA2TSEL4       = 0x0010
};


  /* DMA Module Control 2 */
__no_init volatile unsigned short DMACTL2 @ 0x0504;


  /* DMA Module Control 3 */
__no_init volatile unsigned short DMACTL3 @ 0x0506;


__no_init volatile union
{
  unsigned short DMACTL4;   /* DMA Module Control 4 */

  struct
  {
    unsigned short ENNMI           : 1; /* Enable NMI interruption of DMA */
    unsigned short ROUNDROBIN      : 1; /* Round-Robin DMA channel priorities */
    unsigned short DMARMWDIS       : 1; /* Inhibited DMA transfers during read-modify-write CPU operations */
  }DMACTL4_bit;
} @0x0508;


enum {
  ENNMI           = 0x0001,
  ROUNDROBIN      = 0x0002,
  DMARMWDIS       = 0x0004
};


  /* DMA Interrupt Vector Word */
__no_init volatile unsigned short DMAIV @ 0x050E;


__no_init volatile union
{
  unsigned short DMA0CTL;   /* DMA Channel 0 Control */

  struct
  {
    unsigned short DMAREQ          : 1; /* Initiate DMA transfer with DMATSEL */
    unsigned short DMAABORT        : 1; /* DMA transfer aborted by NMI */
    unsigned short DMAIE           : 1; /* DMA interrupt enable */
    unsigned short DMAIFG          : 1; /* DMA interrupt flag */
    unsigned short DMAEN           : 1; /* DMA enable */
    unsigned short DMALEVEL        : 1; /* DMA level sensitive trigger select */
    unsigned short DMASRCBYTE      : 1; /* DMA source byte */
    unsigned short DMADSTBYTE      : 1; /* DMA destination byte */
    unsigned short DMASRCINCR0     : 1; /* DMA source increment bit 0 */
    unsigned short DMASRCINCR1     : 1; /* DMA source increment bit 1 */
    unsigned short DMADSTINCR0     : 1; /* DMA destination increment bit 0 */
    unsigned short DMADSTINCR1     : 1; /* DMA destination increment bit 1 */
    unsigned short DMADT0          : 1; /* DMA transfer mode bit 0 */
    unsigned short DMADT1          : 1; /* DMA transfer mode bit 1 */
    unsigned short DMADT2          : 1; /* DMA transfer mode bit 2 */
  }DMA0CTL_bit;
} @0x0510;


enum {
  DMAREQ          = 0x0001,
  DMAABORT        = 0x0002,
  DMAIE           = 0x0004,
  DMAIFG          = 0x0008,
  DMAEN           = 0x0010,
  DMALEVEL        = 0x0020,
  DMASRCBYTE      = 0x0040,
  DMADSTBYTE      = 0x0080,
  DMASRCINCR0     = 0x0100,
  DMASRCINCR1     = 0x0200,
  DMADSTINCR0     = 0x0400,
  DMADSTINCR1     = 0x0800,
  DMADT0          = 0x1000,
  DMADT1          = 0x2000,
  DMADT2          = 0x4000
};


  /* DMA Channel 0 Source Address */
__no_init volatile unsigned long DMA0SA @ 0x0512;


  /* DMA Channel 0 Destination Address */
__no_init volatile unsigned long DMA0DA @ 0x0516;


  /* DMA Channel 0 Transfer Size */
__no_init volatile unsigned short DMA0SZ @ 0x051A;


__no_init volatile union
{
  unsigned short DMA1CTL;   /* DMA Channel 1 Control */

  struct
  {
    unsigned short DMAREQ          : 1; /* Initiate DMA transfer with DMATSEL */
    unsigned short DMAABORT        : 1; /* DMA transfer aborted by NMI */
    unsigned short DMAIE           : 1; /* DMA interrupt enable */
    unsigned short DMAIFG          : 1; /* DMA interrupt flag */
    unsigned short DMAEN           : 1; /* DMA enable */
    unsigned short DMALEVEL        : 1; /* DMA level sensitive trigger select */
    unsigned short DMASRCBYTE      : 1; /* DMA source byte */
    unsigned short DMADSTBYTE      : 1; /* DMA destination byte */
    unsigned short DMASRCINCR0     : 1; /* DMA source increment bit 0 */
    unsigned short DMASRCINCR1     : 1; /* DMA source increment bit 1 */
    unsigned short DMADSTINCR0     : 1; /* DMA destination increment bit 0 */
    unsigned short DMADSTINCR1     : 1; /* DMA destination increment bit 1 */
    unsigned short DMADT0          : 1; /* DMA transfer mode bit 0 */
    unsigned short DMADT1          : 1; /* DMA transfer mode bit 1 */
    unsigned short DMADT2          : 1; /* DMA transfer mode bit 2 */
  }DMA1CTL_bit;
} @0x0520;


/*
enum {
  DMAREQ          = 0x0001,
  DMAABORT        = 0x0002,
  DMAIE           = 0x0004,
  DMAIFG          = 0x0008,
  DMAEN           = 0x0010,
  DMALEVEL        = 0x0020,
  DMASRCBYTE      = 0x0040,
  DMADSTBYTE      = 0x0080,
  DMASRCINCR0     = 0x0100,
  DMASRCINCR1     = 0x0200,
  DMADSTINCR0     = 0x0400,
  DMADSTINCR1     = 0x0800,
  DMADT0          = 0x1000,
  DMADT1          = 0x2000,
  DMADT2          = 0x4000,
};
*/


  /* DMA Channel 1 Source Address */
__no_init volatile unsigned long DMA1SA @ 0x0522;


  /* DMA Channel 1 Destination Address */
__no_init volatile unsigned long DMA1DA @ 0x0526;


  /* DMA Channel 1 Transfer Size */
__no_init volatile unsigned short DMA1SZ @ 0x052A;


__no_init volatile union
{
  unsigned short DMA2CTL;   /* DMA Channel 2 Control */

  struct
  {
    unsigned short DMAREQ          : 1; /* Initiate DMA transfer with DMATSEL */
    unsigned short DMAABORT        : 1; /* DMA transfer aborted by NMI */
    unsigned short DMAIE           : 1; /* DMA interrupt enable */
    unsigned short DMAIFG          : 1; /* DMA interrupt flag */
    unsigned short DMAEN           : 1; /* DMA enable */
    unsigned short DMALEVEL        : 1; /* DMA level sensitive trigger select */
    unsigned short DMASRCBYTE      : 1; /* DMA source byte */
    unsigned short DMADSTBYTE      : 1; /* DMA destination byte */
    unsigned short DMASRCINCR0     : 1; /* DMA source increment bit 0 */
    unsigned short DMASRCINCR1     : 1; /* DMA source increment bit 1 */
    unsigned short DMADSTINCR0     : 1; /* DMA destination increment bit 0 */
    unsigned short DMADSTINCR1     : 1; /* DMA destination increment bit 1 */
    unsigned short DMADT0          : 1; /* DMA transfer mode bit 0 */
    unsigned short DMADT1          : 1; /* DMA transfer mode bit 1 */
    unsigned short DMADT2          : 1; /* DMA transfer mode bit 2 */
  }DMA2CTL_bit;
} @0x0530;


/*
enum {
  DMAREQ          = 0x0001,
  DMAABORT        = 0x0002,
  DMAIE           = 0x0004,
  DMAIFG          = 0x0008,
  DMAEN           = 0x0010,
  DMALEVEL        = 0x0020,
  DMASRCBYTE      = 0x0040,
  DMADSTBYTE      = 0x0080,
  DMASRCINCR0     = 0x0100,
  DMASRCINCR1     = 0x0200,
  DMADSTINCR0     = 0x0400,
  DMADSTINCR1     = 0x0800,
  DMADT0          = 0x1000,
  DMADT1          = 0x2000,
  DMADT2          = 0x4000,
};
*/


  /* DMA Channel 2 Source Address */
__no_init volatile unsigned long DMA2SA @ 0x0532;


  /* DMA Channel 2 Destination Address */
__no_init volatile unsigned long DMA2DA @ 0x0536;


  /* DMA Channel 2 Transfer Size */
__no_init volatile unsigned short DMA2SZ @ 0x053A;


#define __MSP430_HAS_DMAX_3__           /* Definition to show that Module is available */


/* DMACTL0 Control Bits */
#define DMA0TSEL0_L         (0x0001u)    /* DMA channel 0 transfer select bit 0 */
#define DMA0TSEL1_L         (0x0002u)    /* DMA channel 0 transfer select bit 1 */
#define DMA0TSEL2_L         (0x0004u)    /* DMA channel 0 transfer select bit 2 */
#define DMA0TSEL3_L         (0x0008u)    /* DMA channel 0 transfer select bit 3 */
#define DMA0TSEL4_L         (0x0010u)    /* DMA channel 0 transfer select bit 4 */

/* DMACTL0 Control Bits */
#define DMA1TSEL0_H         (0x0001u)    /* DMA channel 1 transfer select bit 0 */
#define DMA1TSEL1_H         (0x0002u)    /* DMA channel 1 transfer select bit 1 */
#define DMA1TSEL2_H         (0x0004u)    /* DMA channel 1 transfer select bit 2 */
#define DMA1TSEL3_H         (0x0008u)    /* DMA channel 1 transfer select bit 3 */
#define DMA1TSEL4_H         (0x0010u)    /* DMA channel 1 transfer select bit 4 */

/* DMACTL01 Control Bits */
#define DMA2TSEL0_L         (0x0001u)    /* DMA channel 2 transfer select bit 0 */
#define DMA2TSEL1_L         (0x0002u)    /* DMA channel 2 transfer select bit 1 */
#define DMA2TSEL2_L         (0x0004u)    /* DMA channel 2 transfer select bit 2 */
#define DMA2TSEL3_L         (0x0008u)    /* DMA channel 2 transfer select bit 3 */
#define DMA2TSEL4_L         (0x0010u)    /* DMA channel 2 transfer select bit 4 */

/* DMACTL4 Control Bits */
#define ENNMI_L             (0x0001u)    /* Enable NMI interruption of DMA */
#define ROUNDROBIN_L        (0x0002u)    /* Round-Robin DMA channel priorities */
#define DMARMWDIS_L         (0x0004u)    /* Inhibited DMA transfers during read-modify-write CPU operations */

/* DMAxCTL Control Bits */
#define DMAREQ_L            (0x0001u)    /* Initiate DMA transfer with DMATSEL */
#define DMAABORT_L          (0x0002u)    /* DMA transfer aborted by NMI */
#define DMAIE_L             (0x0004u)    /* DMA interrupt enable */
#define DMAIFG_L            (0x0008u)    /* DMA interrupt flag */
#define DMAEN_L             (0x0010u)    /* DMA enable */
#define DMALEVEL_L          (0x0020u)    /* DMA level sensitive trigger select */
#define DMASRCBYTE_L        (0x0040u)    /* DMA source byte */
#define DMADSTBYTE_L        (0x0080u)    /* DMA destination byte */

/* DMAxCTL Control Bits */
#define DMASRCINCR0_H       (0x0001u)    /* DMA source increment bit 0 */
#define DMASRCINCR1_H       (0x0002u)    /* DMA source increment bit 1 */
#define DMADSTINCR0_H       (0x0004u)    /* DMA destination increment bit 0 */
#define DMADSTINCR1_H       (0x0008u)    /* DMA destination increment bit 1 */
#define DMADT0_H            (0x0010u)    /* DMA transfer mode bit 0 */
#define DMADT1_H            (0x0020u)    /* DMA transfer mode bit 1 */
#define DMADT2_H            (0x0040u)    /* DMA transfer mode bit 2 */

#define DMASWDW             (0*0x0040u)  /* DMA transfer: source word to destination word */
#define DMASBDW             (1*0x0040u)  /* DMA transfer: source byte to destination word */
#define DMASWDB             (2*0x0040u)  /* DMA transfer: source word to destination byte */
#define DMASBDB             (3*0x0040u)  /* DMA transfer: source byte to destination byte */

#define DMASRCINCR_0        (0*0x0100u)  /* DMA source increment 0: source address unchanged */
#define DMASRCINCR_1        (1*0x0100u)  /* DMA source increment 1: source address unchanged */
#define DMASRCINCR_2        (2*0x0100u)  /* DMA source increment 2: source address decremented */
#define DMASRCINCR_3        (3*0x0100u)  /* DMA source increment 3: source address incremented */

#define DMADSTINCR_0        (0*0x0400u)  /* DMA destination increment 0: destination address unchanged */
#define DMADSTINCR_1        (1*0x0400u)  /* DMA destination increment 1: destination address unchanged */
#define DMADSTINCR_2        (2*0x0400u)  /* DMA destination increment 2: destination address decremented */
#define DMADSTINCR_3        (3*0x0400u)  /* DMA destination increment 3: destination address incremented */

#define DMADT_0             (0*0x1000u)  /* DMA transfer mode 0: Single transfer */
#define DMADT_1             (1*0x1000u)  /* DMA transfer mode 1: Block transfer */
#define DMADT_2             (2*0x1000u)  /* DMA transfer mode 2: Burst-Block transfer */
#define DMADT_3             (3*0x1000u)  /* DMA transfer mode 3: Burst-Block transfer */
#define DMADT_4             (4*0x1000u)  /* DMA transfer mode 4: Repeated Single transfer */
#define DMADT_5             (5*0x1000u)  /* DMA transfer mode 5: Repeated Block transfer */
#define DMADT_6             (6*0x1000u)  /* DMA transfer mode 6: Repeated Burst-Block transfer */
#define DMADT_7             (7*0x1000u)  /* DMA transfer mode 7: Repeated Burst-Block transfer */

/* DMAIV Definitions */
#define DMAIV_NONE           (0x0000u)    /* No Interrupt pending */
#define DMAIV_DMA0IFG        (0x0002u)    /* DMA0IFG*/
#define DMAIV_DMA1IFG        (0x0004u)    /* DMA1IFG*/
#define DMAIV_DMA2IFG        (0x0006u)    /* DMA2IFG*/

#define DMA0TSEL_0          (0*0x0001u)  /* DMA channel 0 transfer select 0  */
#define DMA0TSEL_1          (1*0x0001u)  /* DMA channel 0 transfer select 1  */
#define DMA0TSEL_2          (2*0x0001u)  /* DMA channel 0 transfer select 2  */
#define DMA0TSEL_3          (3*0x0001u)  /* DMA channel 0 transfer select 3  */
#define DMA0TSEL_4          (4*0x0001u)  /* DMA channel 0 transfer select 4  */
#define DMA0TSEL_5          (5*0x0001u)  /* DMA channel 0 transfer select 5  */
#define DMA0TSEL_6          (6*0x0001u)  /* DMA channel 0 transfer select 6  */
#define DMA0TSEL_7          (7*0x0001u)  /* DMA channel 0 transfer select 7  */
#define DMA0TSEL_8          (8*0x0001u)  /* DMA channel 0 transfer select 8  */
#define DMA0TSEL_9          (9*0x0001u)  /* DMA channel 0 transfer select 9  */
#define DMA0TSEL_10         (10*0x0001u) /* DMA channel 0 transfer select 10 */
#define DMA0TSEL_11         (11*0x0001u) /* DMA channel 0 transfer select 11 */
#define DMA0TSEL_12         (12*0x0001u) /* DMA channel 0 transfer select 12 */
#define DMA0TSEL_13         (13*0x0001u) /* DMA channel 0 transfer select 13 */
#define DMA0TSEL_14         (14*0x0001u) /* DMA channel 0 transfer select 14 */
#define DMA0TSEL_15         (15*0x0001u) /* DMA channel 0 transfer select 15 */
#define DMA0TSEL_16         (16*0x0001u) /* DMA channel 0 transfer select 16 */
#define DMA0TSEL_17         (17*0x0001u) /* DMA channel 0 transfer select 17 */
#define DMA0TSEL_18         (18*0x0001u) /* DMA channel 0 transfer select 18 */
#define DMA0TSEL_19         (19*0x0001u) /* DMA channel 0 transfer select 19 */
#define DMA0TSEL_20         (20*0x0001u) /* DMA channel 0 transfer select 20 */
#define DMA0TSEL_21         (21*0x0001u) /* DMA channel 0 transfer select 21 */
#define DMA0TSEL_22         (22*0x0001u) /* DMA channel 0 transfer select 22 */
#define DMA0TSEL_23         (23*0x0001u) /* DMA channel 0 transfer select 23 */
#define DMA0TSEL_24         (24*0x0001u) /* DMA channel 0 transfer select 24 */
#define DMA0TSEL_25         (25*0x0001u) /* DMA channel 0 transfer select 25 */
#define DMA0TSEL_26         (26*0x0001u) /* DMA channel 0 transfer select 26 */
#define DMA0TSEL_27         (27*0x0001u) /* DMA channel 0 transfer select 27 */
#define DMA0TSEL_28         (28*0x0001u) /* DMA channel 0 transfer select 28 */
#define DMA0TSEL_29         (29*0x0001u) /* DMA channel 0 transfer select 29 */
#define DMA0TSEL_30         (30*0x0001u) /* DMA channel 0 transfer select 30 */
#define DMA0TSEL_31         (31*0x0001u) /* DMA channel 0 transfer select 31 */

#define DMA1TSEL_0          (0*0x0100u)  /* DMA channel 1 transfer select 0  */
#define DMA1TSEL_1          (1*0x0100u)  /* DMA channel 1 transfer select 1  */
#define DMA1TSEL_2          (2*0x0100u)  /* DMA channel 1 transfer select 2  */
#define DMA1TSEL_3          (3*0x0100u)  /* DMA channel 1 transfer select 3  */
#define DMA1TSEL_4          (4*0x0100u)  /* DMA channel 1 transfer select 4  */
#define DMA1TSEL_5          (5*0x0100u)  /* DMA channel 1 transfer select 5  */
#define DMA1TSEL_6          (6*0x0100u)  /* DMA channel 1 transfer select 6  */
#define DMA1TSEL_7          (7*0x0100u)  /* DMA channel 1 transfer select 7  */
#define DMA1TSEL_8          (8*0x0100u)  /* DMA channel 1 transfer select 8  */
#define DMA1TSEL_9          (9*0x0100u)  /* DMA channel 1 transfer select 9  */
#define DMA1TSEL_10         (10*0x0100u) /* DMA channel 1 transfer select 10 */
#define DMA1TSEL_11         (11*0x0100u) /* DMA channel 1 transfer select 11 */
#define DMA1TSEL_12         (12*0x0100u) /* DMA channel 1 transfer select 12 */
#define DMA1TSEL_13         (13*0x0100u) /* DMA channel 1 transfer select 13 */
#define DMA1TSEL_14         (14*0x0100u) /* DMA channel 1 transfer select 14 */
#define DMA1TSEL_15         (15*0x0100u) /* DMA channel 1 transfer select 15 */
#define DMA1TSEL_16         (16*0x0100u) /* DMA channel 1 transfer select 16 */
#define DMA1TSEL_17         (17*0x0100u) /* DMA channel 1 transfer select 17 */
#define DMA1TSEL_18         (18*0x0100u) /* DMA channel 1 transfer select 18 */
#define DMA1TSEL_19         (19*0x0100u) /* DMA channel 1 transfer select 19 */
#define DMA1TSEL_20         (20*0x0100u) /* DMA channel 1 transfer select 20 */
#define DMA1TSEL_21         (21*0x0100u) /* DMA channel 1 transfer select 21 */
#define DMA1TSEL_22         (22*0x0100u) /* DMA channel 1 transfer select 22 */
#define DMA1TSEL_23         (23*0x0100u) /* DMA channel 1 transfer select 23 */
#define DMA1TSEL_24         (24*0x0100u) /* DMA channel 1 transfer select 24 */
#define DMA1TSEL_25         (25*0x0100u) /* DMA channel 1 transfer select 25 */
#define DMA1TSEL_26         (26*0x0100u) /* DMA channel 1 transfer select 26 */
#define DMA1TSEL_27         (27*0x0100u) /* DMA channel 1 transfer select 27 */
#define DMA1TSEL_28         (28*0x0100u) /* DMA channel 1 transfer select 28 */
#define DMA1TSEL_29         (29*0x0100u) /* DMA channel 1 transfer select 29 */
#define DMA1TSEL_30         (30*0x0100u) /* DMA channel 1 transfer select 30 */
#define DMA1TSEL_31         (31*0x0100u) /* DMA channel 1 transfer select 31 */

#define DMA2TSEL_0          (0*0x0001u)  /* DMA channel 2 transfer select 0  */
#define DMA2TSEL_1          (1*0x0001u)  /* DMA channel 2 transfer select 1  */
#define DMA2TSEL_2          (2*0x0001u)  /* DMA channel 2 transfer select 2  */
#define DMA2TSEL_3          (3*0x0001u)  /* DMA channel 2 transfer select 3  */
#define DMA2TSEL_4          (4*0x0001u)  /* DMA channel 2 transfer select 4  */
#define DMA2TSEL_5          (5*0x0001u)  /* DMA channel 2 transfer select 5  */
#define DMA2TSEL_6          (6*0x0001u)  /* DMA channel 2 transfer select 6  */
#define DMA2TSEL_7          (7*0x0001u)  /* DMA channel 2 transfer select 7  */
#define DMA2TSEL_8          (8*0x0001u)  /* DMA channel 2 transfer select 8  */
#define DMA2TSEL_9          (9*0x0001u)  /* DMA channel 2 transfer select 9  */
#define DMA2TSEL_10         (10*0x0001u) /* DMA channel 2 transfer select 10 */
#define DMA2TSEL_11         (11*0x0001u) /* DMA channel 2 transfer select 11 */
#define DMA2TSEL_12         (12*0x0001u) /* DMA channel 2 transfer select 12 */
#define DMA2TSEL_13         (13*0x0001u) /* DMA channel 2 transfer select 13 */
#define DMA2TSEL_14         (14*0x0001u) /* DMA channel 2 transfer select 14 */
#define DMA2TSEL_15         (15*0x0001u) /* DMA channel 2 transfer select 15 */
#define DMA2TSEL_16         (16*0x0001u) /* DMA channel 2 transfer select 16 */
#define DMA2TSEL_17         (17*0x0001u) /* DMA channel 2 transfer select 17 */
#define DMA2TSEL_18         (18*0x0001u) /* DMA channel 2 transfer select 18 */
#define DMA2TSEL_19         (19*0x0001u) /* DMA channel 2 transfer select 19 */
#define DMA2TSEL_20         (20*0x0001u) /* DMA channel 2 transfer select 20 */
#define DMA2TSEL_21         (21*0x0001u) /* DMA channel 2 transfer select 21 */
#define DMA2TSEL_22         (22*0x0001u) /* DMA channel 2 transfer select 22 */
#define DMA2TSEL_23         (23*0x0001u) /* DMA channel 2 transfer select 23 */
#define DMA2TSEL_24         (24*0x0001u) /* DMA channel 2 transfer select 24 */
#define DMA2TSEL_25         (25*0x0001u) /* DMA channel 2 transfer select 25 */
#define DMA2TSEL_26         (26*0x0001u) /* DMA channel 2 transfer select 26 */
#define DMA2TSEL_27         (27*0x0001u) /* DMA channel 2 transfer select 27 */
#define DMA2TSEL_28         (28*0x0001u) /* DMA channel 2 transfer select 28 */
#define DMA2TSEL_29         (29*0x0001u) /* DMA channel 2 transfer select 29 */
#define DMA2TSEL_30         (30*0x0001u) /* DMA channel 2 transfer select 30 */
#define DMA2TSEL_31         (31*0x0001u) /* DMA channel 2 transfer select 31 */

#define DMA0TSEL__DMA_REQ   (0*0x0001u)  /* DMA channel 0 transfer select 0:  DMA_REQ (sw) */
#define DMA0TSEL__TA0CCR0   (1*0x0001u)  /* DMA channel 0 transfer select 1:  Timer0_A (TA0CCR0.IFG) */
#define DMA0TSEL__TA0CCR2   (2*0x0001u)  /* DMA channel 0 transfer select 2:  Timer0_A (TA0CCR2.IFG) */
#define DMA0TSEL__TA1CCR0   (3*0x0001u)  /* DMA channel 0 transfer select 3:  Timer1_A (TA1CCR0.IFG) */
#define DMA0TSEL__TA1CCR2   (4*0x0001u)  /* DMA channel 0 transfer select 4:  Timer1_A (TA1CCR2.IFG) */
#define DMA0TSEL__TB0CCR0   (5*0x0001u)  /* DMA channel 0 transfer select 5:  TimerB (TB0CCR0.IFG) */
#define DMA0TSEL__TB0CCR2   (6*0x0001u)  /* DMA channel 0 transfer select 6:  TimerB (TB0CCR2.IFG) */
#define DMA0TSEL__RES7      (7*0x0001u)  /* DMA channel 0 transfer select 7:  Reserved */
#define DMA0TSEL__RES8      (8*0x0001u)  /* DMA channel 0 transfer select 8:  Reserved */
#define DMA0TSEL__RES9      (9*0x0001u)  /* DMA channel 0 transfer select 9:  Reserved */
#define DMA0TSEL__RES10     (10*0x0001u) /* DMA channel 0 transfer select 10: Reserved */
#define DMA0TSEL__RES11     (11*0x0001u) /* DMA channel 0 transfer select 11: Reserved */
#define DMA0TSEL__RES12     (12*0x0001u) /* DMA channel 0 transfer select 12: Reserved */
#define DMA0TSEL__RES13     (13*0x0001u) /* DMA channel 0 transfer select 13: Reserved */
#define DMA0TSEL__RES14     (14*0x0001u) /* DMA channel 0 transfer select 14: Reserved */
#define DMA0TSEL__RES15     (15*0x0001u) /* DMA channel 0 transfer select 15: Reserved */
#define DMA0TSEL__USCIA0RX  (16*0x0001u) /* DMA channel 0 transfer select 16: USCIA0 receive */
#define DMA0TSEL__USCIA0TX  (17*0x0001u) /* DMA channel 0 transfer select 17: USCIA0 transmit */
#define DMA0TSEL__USCIB0RX  (18*0x0001u) /* DMA channel 0 transfer select 18: USCIB0 receive */
#define DMA0TSEL__USCIB0TX  (19*0x0001u) /* DMA channel 0 transfer select 19: USCIB0 transmit */
#define DMA0TSEL__USCIA1RX  (20*0x0001u) /* DMA channel 0 transfer select 20: USCIA1 receive */
#define DMA0TSEL__USCIA1TX  (21*0x0001u) /* DMA channel 0 transfer select 21: USCIA1 transmit */
#define DMA0TSEL__USCIB1RX  (22*0x0001u) /* DMA channel 0 transfer select 22: USCIB1 receive */
#define DMA0TSEL__USCIB1TX  (23*0x0001u) /* DMA channel 0 transfer select 23: USCIB1 transmit */
#define DMA0TSEL__ADC10IFG  (24*0x0001u) /* DMA channel 0 transfer select 24: ADC10IFGx */
#define DMA0TSEL__RES25     (25*0x0001u) /* DMA channel 0 transfer select 25: Reserved */
#define DMA0TSEL__RES26     (26*0x0001u) /* DMA channel 0 transfer select 26: Reserved */
#define DMA0TSEL__RES27     (27*0x0001u) /* DMA channel 0 transfer select 27: Reserved */
#define DMA0TSEL__RES28     (28*0x0001u) /* DMA channel 0 transfer select 28: Reserved */
#define DMA0TSEL__MPY       (29*0x0001u) /* DMA channel 0 transfer select 29: Multiplier ready */
#define DMA0TSEL__DMA2IFG   (30*0x0001u) /* DMA channel 0 transfer select 30: previous DMA channel DMA2IFG */
#define DMA0TSEL__DMAE0     (31*0x0001u) /* DMA channel 0 transfer select 31: ext. Trigger (DMAE0) */

#define DMA1TSEL__DMA_REQ   (0*0x0100u)  /* DMA channel 1 transfer select 0:  DMA_REQ (sw) */
#define DMA1TSEL__TA0CCR0   (1*0x0100u)  /* DMA channel 1 transfer select 1:  Timer0_A (TA0CCR0.IFG) */
#define DMA1TSEL__TA0CCR2   (2*0x0100u)  /* DMA channel 1 transfer select 2:  Timer0_A (TA0CCR2.IFG) */
#define DMA1TSEL__TA1CCR0   (3*0x0100u)  /* DMA channel 1 transfer select 3:  Timer1_A (TA1CCR0.IFG) */
#define DMA1TSEL__TA1CCR2   (4*0x0100u)  /* DMA channel 1 transfer select 4:  Timer1_A (TA1CCR2.IFG) */
#define DMA1TSEL__TB0CCR0   (5*0x0100u)  /* DMA channel 1 transfer select 5:  TimerB (TB0CCR0.IFG) */
#define DMA1TSEL__TB0CCR2   (6*0x0100u)  /* DMA channel 1 transfer select 6:  TimerB (TB0CCR2.IFG) */
#define DMA1TSEL__RES7      (7*0x0100u)  /* DMA channel 1 transfer select 7:  Reserved */
#define DMA1TSEL__RES8      (8*0x0100u)  /* DMA channel 1 transfer select 8:  Reserved */
#define DMA1TSEL__RES9      (9*0x0100u)  /* DMA channel 1 transfer select 9:  Reserved */
#define DMA1TSEL__RES10     (10*0x0100u) /* DMA channel 1 transfer select 10: Reserved */
#define DMA1TSEL__RES11     (11*0x0100u) /* DMA channel 1 transfer select 11: Reserved */
#define DMA1TSEL__RES12     (12*0x0100u) /* DMA channel 1 transfer select 12: Reserved */
#define DMA1TSEL__RES13     (13*0x0100u) /* DMA channel 1 transfer select 13: Reserved */
#define DMA1TSEL__RES14     (14*0x0100u) /* DMA channel 1 transfer select 14: Reserved */
#define DMA1TSEL__RES15     (15*0x0100u) /* DMA channel 1 transfer select 15: Reserved */
#define DMA1TSEL__USCIA0RX  (16*0x0100u) /* DMA channel 1 transfer select 16: USCIA0 receive */
#define DMA1TSEL__USCIA0TX  (17*0x0100u) /* DMA channel 1 transfer select 17: USCIA0 transmit */
#define DMA1TSEL__USCIB0RX  (18*0x0100u) /* DMA channel 1 transfer select 18: USCIB0 receive */
#define DMA1TSEL__USCIB0TX  (19*0x0100u) /* DMA channel 1 transfer select 19: USCIB0 transmit */
#define DMA1TSEL__USCIA1RX  (20*0x0100u) /* DMA channel 1 transfer select 20: USCIA1 receive */
#define DMA1TSEL__USCIA1TX  (21*0x0100u) /* DMA channel 1 transfer select 21: USCIA1 transmit */
#define DMA1TSEL__USCIB1RX  (22*0x0100u) /* DMA channel 1 transfer select 22: USCIB1 receive */
#define DMA1TSEL__USCIB1TX  (23*0x0100u) /* DMA channel 1 transfer select 23: USCIB1 transmit */
#define DMA1TSEL__ADC10IFG  (24*0x0100u) /* DMA channel 1 transfer select 24: ADC10IFGx */
#define DMA1TSEL__RES25     (25*0x0100u) /* DMA channel 1 transfer select 25: Reserved */
#define DMA1TSEL__RES26     (26*0x0100u) /* DMA channel 1 transfer select 26: Reserved */
#define DMA1TSEL__RES27     (27*0x0100u) /* DMA channel 1 transfer select 27: Reserved */
#define DMA1TSEL__RES28     (28*0x0100u) /* DMA channel 1 transfer select 28: Reserved */
#define DMA1TSEL__MPY       (29*0x0100u) /* DMA channel 1 transfer select 29: Multiplier ready */
#define DMA1TSEL__DMA0IFG   (30*0x0100u) /* DMA channel 1 transfer select 30: previous DMA channel DMA0IFG */
#define DMA1TSEL__DMAE0     (31*0x0100u) /* DMA channel 1 transfer select 31: ext. Trigger (DMAE0) */

#define DMA2TSEL__DMA_REQ   (0*0x0001u)  /* DMA channel 2 transfer select 0:  DMA_REQ (sw) */
#define DMA2TSEL__TA0CCR0   (1*0x0001u)  /* DMA channel 2 transfer select 1:  Timer0_A (TA0CCR0.IFG) */
#define DMA2TSEL__TA0CCR2   (2*0x0001u)  /* DMA channel 2 transfer select 2:  Timer0_A (TA0CCR2.IFG) */
#define DMA2TSEL__TA1CCR0   (3*0x0001u)  /* DMA channel 2 transfer select 3:  Timer1_A (TA1CCR0.IFG) */
#define DMA2TSEL__TA1CCR2   (4*0x0001u)  /* DMA channel 2 transfer select 4:  Timer1_A (TA1CCR2.IFG) */
#define DMA2TSEL__TB0CCR0   (5*0x0001u)  /* DMA channel 2 transfer select 5:  TimerB (TB0CCR0.IFG) */
#define DMA2TSEL__TB0CCR2   (6*0x0001u)  /* DMA channel 2 transfer select 6:  TimerB (TB0CCR2.IFG) */
#define DMA2TSEL__RES7      (7*0x0001u)  /* DMA channel 2 transfer select 7:  Reserved */
#define DMA2TSEL__RES8      (8*0x0001u)  /* DMA channel 2 transfer select 8:  Reserved */
#define DMA2TSEL__RES9      (9*0x0001u)  /* DMA channel 2 transfer select 9:  Reserved */
#define DMA2TSEL__RES10     (10*0x0001u) /* DMA channel 2 transfer select 10: Reserved */
#define DMA2TSEL__RES11     (11*0x0001u) /* DMA channel 2 transfer select 11: Reserved */
#define DMA2TSEL__RES12     (12*0x0001u) /* DMA channel 2 transfer select 12: Reserved */
#define DMA2TSEL__RES13     (13*0x0001u) /* DMA channel 2 transfer select 13: Reserved */
#define DMA2TSEL__RES14     (14*0x0001u) /* DMA channel 2 transfer select 14: Reserved */
#define DMA2TSEL__RES15     (15*0x0001u) /* DMA channel 2 transfer select 15: Reserved */
#define DMA2TSEL__USCIA0RX  (16*0x0001u) /* DMA channel 2 transfer select 16: USCIA0 receive */
#define DMA2TSEL__USCIA0TX  (17*0x0001u) /* DMA channel 2 transfer select 17: USCIA0 transmit */
#define DMA2TSEL__USCIB0RX  (18*0x0001u) /* DMA channel 2 transfer select 18: USCIB0 receive */
#define DMA2TSEL__USCIB0TX  (19*0x0001u) /* DMA channel 2 transfer select 19: USCIB0 transmit */
#define DMA2TSEL__USCIA1RX  (20*0x0001u) /* DMA channel 2 transfer select 20: USCIA1 receive */
#define DMA2TSEL__USCIA1TX  (21*0x0001u) /* DMA channel 2 transfer select 21: USCIA1 transmit */
#define DMA2TSEL__USCIB1RX  (22*0x0001u) /* DMA channel 2 transfer select 22: USCIB1 receive */
#define DMA2TSEL__USCIB1TX  (23*0x0001u) /* DMA channel 2 transfer select 23: USCIB1 transmit */
#define DMA2TSEL__ADC10IFG  (24*0x0001u) /* DMA channel 2 transfer select 24: ADC10IFGx */
#define DMA2TSEL__RES25     (25*0x0001u) /* DMA channel 2 transfer select 25: Reserved */
#define DMA2TSEL__RES26     (26*0x0001u) /* DMA channel 2 transfer select 26: Reserved */
#define DMA2TSEL__RES27     (27*0x0001u) /* DMA channel 2 transfer select 27: Reserved */
#define DMA2TSEL__RES28     (28*0x0001u) /* DMA channel 2 transfer select 28: Reserved */
#define DMA2TSEL__MPY       (29*0x0001u) /* DMA channel 2 transfer select 29: Multiplier ready */
#define DMA2TSEL__DMA1IFG   (30*0x0001u) /* DMA channel 2 transfer select 30: previous DMA channel DMA1IFG */
#define DMA2TSEL__DMAE0     (31*0x0001u) /* DMA channel 2 transfer select 31: ext. Trigger (DMAE0) */

/*-------------------------------------------------------------------------
 *   Flash
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short FCTL1;   /* FLASH Control 1 */

  struct
  {
    unsigned short                : 1;
    unsigned short ERASE           : 1; /* Enable bit for Flash segment erase */
    unsigned short MERAS           : 1; /* Enable bit for Flash mass erase */
    unsigned short                : 2;
    unsigned short SWRT            : 1; /* Smart Write enable */
    unsigned short WRT             : 1; /* Enable bit for Flash write */
    unsigned short BLKWRT          : 1; /* Enable bit for Flash segment write */
  }FCTL1_bit;
} @0x0140;


enum {
  ERASE           = 0x0002,
  MERAS           = 0x0004,
  SWRT            = 0x0020,
  WRT             = 0x0040,
  BLKWRT          = 0x0080
};


__no_init volatile union
{
  unsigned short FCTL3;   /* FLASH Control 3 */

  struct
  {
    unsigned short BUSY            : 1; /* Flash busy: 1 */
    unsigned short KEYV            : 1; /* Flash Key violation flag */
    unsigned short ACCVIFG         : 1; /* Flash Access violation flag */
    unsigned short WAIT            : 1; /* Wait flag for segment write */
    unsigned short LOCK            : 1; /* Lock bit: 1 - Flash is locked (read only) */
    unsigned short EMEX            : 1; /* Flash Emergency Exit */
    unsigned short LOCKA           : 1; /* Segment A Lock bit: read = 1 - Segment is locked (read only) */
  }FCTL3_bit;
} @0x0144;


enum {
  BUSY            = 0x0001,
  KEYV            = 0x0002,
  ACCVIFG         = 0x0004,
  WAIT            = 0x0008,
  LOCK            = 0x0010,
  EMEX            = 0x0020,
  LOCKA           = 0x0040
};


__no_init volatile union
{
  unsigned short FCTL4;   /* FLASH Control 4 */

  struct
  {
    unsigned short VPE             : 1; /* Voltage Changed during Program Error Flag */
    unsigned short                : 3;
    unsigned short MGR0            : 1; /* Marginal read 0 mode. */
    unsigned short MGR1            : 1; /* Marginal read 1 mode. */
    unsigned short                : 1;
    unsigned short LOCKINFO        : 1; /* Lock INFO Memory bit: read = 1 - Segment is locked (read only) */
  }FCTL4_bit;
} @ 0x0146;


enum {
  VPE             = 0x0001,
  MGR0            = 0x0010,
  MGR1            = 0x0020,
  LOCKINFO        = 0x0080
};


#define __MSP430_HAS_FLASH__         /* Definition to show that Module is available */


#define FRPW                (0x9600u)  /* Flash password returned by read */
#define FWPW                (0xA500u)  /* Flash password for write */
#define FXPW                (0x3300u)  /* for use with XOR instruction */
#define FRKEY               (0x9600u)  /* (legacy definition) Flash key returned by read */
#define FWKEY               (0xA500u)  /* (legacy definition) Flash key for write */
#define FXKEY               (0x3300u)  /* (legacy definition) for use with XOR instruction */
#define ERASE_L             (0x0002u)  /* Enable bit for Flash segment erase */
#define MERAS_L             (0x0004u)  /* Enable bit for Flash mass erase */
#define SWRT_L              (0x0020u)  /* Smart Write enable */
#define WRT_L               (0x0040u)  /* Enable bit for Flash write */
#define BLKWRT_L            (0x0080u)  /* Enable bit for Flash segment write */

/* FCTL3 Control Bits */
#define BUSY_L              (0x0001u)  /* Flash busy: 1 */
#define KEYV_L              (0x0002u)  /* Flash Key violation flag */
#define ACCVIFG_L           (0x0004u)  /* Flash Access violation flag */
#define WAIT_L              (0x0008u)  /* Wait flag for segment write */
#define LOCK_L              (0x0010u)  /* Lock bit: 1 - Flash is locked (read only) */
#define EMEX_L              (0x0020u)  /* Flash Emergency Exit */
#define LOCKA_L             (0x0040u)  /* Segment A Lock bit: read = 1 - Segment is locked (read only) */

/* FCTL4 Control Bits */
#define VPE_L               (0x0001u)  /* Voltage Changed during Program Error Flag */
#define MGR0_L              (0x0010u)  /* Marginal read 0 mode. */
#define MGR1_L              (0x0020u)  /* Marginal read 1 mode. */
#define LOCKINFO_L          (0x0080u)  /* Lock INFO Memory bit: read = 1 - Segment is locked (read only) */

/*-------------------------------------------------------------------------
 *   MPY 16  Multiplier  16 Bit Mode
 *-------------------------------------------------------------------------*/


  /* Multiply Unsigned/Operand 1 */
__no_init volatile unsigned short MPY @ 0x04C0;


  /* Multiply Signed/Operand 1 */
__no_init volatile unsigned short MPYS @ 0x04C2;


  /* Multiply Unsigned and Accumulate/Operand 1 */
__no_init volatile unsigned short MAC @ 0x04C4;


  /* Multiply Signed and Accumulate/Operand 1 */
__no_init volatile unsigned short MACS @ 0x04C6;


  /* Operand 2 */
__no_init volatile unsigned short OP2 @ 0x04C8;


  /* Result Low Word */
__no_init volatile unsigned short RESLO @ 0x04CA;


  /* Result High Word */
__no_init volatile unsigned short RESHI @ 0x04CC;


  /* Sum Extend */
__no_init volatile unsigned __READ short SUMEXT @ 0x04CE;


__no_init volatile union
{
  unsigned short MPY32CTL0;   /* MPY32 Control Register 0 */

  struct
  {
    unsigned short MPYC            : 1; /* Carry of the multiplier */
    unsigned short                : 1;
    unsigned short MPYFRAC         : 1; /* Fractional mode */
    unsigned short MPYSAT          : 1; /* Saturation mode */
    unsigned short MPYM0           : 1; /* Multiplier mode Bit:0 */
    unsigned short MPYM1           : 1; /* Multiplier mode Bit:1 */
    unsigned short OP1_32          : 1; /*  */
    unsigned short OP2_32          : 1; /*  */
    unsigned short MPYDLYWRTEN     : 1; /* Delayed write enable */
    unsigned short MPYDLY32        : 1; /* Delayed write mode */
  }MPY32CTL0_bit;
} @ 0x04EC;


enum {
  MPYC            = 0x0001,
  MPYFRAC         = 0x0004,
  MPYSAT          = 0x0008,
  MPYM0           = 0x0010,
  MPYM1           = 0x0020,
  OP1_32          = 0x0040,
  OP2_32          = 0x0080,
  MPYDLYWRTEN     = 0x0100,
  MPYDLY32        = 0x0200
};


#define __MSP430_HAS_MPY32__          /* Definition to show that Module is available */


#define  MPY32L_             (0x04D0u)  /* 32-bit operand 1 - multiply - low word */
#define  MPY32H_             (0x04D2u)  /* 32-bit operand 1 - multiply - high word */
#define  MPYS32L_            (0x04D4u)  /* 32-bit operand 1 - signed multiply - low word */
#define  MPYS32H_            (0x04D6u)  /* 32-bit operand 1 - signed multiply - high word */
#define  MAC32L_             (0x04D8u)  /* 32-bit operand 1 - multiply accumulate - low word */
#define  MAC32H_             (0x04DAu)  /* 32-bit operand 1 - multiply accumulate - high word */
#define  MACS32L_            (0x04DCu)  /* 32-bit operand 1 - signed multiply accumulate - low word */
#define  MACS32H_            (0x04DEu)  /* 32-bit operand 1 - signed multiply accumulate - high word */
#define  OP2L_               (0x04E0u)  /* 32-bit operand 2 - low word */
#define  OP2H_               (0x04E2u)  /* 32-bit operand 2 - high word */
#define  RES0_               (0x04E4u)  /* 32x32-bit result 0 - least significant word */
#define  RES1_               (0x04E6u)  /* 32x32-bit result 1 */
#define  RES2_               (0x04E8u)  /* 32x32-bit result 2 */
#define  RES3_               (0x04EAu)  /* 32x32-bit result 3 - most significant word */

#define MPY_B               MPY_L      /* Multiply Unsigned/Operand 1 (Byte Access) */
#define MPYS_B              MPYS_L     /* Multiply Signed/Operand 1 (Byte Access) */
#define MAC_B               MAC_L      /* Multiply Unsigned and Accumulate/Operand 1 (Byte Access) */
#define MACS_B              MACS_L     /* Multiply Signed and Accumulate/Operand 1 (Byte Access) */
#define OP2_B               OP2_L      /* Operand 2 (Byte Access) */
#define MPY32L_B            MPY32L_L   /* 32-bit operand 1 - multiply - low word (Byte Access) */
#define MPY32H_B            MPY32H_L   /* 32-bit operand 1 - multiply - high word (Byte Access) */
#define MPYS32L_B           MPYS32L_L  /* 32-bit operand 1 - signed multiply - low word (Byte Access) */
#define MPYS32H_B           MPYS32H_L  /* 32-bit operand 1 - signed multiply - high word (Byte Access) */
#define MAC32L_B            MAC32L_L   /* 32-bit operand 1 - multiply accumulate - low word (Byte Access) */
#define MAC32H_B            MAC32H_L   /* 32-bit operand 1 - multiply accumulate - high word (Byte Access) */
#define MACS32L_B           MACS32L_L  /* 32-bit operand 1 - signed multiply accumulate - low word (Byte Access) */
#define MACS32H_B           MACS32H_L  /* 32-bit operand 1 - signed multiply accumulate - high word (Byte Access) */
#define OP2L_B              OP2L_L     /* 32-bit operand 2 - low word (Byte Access) */
#define OP2H_B              OP2H_L     /* 32-bit operand 2 - high word (Byte Access) */

/* MPY32CTL0 Control Bits */
#define MPYC_L              (0x0001u)  /* Carry of the multiplier */
#define MPYFRAC_L           (0x0004u)  /* Fractional mode */
#define MPYSAT_L            (0x0008u)  /* Saturation mode */
#define MPYM0_L             (0x0010u)  /* Multiplier mode Bit:0 */
#define MPYM1_L             (0x0020u)  /* Multiplier mode Bit:1 */
#define OP1_32_L            (0x0040u)  /* Bit-width of operand 1 0:16Bit / 1:32Bit */
#define OP2_32_L            (0x0080u)  /* Bit-width of operand 2 0:16Bit / 1:32Bit */
#define MPYDLYWRTEN_H       (0x0001u)  /* Delayed write enable */
#define MPYDLY32_H          (0x0002u)  /* Delayed write mode */

#define MPYM_0              (0x0000u)  /* Multiplier mode: MPY */
#define MPYM_1              (0x0010u)  /* Multiplier mode: MPYS */
#define MPYM_2              (0x0020u)  /* Multiplier mode: MAC */
#define MPYM_3              (0x0030u)  /* Multiplier mode: MACS */
#define MPYM__MPY           (0x0000u)  /* Multiplier mode: MPY */
#define MPYM__MPYS          (0x0010u)  /* Multiplier mode: MPYS */
#define MPYM__MAC           (0x0020u)  /* Multiplier mode: MAC */
#define MPYM__MACS          (0x0030u)  /* Multiplier mode: MACS */

/*-------------------------------------------------------------------------
 *   MPY 32  Multiplier  32 Bit Mode
 *-------------------------------------------------------------------------*/


  /* 32-bit operand 1 - multiply - low word */
__no_init volatile unsigned short MPY32L @ 0x04D0;


  /* 32-bit operand 1 - multiply - high word */
__no_init volatile unsigned short MPY32H @ 0x04D2;


  /* 32-bit operand 1 - signed multiply - low word */
__no_init volatile unsigned short MPYS32L @ 0x04D4;


  /* 32-bit operand 1 - signed multiply - high word */
__no_init volatile unsigned short MPYS32H @ 0x04D6;


  /* 32-bit operand 1 - multiply accumulate - low word */
__no_init volatile unsigned short MAC32L @ 0x04D8;


  /* 32-bit operand 1 - multiply accumulate - high word */
__no_init volatile unsigned short MAC32H @ 0x04DA;


  /* 32-bit operand 1 - signed multiply accumulate - low word */
__no_init volatile unsigned short MACS32L @ 0x04DC;


  /* 32-bit operand 1 - signed multiply accumulate - high word */
__no_init volatile unsigned short MACS32H @ 0x04DE;


  /* 32-bit operand 2 - low word */
__no_init volatile unsigned short OP2L @ 0x04E0;


  /* 32-bit operand 2 - high word */
__no_init volatile unsigned short OP2H @ 0x04E2;


  /* 32x32-bit result 0 - least significant word */
__no_init volatile unsigned short RES0 @ 0x04E4;


  /* 32x32-bit result 1 */
__no_init volatile unsigned short RES1 @ 0x04E6;


  /* 32x32-bit result 2 */
__no_init volatile unsigned short RES2 @ 0x04E8;


  /* 32x32-bit result 3 - most significant word */
__no_init volatile unsigned short RES3 @ 0x04EA;


#define __MSP430_HAS_MPY32__          /* Definition to show that Module is available */

#define  MPY32CTL0_          (0x04ECu)  /* MPY32 Control Register 0 */

#define MPY_B               MPY_L      /* Multiply Unsigned/Operand 1 (Byte Access) */
#define MPYS_B              MPYS_L     /* Multiply Signed/Operand 1 (Byte Access) */
#define MAC_B               MAC_L      /* Multiply Unsigned and Accumulate/Operand 1 (Byte Access) */
#define MACS_B              MACS_L     /* Multiply Signed and Accumulate/Operand 1 (Byte Access) */
#define OP2_B               OP2_L      /* Operand 2 (Byte Access) */
#define MPY32L_B            MPY32L_L   /* 32-bit operand 1 - multiply - low word (Byte Access) */
#define MPY32H_B            MPY32H_L   /* 32-bit operand 1 - multiply - high word (Byte Access) */
#define MPYS32L_B           MPYS32L_L  /* 32-bit operand 1 - signed multiply - low word (Byte Access) */
#define MPYS32H_B           MPYS32H_L  /* 32-bit operand 1 - signed multiply - high word (Byte Access) */
#define MAC32L_B            MAC32L_L   /* 32-bit operand 1 - multiply accumulate - low word (Byte Access) */
#define MAC32H_B            MAC32H_L   /* 32-bit operand 1 - multiply accumulate - high word (Byte Access) */
#define MACS32L_B           MACS32L_L  /* 32-bit operand 1 - signed multiply accumulate - low word (Byte Access) */
#define MACS32H_B           MACS32H_L  /* 32-bit operand 1 - signed multiply accumulate - high word (Byte Access) */
#define OP2L_B              OP2L_L     /* 32-bit operand 2 - low word (Byte Access) */
#define OP2H_B              OP2H_L     /* 32-bit operand 2 - high word (Byte Access) */

/* MPY32CTL0 Control Bits */
#define MPYC_L              (0x0001u)  /* Carry of the multiplier */
#define MPYFRAC_L           (0x0004u)  /* Fractional mode */
#define MPYSAT_L            (0x0008u)  /* Saturation mode */
#define MPYM0_L             (0x0010u)  /* Multiplier mode Bit:0 */
#define MPYM1_L             (0x0020u)  /* Multiplier mode Bit:1 */
#define OP1_32_L            (0x0040u)  /* Bit-width of operand 1 0:16Bit / 1:32Bit */
#define OP2_32_L            (0x0080u)  /* Bit-width of operand 2 0:16Bit / 1:32Bit */
#define MPYDLYWRTEN_H       (0x0001u)  /* Delayed write enable */
#define MPYDLY32_H          (0x0002u)  /* Delayed write mode */

#define MPYM_0              (0x0000u)  /* Multiplier mode: MPY */
#define MPYM_1              (0x0010u)  /* Multiplier mode: MPYS */
#define MPYM_2              (0x0020u)  /* Multiplier mode: MAC */
#define MPYM_3              (0x0030u)  /* Multiplier mode: MACS */
#define MPYM__MPY           (0x0000u)  /* Multiplier mode: MPY */
#define MPYM__MPYS          (0x0010u)  /* Multiplier mode: MPYS */
#define MPYM__MAC           (0x0020u)  /* Multiplier mode: MAC */
#define MPYM__MACS          (0x0030u)  /* Multiplier mode: MACS */

/*-------------------------------------------------------------------------
 *   Port A
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned __READ short PAIN;   /* Port A Input */

  struct
  {
    unsigned __READ short PAIN0           : 1; /*  */
    unsigned __READ short PAIN1           : 1; /*  */
    unsigned __READ short PAIN2           : 1; /*  */
    unsigned __READ short PAIN3           : 1; /*  */
    unsigned __READ short PAIN4           : 1; /*  */
    unsigned __READ short PAIN5           : 1; /*  */
    unsigned __READ short PAIN6           : 1; /*  */
    unsigned __READ short PAIN7           : 1; /*  */
    unsigned __READ short PAIN8           : 1; /*  */
    unsigned __READ short PAIN9           : 1; /*  */
    unsigned __READ short PAIN10          : 1; /*  */
    unsigned __READ short PAIN11          : 1; /*  */
    unsigned __READ short PAIN12          : 1; /*  */
    unsigned __READ short PAIN13          : 1; /*  */
    unsigned __READ short PAIN14          : 1; /*  */
    unsigned __READ short PAIN15          : 1; /*  */
  }PAIN_bit;
} @0x0200;


enum {
  PAIN0           = 0x0001,
  PAIN1           = 0x0002,
  PAIN2           = 0x0004,
  PAIN3           = 0x0008,
  PAIN4           = 0x0010,
  PAIN5           = 0x0020,
  PAIN6           = 0x0040,
  PAIN7           = 0x0080,
  PAIN8           = 0x0100,
  PAIN9           = 0x0200,
  PAIN10          = 0x0400,
  PAIN11          = 0x0800,
  PAIN12          = 0x1000,
  PAIN13          = 0x2000,
  PAIN14          = 0x4000,
  PAIN15          = 0x8000
};


__no_init volatile union
{
  unsigned short PAOUT;   /* Port A Output */

  struct
  {
    unsigned short PAOUT0          : 1; /*  */
    unsigned short PAOUT1          : 1; /*  */
    unsigned short PAOUT2          : 1; /*  */
    unsigned short PAOUT3          : 1; /*  */
    unsigned short PAOUT4          : 1; /*  */
    unsigned short PAOUT5          : 1; /*  */
    unsigned short PAOUT6          : 1; /*  */
    unsigned short PAOUT7          : 1; /*  */
    unsigned short PAOUT8          : 1; /*  */
    unsigned short PAOUT9          : 1; /*  */
    unsigned short PAOUT10         : 1; /*  */
    unsigned short PAOUT11         : 1; /*  */
    unsigned short PAOUT12         : 1; /*  */
    unsigned short PAOUT13         : 1; /*  */
    unsigned short PAOUT14         : 1; /*  */
    unsigned short PAOUT15         : 1; /*  */
  }PAOUT_bit;
} @0x0202;


enum {
  PAOUT0          = 0x0001,
  PAOUT1          = 0x0002,
  PAOUT2          = 0x0004,
  PAOUT3          = 0x0008,
  PAOUT4          = 0x0010,
  PAOUT5          = 0x0020,
  PAOUT6          = 0x0040,
  PAOUT7          = 0x0080,
  PAOUT8          = 0x0100,
  PAOUT9          = 0x0200,
  PAOUT10         = 0x0400,
  PAOUT11         = 0x0800,
  PAOUT12         = 0x1000,
  PAOUT13         = 0x2000,
  PAOUT14         = 0x4000,
  PAOUT15         = 0x8000
};


__no_init volatile union
{
  unsigned short PADIR;   /* Port A Direction */

  struct
  {
    unsigned short PADIR0          : 1; /*  */
    unsigned short PADIR1          : 1; /*  */
    unsigned short PADIR2          : 1; /*  */
    unsigned short PADIR3          : 1; /*  */
    unsigned short PADIR4          : 1; /*  */
    unsigned short PADIR5          : 1; /*  */
    unsigned short PADIR6          : 1; /*  */
    unsigned short PADIR7          : 1; /*  */
    unsigned short PADIR8          : 1; /*  */
    unsigned short PADIR9          : 1; /*  */
    unsigned short PADIR10         : 1; /*  */
    unsigned short PADIR11         : 1; /*  */
    unsigned short PADIR12         : 1; /*  */
    unsigned short PADIR13         : 1; /*  */
    unsigned short PADIR14         : 1; /*  */
    unsigned short PADIR15         : 1; /*  */
  }PADIR_bit;
} @0x0204;


enum {
  PADIR0          = 0x0001,
  PADIR1          = 0x0002,
  PADIR2          = 0x0004,
  PADIR3          = 0x0008,
  PADIR4          = 0x0010,
  PADIR5          = 0x0020,
  PADIR6          = 0x0040,
  PADIR7          = 0x0080,
  PADIR8          = 0x0100,
  PADIR9          = 0x0200,
  PADIR10         = 0x0400,
  PADIR11         = 0x0800,
  PADIR12         = 0x1000,
  PADIR13         = 0x2000,
  PADIR14         = 0x4000,
  PADIR15         = 0x8000
};


__no_init volatile union
{
  unsigned short PAREN;   /* Port A Resistor Enable */

  struct
  {
    unsigned short PAREN0          : 1; /*  */
    unsigned short PAREN1          : 1; /*  */
    unsigned short PAREN2          : 1; /*  */
    unsigned short PAREN3          : 1; /*  */
    unsigned short PAREN4          : 1; /*  */
    unsigned short PAREN5          : 1; /*  */
    unsigned short PAREN6          : 1; /*  */
    unsigned short PAREN7          : 1; /*  */
    unsigned short PAREN8          : 1; /*  */
    unsigned short PAREN9          : 1; /*  */
    unsigned short PAREN10         : 1; /*  */
    unsigned short PAREN11         : 1; /*  */
    unsigned short PAREN12         : 1; /*  */
    unsigned short PAREN13         : 1; /*  */
    unsigned short PAREN14         : 1; /*  */
    unsigned short PAREN15         : 1; /*  */
  }PAREN_bit;
} @0x0206;


enum {
  PAREN0          = 0x0001,
  PAREN1          = 0x0002,
  PAREN2          = 0x0004,
  PAREN3          = 0x0008,
  PAREN4          = 0x0010,
  PAREN5          = 0x0020,
  PAREN6          = 0x0040,
  PAREN7          = 0x0080,
  PAREN8          = 0x0100,
  PAREN9          = 0x0200,
  PAREN10         = 0x0400,
  PAREN11         = 0x0800,
  PAREN12         = 0x1000,
  PAREN13         = 0x2000,
  PAREN14         = 0x4000,
  PAREN15         = 0x8000
};


__no_init volatile union
{
  unsigned short PADS;   /* Port A Resistor Drive Strenght */

  struct
  {
    unsigned short PADS0           : 1; /*  */
    unsigned short PADS1           : 1; /*  */
    unsigned short PADS2           : 1; /*  */
    unsigned short PADS3           : 1; /*  */
    unsigned short PADS4           : 1; /*  */
    unsigned short PADS5           : 1; /*  */
    unsigned short PADS6           : 1; /*  */
    unsigned short PADS7           : 1; /*  */
    unsigned short PADS8           : 1; /*  */
    unsigned short PADS9           : 1; /*  */
    unsigned short PADS10          : 1; /*  */
    unsigned short PADS11          : 1; /*  */
    unsigned short PADS12          : 1; /*  */
    unsigned short PADS13          : 1; /*  */
    unsigned short PADS14          : 1; /*  */
    unsigned short PADS15          : 1; /*  */
  }PADS_bit;
} @0x0208;


enum {
  PADS0           = 0x0001,
  PADS1           = 0x0002,
  PADS2           = 0x0004,
  PADS3           = 0x0008,
  PADS4           = 0x0010,
  PADS5           = 0x0020,
  PADS6           = 0x0040,
  PADS7           = 0x0080,
  PADS8           = 0x0100,
  PADS9           = 0x0200,
  PADS10          = 0x0400,
  PADS11          = 0x0800,
  PADS12          = 0x1000,
  PADS13          = 0x2000,
  PADS14          = 0x4000,
  PADS15          = 0x8000
};


__no_init volatile union
{
  unsigned short PASEL;   /* Port A Selection */

  struct
  {
    unsigned short PASEL0          : 1; /*  */
    unsigned short PASEL1          : 1; /*  */
    unsigned short PASEL2          : 1; /*  */
    unsigned short PASEL3          : 1; /*  */
    unsigned short PASEL4          : 1; /*  */
    unsigned short PASEL5          : 1; /*  */
    unsigned short PASEL6          : 1; /*  */
    unsigned short PASEL7          : 1; /*  */
    unsigned short PASEL8          : 1; /*  */
    unsigned short PASEL9          : 1; /*  */
    unsigned short PASEL10         : 1; /*  */
    unsigned short PASEL11         : 1; /*  */
    unsigned short PASEL12         : 1; /*  */
    unsigned short PASEL13         : 1; /*  */
    unsigned short PASEL14         : 1; /*  */
    unsigned short PASEL15         : 1; /*  */
  }PASEL_bit;
} @0x020A;


enum {
  PASEL0          = 0x0001,
  PASEL1          = 0x0002,
  PASEL2          = 0x0004,
  PASEL3          = 0x0008,
  PASEL4          = 0x0010,
  PASEL5          = 0x0020,
  PASEL6          = 0x0040,
  PASEL7          = 0x0080,
  PASEL8          = 0x0100,
  PASEL9          = 0x0200,
  PASEL10         = 0x0400,
  PASEL11         = 0x0800,
  PASEL12         = 0x1000,
  PASEL13         = 0x2000,
  PASEL14         = 0x4000,
  PASEL15         = 0x8000
};


__no_init volatile union
{
  unsigned short PAIES;   /* Port A Interrupt Edge Select */

  struct
  {
    unsigned short PAIES0          : 1; /*  */
    unsigned short PAIES1          : 1; /*  */
    unsigned short PAIES2          : 1; /*  */
    unsigned short PAIES3          : 1; /*  */
    unsigned short PAIES4          : 1; /*  */
    unsigned short PAIES5          : 1; /*  */
    unsigned short PAIES6          : 1; /*  */
    unsigned short PAIES7          : 1; /*  */
    unsigned short PAIES8          : 1; /*  */
    unsigned short PAIES9          : 1; /*  */
    unsigned short PAIES10         : 1; /*  */
    unsigned short PAIES11         : 1; /*  */
    unsigned short PAIES12         : 1; /*  */
    unsigned short PAIES13         : 1; /*  */
    unsigned short PAIES14         : 1; /*  */
    unsigned short PAIES15         : 1; /*  */
  }PAIES_bit;
} @0x0218;


enum {
  PAIES0          = 0x0001,
  PAIES1          = 0x0002,
  PAIES2          = 0x0004,
  PAIES3          = 0x0008,
  PAIES4          = 0x0010,
  PAIES5          = 0x0020,
  PAIES6          = 0x0040,
  PAIES7          = 0x0080,
  PAIES8          = 0x0100,
  PAIES9          = 0x0200,
  PAIES10         = 0x0400,
  PAIES11         = 0x0800,
  PAIES12         = 0x1000,
  PAIES13         = 0x2000,
  PAIES14         = 0x4000,
  PAIES15         = 0x8000
};


__no_init volatile union
{
  unsigned short PAIE;   /* Port A Interrupt Enable */

  struct
  {
    unsigned short PAIE0           : 1; /*  */
    unsigned short PAIE1           : 1; /*  */
    unsigned short PAIE2           : 1; /*  */
    unsigned short PAIE3           : 1; /*  */
    unsigned short PAIE4           : 1; /*  */
    unsigned short PAIE5           : 1; /*  */
    unsigned short PAIE6           : 1; /*  */
    unsigned short PAIE7           : 1; /*  */
    unsigned short PAIE8           : 1; /*  */
    unsigned short PAIE9           : 1; /*  */
    unsigned short PAIE10          : 1; /*  */
    unsigned short PAIE11          : 1; /*  */
    unsigned short PAIE12          : 1; /*  */
    unsigned short PAIE13          : 1; /*  */
    unsigned short PAIE14          : 1; /*  */
    unsigned short PAIE15          : 1; /*  */
  }PAIE_bit;
} @0x021A;


enum {
  PAIE0           = 0x0001,
  PAIE1           = 0x0002,
  PAIE2           = 0x0004,
  PAIE3           = 0x0008,
  PAIE4           = 0x0010,
  PAIE5           = 0x0020,
  PAIE6           = 0x0040,
  PAIE7           = 0x0080,
  PAIE8           = 0x0100,
  PAIE9           = 0x0200,
  PAIE10          = 0x0400,
  PAIE11          = 0x0800,
  PAIE12          = 0x1000,
  PAIE13          = 0x2000,
  PAIE14          = 0x4000,
  PAIE15          = 0x8000
};


__no_init volatile union
{
  unsigned short PAIFG;   /* Port A Interrupt Flag */

  struct
  {
    unsigned short PAIFG0          : 1; /*  */
    unsigned short PAIFG1          : 1; /*  */
    unsigned short PAIFG2          : 1; /*  */
    unsigned short PAIFG3          : 1; /*  */
    unsigned short PAIFG4          : 1; /*  */
    unsigned short PAIFG5          : 1; /*  */
    unsigned short PAIFG6          : 1; /*  */
    unsigned short PAIFG7          : 1; /*  */
    unsigned short PAIFG8          : 1; /*  */
    unsigned short PAIFG9          : 1; /*  */
    unsigned short PAIFG10         : 1; /*  */
    unsigned short PAIFG11         : 1; /*  */
    unsigned short PAIFG12         : 1; /*  */
    unsigned short PAIFG13         : 1; /*  */
    unsigned short PAIFG14         : 1; /*  */
    unsigned short PAIFG15         : 1; /*  */
  }PAIFG_bit;
} @ 0x021C;


enum {
  PAIFG0          = 0x0001,
  PAIFG1          = 0x0002,
  PAIFG2          = 0x0004,
  PAIFG3          = 0x0008,
  PAIFG4          = 0x0010,
  PAIFG5          = 0x0020,
  PAIFG6          = 0x0040,
  PAIFG7          = 0x0080,
  PAIFG8          = 0x0100,
  PAIFG9          = 0x0200,
  PAIFG10         = 0x0400,
  PAIFG11         = 0x0800,
  PAIFG12         = 0x1000,
  PAIFG13         = 0x2000,
  PAIFG14         = 0x4000,
  PAIFG15         = 0x8000
};


#define __MSP430_HAS_PORTA_R__        /* Definition to show that Module is available */


#define P1IV_               (0x020Eu)  /* Port 1 Interrupt Vector Word */
#define P2IV_               (0x021Eu)  /* Port 2 Interrupt Vector Word */
#define P1IN                (PAIN_L)  /* Port 1 Input */
#define P1OUT               (PAOUT_L) /* Port 1 Output */
#define P1DIR               (PADIR_L) /* Port 1 Direction */
#define P1REN               (PAREN_L) /* Port 1 Resistor Enable */
#define P1DS                (PADS_L)  /* Port 1 Resistor Drive Strenght */
#define P1SEL               (PASEL_L) /* Port 1 Selection */
#define P1IES               (PAIES_L) /* Port 1 Interrupt Edge Select */
#define P1IE                (PAIE_L)  /* Port 1 Interrupt Enable */
#define P1IFG               (PAIFG_L) /* Port 1 Interrupt Flag */
#define P1IV_NONE            (0x0000u)    /* No Interrupt pending */
#define P1IV_P1IFG0          (0x0002u)    /* P1IV P1IFG.0 */
#define P1IV_P1IFG1          (0x0004u)    /* P1IV P1IFG.1 */
#define P1IV_P1IFG2          (0x0006u)    /* P1IV P1IFG.2 */
#define P1IV_P1IFG3          (0x0008u)    /* P1IV P1IFG.3 */
#define P1IV_P1IFG4          (0x000Au)    /* P1IV P1IFG.4 */
#define P1IV_P1IFG5          (0x000Cu)    /* P1IV P1IFG.5 */
#define P1IV_P1IFG6          (0x000Eu)    /* P1IV P1IFG.6 */
#define P1IV_P1IFG7          (0x0010u)    /* P1IV P1IFG.7 */

#define P2IN                (PAIN_H)  /* Port 2 Input */
#define P2OUT               (PAOUT_H) /* Port 2 Output */
#define P2DIR               (PADIR_H) /* Port 2 Direction */
#define P2REN               (PAREN_H) /* Port 2 Resistor Enable */
#define P2DS                (PADS_H)  /* Port 2 Resistor Drive Strenght */
#define P2SEL               (PASEL_H) /* Port 2 Selection */
#define P2IES               (PAIES_H) /* Port 2 Interrupt Edge Select */
#define P2IE                (PAIE_H)  /* Port 2 Interrupt Enable */
#define P2IFG               (PAIFG_H) /* Port 2 Interrupt Flag */
#define P2IV_NONE            (0x0000u)    /* No Interrupt pending */
#define P2IV_P2IFG0          (0x0002u)    /* P2IV P2IFG.0 */
#define P2IV_P2IFG1          (0x0004u)    /* P2IV P2IFG.1 */
#define P2IV_P2IFG2          (0x0006u)    /* P2IV P2IFG.2 */
#define P2IV_P2IFG3          (0x0008u)    /* P2IV P2IFG.3 */
#define P2IV_P2IFG4          (0x000Au)    /* P2IV P2IFG.4 */
#define P2IV_P2IFG5          (0x000Cu)    /* P2IV P2IFG.5 */
#define P2IV_P2IFG6          (0x000Eu)    /* P2IV P2IFG.6 */
#define P2IV_P2IFG7          (0x0010u)    /* P2IV P2IFG.7 */

/*-------------------------------------------------------------------------
 *   Port 1/2
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned char P1IN;   /*  */

  struct
  {
    unsigned char P1IN0           : 1; /*  */
    unsigned char P1IN1           : 1; /*  */
    unsigned char P1IN2           : 1; /*  */
    unsigned char P1IN3           : 1; /*  */
    unsigned char P1IN4           : 1; /*  */
    unsigned char P1IN5           : 1; /*  */
    unsigned char P1IN6           : 1; /*  */
    unsigned char P1IN7           : 1; /*  */
  }P1IN_bit;
} @0x0200;


enum {
  P1IN0           = 0x0001,
  P1IN1           = 0x0002,
  P1IN2           = 0x0004,
  P1IN3           = 0x0008,
  P1IN4           = 0x0010,
  P1IN5           = 0x0020,
  P1IN6           = 0x0040,
  P1IN7           = 0x0080
};


__no_init volatile union
{
  unsigned char P1OUT;   /*  */

  struct
  {
    unsigned char P1OUT0          : 1; /*  */
    unsigned char P1OUT1          : 1; /*  */
    unsigned char P1OUT2          : 1; /*  */
    unsigned char P1OUT3          : 1; /*  */
    unsigned char P1OUT4          : 1; /*  */
    unsigned char P1OUT5          : 1; /*  */
    unsigned char P1OUT6          : 1; /*  */
    unsigned char P1OUT7          : 1; /*  */
  }P1OUT_bit;
} @0x0202;


enum {
  P1OUT0          = 0x0001,
  P1OUT1          = 0x0002,
  P1OUT2          = 0x0004,
  P1OUT3          = 0x0008,
  P1OUT4          = 0x0010,
  P1OUT5          = 0x0020,
  P1OUT6          = 0x0040,
  P1OUT7          = 0x0080
};


__no_init volatile union
{
  unsigned char P1DIR;   /*  */

  struct
  {
    unsigned char P1DIR0          : 1; /*  */
    unsigned char P1DIR1          : 1; /*  */
    unsigned char P1DIR2          : 1; /*  */
    unsigned char P1DIR3          : 1; /*  */
    unsigned char P1DIR4          : 1; /*  */
    unsigned char P1DIR5          : 1; /*  */
    unsigned char P1DIR6          : 1; /*  */
    unsigned char P1DIR7          : 1; /*  */
  }P1DIR_bit;
} @0x0204;


enum {
  P1DIR0          = 0x0001,
  P1DIR1          = 0x0002,
  P1DIR2          = 0x0004,
  P1DIR3          = 0x0008,
  P1DIR4          = 0x0010,
  P1DIR5          = 0x0020,
  P1DIR6          = 0x0040,
  P1DIR7          = 0x0080
};


__no_init volatile union
{
  unsigned char P1REN;   /*  */

  struct
  {
    unsigned char P1REN0          : 1; /*  */
    unsigned char P1REN1          : 1; /*  */
    unsigned char P1REN2          : 1; /*  */
    unsigned char P1REN3          : 1; /*  */
    unsigned char P1REN4          : 1; /*  */
    unsigned char P1REN5          : 1; /*  */
    unsigned char P1REN6          : 1; /*  */
    unsigned char P1REN7          : 1; /*  */
  }P1REN_bit;
} @0x0206;


enum {
  P1REN0          = 0x0001,
  P1REN1          = 0x0002,
  P1REN2          = 0x0004,
  P1REN3          = 0x0008,
  P1REN4          = 0x0010,
  P1REN5          = 0x0020,
  P1REN6          = 0x0040,
  P1REN7          = 0x0080
};


__no_init volatile union
{
  unsigned char P1DS;   /*  */

  struct
  {
    unsigned char P1DS0           : 1; /*  */
    unsigned char P1DS1           : 1; /*  */
    unsigned char P1DS2           : 1; /*  */
    unsigned char P1DS3           : 1; /*  */
    unsigned char P1DS4           : 1; /*  */
    unsigned char P1DS5           : 1; /*  */
    unsigned char P1DS6           : 1; /*  */
    unsigned char P1DS7           : 1; /*  */
  }P1DS_bit;
} @0x0208;


enum {
  P1DS0           = 0x0001,
  P1DS1           = 0x0002,
  P1DS2           = 0x0004,
  P1DS3           = 0x0008,
  P1DS4           = 0x0010,
  P1DS5           = 0x0020,
  P1DS6           = 0x0040,
  P1DS7           = 0x0080
};


__no_init volatile union
{
  unsigned char P1SEL;   /*  */

  struct
  {
    unsigned char P1SEL0          : 1; /*  */
    unsigned char P1SEL1          : 1; /*  */
    unsigned char P1SEL2          : 1; /*  */
    unsigned char P1SEL3          : 1; /*  */
    unsigned char P1SEL4          : 1; /*  */
    unsigned char P1SEL5          : 1; /*  */
    unsigned char P1SEL6          : 1; /*  */
    unsigned char P1SEL7          : 1; /*  */
  }P1SEL_bit;
} @0x020A;


enum {
  P1SEL0          = 0x0001,
  P1SEL1          = 0x0002,
  P1SEL2          = 0x0004,
  P1SEL3          = 0x0008,
  P1SEL4          = 0x0010,
  P1SEL5          = 0x0020,
  P1SEL6          = 0x0040,
  P1SEL7          = 0x0080
};


  /* Port 1 Interrupt Vector Word */
__no_init volatile unsigned short P1IV @ 0x020E;


__no_init volatile union
{
  unsigned char P1IES;   /*  */

  struct
  {
    unsigned char P1IES0          : 1; /*  */
    unsigned char P1IES1          : 1; /*  */
    unsigned char P1IES2          : 1; /*  */
    unsigned char P1IES3          : 1; /*  */
    unsigned char P1IES4          : 1; /*  */
    unsigned char P1IES5          : 1; /*  */
    unsigned char P1IES6          : 1; /*  */
    unsigned char P1IES7          : 1; /*  */
  }P1IES_bit;
} @0x0218;


enum {
  P1IES0          = 0x0001,
  P1IES1          = 0x0002,
  P1IES2          = 0x0004,
  P1IES3          = 0x0008,
  P1IES4          = 0x0010,
  P1IES5          = 0x0020,
  P1IES6          = 0x0040,
  P1IES7          = 0x0080
};


__no_init volatile union
{
  unsigned char P1IE;   /*  */

  struct
  {
    unsigned char P1IE0           : 1; /*  */
    unsigned char P1IE1           : 1; /*  */
    unsigned char P1IE2           : 1; /*  */
    unsigned char P1IE3           : 1; /*  */
    unsigned char P1IE4           : 1; /*  */
    unsigned char P1IE5           : 1; /*  */
    unsigned char P1IE6           : 1; /*  */
    unsigned char P1IE7           : 1; /*  */
  }P1IE_bit;
} @0x021A;


enum {
  P1IE0           = 0x0001,
  P1IE1           = 0x0002,
  P1IE2           = 0x0004,
  P1IE3           = 0x0008,
  P1IE4           = 0x0010,
  P1IE5           = 0x0020,
  P1IE6           = 0x0040,
  P1IE7           = 0x0080
};


__no_init volatile union
{
  unsigned char P1IFG;   /*  */

  struct
  {
    unsigned char P1IFG0          : 1; /*  */
    unsigned char P1IFG1          : 1; /*  */
    unsigned char P1IFG2          : 1; /*  */
    unsigned char P1IFG3          : 1; /*  */
    unsigned char P1IFG4          : 1; /*  */
    unsigned char P1IFG5          : 1; /*  */
    unsigned char P1IFG6          : 1; /*  */
    unsigned char P1IFG7          : 1; /*  */
  }P1IFG_bit;
} @0x021C;


enum {
  P1IFG0          = 0x0001,
  P1IFG1          = 0x0002,
  P1IFG2          = 0x0004,
  P1IFG3          = 0x0008,
  P1IFG4          = 0x0010,
  P1IFG5          = 0x0020,
  P1IFG6          = 0x0040,
  P1IFG7          = 0x0080
};


__no_init volatile union
{
  unsigned char P2IN;   /*  */

  struct
  {
    unsigned char P2IN0           : 1; /*  */
    unsigned char P2IN1           : 1; /*  */
    unsigned char P2IN2           : 1; /*  */
    unsigned char P2IN3           : 1; /*  */
    unsigned char P2IN4           : 1; /*  */
    unsigned char P2IN5           : 1; /*  */
    unsigned char P2IN6           : 1; /*  */
    unsigned char P2IN7           : 1; /*  */
  }P2IN_bit;
} @0x0201;


enum {
  P2IN0           = 0x0001,
  P2IN1           = 0x0002,
  P2IN2           = 0x0004,
  P2IN3           = 0x0008,
  P2IN4           = 0x0010,
  P2IN5           = 0x0020,
  P2IN6           = 0x0040,
  P2IN7           = 0x0080
};


__no_init volatile union
{
  unsigned char P2OUT;   /*  */

  struct
  {
    unsigned char P2OUT0          : 1; /*  */
    unsigned char P2OUT1          : 1; /*  */
    unsigned char P2OUT2          : 1; /*  */
    unsigned char P2OUT3          : 1; /*  */
    unsigned char P2OUT4          : 1; /*  */
    unsigned char P2OUT5          : 1; /*  */
    unsigned char P2OUT6          : 1; /*  */
    unsigned char P2OUT7          : 1; /*  */
  }P2OUT_bit;
} @0x0203;


enum {
  P2OUT0          = 0x0001,
  P2OUT1          = 0x0002,
  P2OUT2          = 0x0004,
  P2OUT3          = 0x0008,
  P2OUT4          = 0x0010,
  P2OUT5          = 0x0020,
  P2OUT6          = 0x0040,
  P2OUT7          = 0x0080
};


__no_init volatile union
{
  unsigned char P2DIR;   /*  */

  struct
  {
    unsigned char P2DIR0          : 1; /*  */
    unsigned char P2DIR1          : 1; /*  */
    unsigned char P2DIR2          : 1; /*  */
    unsigned char P2DIR3          : 1; /*  */
    unsigned char P2DIR4          : 1; /*  */
    unsigned char P2DIR5          : 1; /*  */
    unsigned char P2DIR6          : 1; /*  */
    unsigned char P2DIR7          : 1; /*  */
  }P2DIR_bit;
} @0x0205;


enum {
  P2DIR0          = 0x0001,
  P2DIR1          = 0x0002,
  P2DIR2          = 0x0004,
  P2DIR3          = 0x0008,
  P2DIR4          = 0x0010,
  P2DIR5          = 0x0020,
  P2DIR6          = 0x0040,
  P2DIR7          = 0x0080
};


__no_init volatile union
{
  unsigned char P2REN;   /*  */

  struct
  {
    unsigned char P2REN0          : 1; /*  */
    unsigned char P2REN1          : 1; /*  */
    unsigned char P2REN2          : 1; /*  */
    unsigned char P2REN3          : 1; /*  */
    unsigned char P2REN4          : 1; /*  */
    unsigned char P2REN5          : 1; /*  */
    unsigned char P2REN6          : 1; /*  */
    unsigned char P2REN7          : 1; /*  */
  }P2REN_bit;
} @0x0207;


enum {
  P2REN0          = 0x0001,
  P2REN1          = 0x0002,
  P2REN2          = 0x0004,
  P2REN3          = 0x0008,
  P2REN4          = 0x0010,
  P2REN5          = 0x0020,
  P2REN6          = 0x0040,
  P2REN7          = 0x0080
};


__no_init volatile union
{
  unsigned char P2DS;   /*  */

  struct
  {
    unsigned char P2DS0           : 1; /*  */
    unsigned char P2DS1           : 1; /*  */
    unsigned char P2DS2           : 1; /*  */
    unsigned char P2DS3           : 1; /*  */
    unsigned char P2DS4           : 1; /*  */
    unsigned char P2DS5           : 1; /*  */
    unsigned char P2DS6           : 1; /*  */
    unsigned char P2DS7           : 1; /*  */
  }P2DS_bit;
} @0x0209;


enum {
  P2DS0           = 0x0001,
  P2DS1           = 0x0002,
  P2DS2           = 0x0004,
  P2DS3           = 0x0008,
  P2DS4           = 0x0010,
  P2DS5           = 0x0020,
  P2DS6           = 0x0040,
  P2DS7           = 0x0080
};


__no_init volatile union
{
  unsigned char P2SEL;   /*  */

  struct
  {
    unsigned char P2SEL0          : 1; /*  */
    unsigned char P2SEL1          : 1; /*  */
    unsigned char P2SEL2          : 1; /*  */
    unsigned char P2SEL3          : 1; /*  */
    unsigned char P2SEL4          : 1; /*  */
    unsigned char P2SEL5          : 1; /*  */
    unsigned char P2SEL6          : 1; /*  */
    unsigned char P2SEL7          : 1; /*  */
  }P2SEL_bit;
} @0x020B;


enum {
  P2SEL0          = 0x0001,
  P2SEL1          = 0x0002,
  P2SEL2          = 0x0004,
  P2SEL3          = 0x0008,
  P2SEL4          = 0x0010,
  P2SEL5          = 0x0020,
  P2SEL6          = 0x0040,
  P2SEL7          = 0x0080
};


  /* Port 2 Interrupt Vector Word */
__no_init volatile unsigned short P2IV @ 0x021E;


__no_init volatile union
{
  unsigned char P2IES;   /*  */

  struct
  {
    unsigned char P2IES0          : 1; /*  */
    unsigned char P2IES1          : 1; /*  */
    unsigned char P2IES2          : 1; /*  */
    unsigned char P2IES3          : 1; /*  */
    unsigned char P2IES4          : 1; /*  */
    unsigned char P2IES5          : 1; /*  */
    unsigned char P2IES6          : 1; /*  */
    unsigned char P2IES7          : 1; /*  */
  }P2IES_bit;
} @0x0219;


enum {
  P2IES0          = 0x0001,
  P2IES1          = 0x0002,
  P2IES2          = 0x0004,
  P2IES3          = 0x0008,
  P2IES4          = 0x0010,
  P2IES5          = 0x0020,
  P2IES6          = 0x0040,
  P2IES7          = 0x0080
};


__no_init volatile union
{
  unsigned char P2IE;   /*  */

  struct
  {
    unsigned char P2IE0           : 1; /*  */
    unsigned char P2IE1           : 1; /*  */
    unsigned char P2IE2           : 1; /*  */
    unsigned char P2IE3           : 1; /*  */
    unsigned char P2IE4           : 1; /*  */
    unsigned char P2IE5           : 1; /*  */
    unsigned char P2IE6           : 1; /*  */
    unsigned char P2IE7           : 1; /*  */
  }P2IE_bit;
} @0x021B;


enum {
  P2IE0           = 0x0001,
  P2IE1           = 0x0002,
  P2IE2           = 0x0004,
  P2IE3           = 0x0008,
  P2IE4           = 0x0010,
  P2IE5           = 0x0020,
  P2IE6           = 0x0040,
  P2IE7           = 0x0080
};


__no_init volatile union
{
  unsigned char P2IFG;   /*  */

  struct
  {
    unsigned char P2IFG0          : 1; /*  */
    unsigned char P2IFG1          : 1; /*  */
    unsigned char P2IFG2          : 1; /*  */
    unsigned char P2IFG3          : 1; /*  */
    unsigned char P2IFG4          : 1; /*  */
    unsigned char P2IFG5          : 1; /*  */
    unsigned char P2IFG6          : 1; /*  */
    unsigned char P2IFG7          : 1; /*  */
  }P2IFG_bit;
} @ 0x021D;


enum {
  P2IFG0          = 0x0001,
  P2IFG1          = 0x0002,
  P2IFG2          = 0x0004,
  P2IFG3          = 0x0008,
  P2IFG4          = 0x0010,
  P2IFG5          = 0x0020,
  P2IFG6          = 0x0040,
  P2IFG7          = 0x0080
};


/*-------------------------------------------------------------------------
 *   Port B
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned __READ short PBIN;   /* Port B Input */

  struct
  {
    unsigned __READ short PBIN0           : 1; /*  */
    unsigned __READ short PBIN1           : 1; /*  */
    unsigned __READ short PBIN2           : 1; /*  */
    unsigned __READ short PBIN3           : 1; /*  */
    unsigned __READ short PBIN4           : 1; /*  */
    unsigned __READ short PBIN5           : 1; /*  */
    unsigned __READ short PBIN6           : 1; /*  */
    unsigned __READ short PBIN7           : 1; /*  */
    unsigned __READ short PBIN8           : 1; /*  */
    unsigned __READ short PBIN9           : 1; /*  */
    unsigned __READ short PBIN10          : 1; /*  */
    unsigned __READ short PBIN11          : 1; /*  */
    unsigned __READ short PBIN12          : 1; /*  */
    unsigned __READ short PBIN13          : 1; /*  */
    unsigned __READ short PBIN14          : 1; /*  */
    unsigned __READ short PBIN15          : 1; /*  */
  }PBIN_bit;
} @0x0220;


enum {
  PBIN0           = 0x0001,
  PBIN1           = 0x0002,
  PBIN2           = 0x0004,
  PBIN3           = 0x0008,
  PBIN4           = 0x0010,
  PBIN5           = 0x0020,
  PBIN6           = 0x0040,
  PBIN7           = 0x0080,
  PBIN8           = 0x0100,
  PBIN9           = 0x0200,
  PBIN10          = 0x0400,
  PBIN11          = 0x0800,
  PBIN12          = 0x1000,
  PBIN13          = 0x2000,
  PBIN14          = 0x4000,
  PBIN15          = 0x8000
};


__no_init volatile union
{
  unsigned short PBOUT;   /* Port B Output */

  struct
  {
    unsigned short PBOUT0          : 1; /*  */
    unsigned short PBOUT1          : 1; /*  */
    unsigned short PBOUT2          : 1; /*  */
    unsigned short PBOUT3          : 1; /*  */
    unsigned short PBOUT4          : 1; /*  */
    unsigned short PBOUT5          : 1; /*  */
    unsigned short PBOUT6          : 1; /*  */
    unsigned short PBOUT7          : 1; /*  */
    unsigned short PBOUT8          : 1; /*  */
    unsigned short PBOUT9          : 1; /*  */
    unsigned short PBOUT10         : 1; /*  */
    unsigned short PBOUT11         : 1; /*  */
    unsigned short PBOUT12         : 1; /*  */
    unsigned short PBOUT13         : 1; /*  */
    unsigned short PBOUT14         : 1; /*  */
    unsigned short PBOUT15         : 1; /*  */
  }PBOUT_bit;
} @0x0222;


enum {
  PBOUT0          = 0x0001,
  PBOUT1          = 0x0002,
  PBOUT2          = 0x0004,
  PBOUT3          = 0x0008,
  PBOUT4          = 0x0010,
  PBOUT5          = 0x0020,
  PBOUT6          = 0x0040,
  PBOUT7          = 0x0080,
  PBOUT8          = 0x0100,
  PBOUT9          = 0x0200,
  PBOUT10         = 0x0400,
  PBOUT11         = 0x0800,
  PBOUT12         = 0x1000,
  PBOUT13         = 0x2000,
  PBOUT14         = 0x4000,
  PBOUT15         = 0x8000
};


__no_init volatile union
{
  unsigned short PBDIR;   /* Port B Direction */

  struct
  {
    unsigned short PBDIR0          : 1; /*  */
    unsigned short PBDIR1          : 1; /*  */
    unsigned short PBDIR2          : 1; /*  */
    unsigned short PBDIR3          : 1; /*  */
    unsigned short PBDIR4          : 1; /*  */
    unsigned short PBDIR5          : 1; /*  */
    unsigned short PBDIR6          : 1; /*  */
    unsigned short PBDIR7          : 1; /*  */
    unsigned short PBDIR8          : 1; /*  */
    unsigned short PBDIR9          : 1; /*  */
    unsigned short PBDIR10         : 1; /*  */
    unsigned short PBDIR11         : 1; /*  */
    unsigned short PBDIR12         : 1; /*  */
    unsigned short PBDIR13         : 1; /*  */
    unsigned short PBDIR14         : 1; /*  */
    unsigned short PBDIR15         : 1; /*  */
  }PBDIR_bit;
} @0x0224;


enum {
  PBDIR0          = 0x0001,
  PBDIR1          = 0x0002,
  PBDIR2          = 0x0004,
  PBDIR3          = 0x0008,
  PBDIR4          = 0x0010,
  PBDIR5          = 0x0020,
  PBDIR6          = 0x0040,
  PBDIR7          = 0x0080,
  PBDIR8          = 0x0100,
  PBDIR9          = 0x0200,
  PBDIR10         = 0x0400,
  PBDIR11         = 0x0800,
  PBDIR12         = 0x1000,
  PBDIR13         = 0x2000,
  PBDIR14         = 0x4000,
  PBDIR15         = 0x8000
};


__no_init volatile union
{
  unsigned short PBREN;   /* Port B Resistor Enable */

  struct
  {
    unsigned short PBREN0          : 1; /*  */
    unsigned short PBREN1          : 1; /*  */
    unsigned short PBREN2          : 1; /*  */
    unsigned short PBREN3          : 1; /*  */
    unsigned short PBREN4          : 1; /*  */
    unsigned short PBREN5          : 1; /*  */
    unsigned short PBREN6          : 1; /*  */
    unsigned short PBREN7          : 1; /*  */
    unsigned short PBREN8          : 1; /*  */
    unsigned short PBREN9          : 1; /*  */
    unsigned short PBREN10         : 1; /*  */
    unsigned short PBREN11         : 1; /*  */
    unsigned short PBREN12         : 1; /*  */
    unsigned short PBREN13         : 1; /*  */
    unsigned short PBREN14         : 1; /*  */
    unsigned short PBREN15         : 1; /*  */
  }PBREN_bit;
} @0x0226;


enum {
  PBREN0          = 0x0001,
  PBREN1          = 0x0002,
  PBREN2          = 0x0004,
  PBREN3          = 0x0008,
  PBREN4          = 0x0010,
  PBREN5          = 0x0020,
  PBREN6          = 0x0040,
  PBREN7          = 0x0080,
  PBREN8          = 0x0100,
  PBREN9          = 0x0200,
  PBREN10         = 0x0400,
  PBREN11         = 0x0800,
  PBREN12         = 0x1000,
  PBREN13         = 0x2000,
  PBREN14         = 0x4000,
  PBREN15         = 0x8000
};


__no_init volatile union
{
  unsigned short PBDS;   /* Port B Resistor Drive Strenght */

  struct
  {
    unsigned short PBDS0           : 1; /*  */
    unsigned short PBDS1           : 1; /*  */
    unsigned short PBDS2           : 1; /*  */
    unsigned short PBDS3           : 1; /*  */
    unsigned short PBDS4           : 1; /*  */
    unsigned short PBDS5           : 1; /*  */
    unsigned short PBDS6           : 1; /*  */
    unsigned short PBDS7           : 1; /*  */
    unsigned short PBDS8           : 1; /*  */
    unsigned short PBDS9           : 1; /*  */
    unsigned short PBDS10          : 1; /*  */
    unsigned short PBDS11          : 1; /*  */
    unsigned short PBDS12          : 1; /*  */
    unsigned short PBDS13          : 1; /*  */
    unsigned short PBDS14          : 1; /*  */
    unsigned short PBDS15          : 1; /*  */
  }PBDS_bit;
} @0x0228;


enum {
  PBDS0           = 0x0001,
  PBDS1           = 0x0002,
  PBDS2           = 0x0004,
  PBDS3           = 0x0008,
  PBDS4           = 0x0010,
  PBDS5           = 0x0020,
  PBDS6           = 0x0040,
  PBDS7           = 0x0080,
  PBDS8           = 0x0100,
  PBDS9           = 0x0200,
  PBDS10          = 0x0400,
  PBDS11          = 0x0800,
  PBDS12          = 0x1000,
  PBDS13          = 0x2000,
  PBDS14          = 0x4000,
  PBDS15          = 0x8000
};


__no_init volatile union
{
  unsigned short PBSEL;   /* Port B Selection */

  struct
  {
    unsigned short PBSEL0          : 1; /*  */
    unsigned short PBSEL1          : 1; /*  */
    unsigned short PBSEL2          : 1; /*  */
    unsigned short PBSEL3          : 1; /*  */
    unsigned short PBSEL4          : 1; /*  */
    unsigned short PBSEL5          : 1; /*  */
    unsigned short PBSEL6          : 1; /*  */
    unsigned short PBSEL7          : 1; /*  */
    unsigned short PBSEL8          : 1; /*  */
    unsigned short PBSEL9          : 1; /*  */
    unsigned short PBSEL10         : 1; /*  */
    unsigned short PBSEL11         : 1; /*  */
    unsigned short PBSEL12         : 1; /*  */
    unsigned short PBSEL13         : 1; /*  */
    unsigned short PBSEL14         : 1; /*  */
    unsigned short PBSEL15         : 1; /*  */
  }PBSEL_bit;
} @ 0x022A;


enum {
  PBSEL0          = 0x0001,
  PBSEL1          = 0x0002,
  PBSEL2          = 0x0004,
  PBSEL3          = 0x0008,
  PBSEL4          = 0x0010,
  PBSEL5          = 0x0020,
  PBSEL6          = 0x0040,
  PBSEL7          = 0x0080,
  PBSEL8          = 0x0100,
  PBSEL9          = 0x0200,
  PBSEL10         = 0x0400,
  PBSEL11         = 0x0800,
  PBSEL12         = 0x1000,
  PBSEL13         = 0x2000,
  PBSEL14         = 0x4000,
  PBSEL15         = 0x8000
};


#define __MSP430_HAS_PORTB_R__        /* Definition to show that Module is available */


#define P3IN                (PBIN_L)  /* Port 3 Input */
#define P3OUT               (PBOUT_L) /* Port 3 Output */
#define P3DIR               (PBDIR_L) /* Port 3 Direction */
#define P3REN               (PBREN_L) /* Port 3 Resistor Enable */
#define P3DS                (PBDS_L)  /* Port 3 Resistor Drive Strenght */
#define P3SEL               (PBSEL_L) /* Port 3 Selection */

/*-------------------------------------------------------------------------
 *   Port 3
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned char P3IN;   /*  */

  struct
  {
    unsigned char P3IN0           : 1; /*  */
    unsigned char P3IN1           : 1; /*  */
    unsigned char P3IN2           : 1; /*  */
    unsigned char P3IN3           : 1; /*  */
    unsigned char P3IN4           : 1; /*  */
    unsigned char P3IN5           : 1; /*  */
    unsigned char P3IN6           : 1; /*  */
    unsigned char P3IN7           : 1; /*  */
  }P3IN_bit;
} @0x0220;


enum {
  P3IN0           = 0x0001,
  P3IN1           = 0x0002,
  P3IN2           = 0x0004,
  P3IN3           = 0x0008,
  P3IN4           = 0x0010,
  P3IN5           = 0x0020,
  P3IN6           = 0x0040,
  P3IN7           = 0x0080
};


__no_init volatile union
{
  unsigned char P3OUT;   /*  */

  struct
  {
    unsigned char P3OUT0          : 1; /*  */
    unsigned char P3OUT1          : 1; /*  */
    unsigned char P3OUT2          : 1; /*  */
    unsigned char P3OUT3          : 1; /*  */
    unsigned char P3OUT4          : 1; /*  */
    unsigned char P3OUT5          : 1; /*  */
    unsigned char P3OUT6          : 1; /*  */
    unsigned char P3OUT7          : 1; /*  */
  }P3OUT_bit;
} @0x0222;


enum {
  P3OUT0          = 0x0001,
  P3OUT1          = 0x0002,
  P3OUT2          = 0x0004,
  P3OUT3          = 0x0008,
  P3OUT4          = 0x0010,
  P3OUT5          = 0x0020,
  P3OUT6          = 0x0040,
  P3OUT7          = 0x0080
};


__no_init volatile union
{
  unsigned char P3DIR;   /*  */

  struct
  {
    unsigned char P3DIR0          : 1; /*  */
    unsigned char P3DIR1          : 1; /*  */
    unsigned char P3DIR2          : 1; /*  */
    unsigned char P3DIR3          : 1; /*  */
    unsigned char P3DIR4          : 1; /*  */
    unsigned char P3DIR5          : 1; /*  */
    unsigned char P3DIR6          : 1; /*  */
    unsigned char P3DIR7          : 1; /*  */
  }P3DIR_bit;
} @0x0224;


enum {
  P3DIR0          = 0x0001,
  P3DIR1          = 0x0002,
  P3DIR2          = 0x0004,
  P3DIR3          = 0x0008,
  P3DIR4          = 0x0010,
  P3DIR5          = 0x0020,
  P3DIR6          = 0x0040,
  P3DIR7          = 0x0080
};


__no_init volatile union
{
  unsigned char P3REN;   /*  */

  struct
  {
    unsigned char P3REN0          : 1; /*  */
    unsigned char P3REN1          : 1; /*  */
    unsigned char P3REN2          : 1; /*  */
    unsigned char P3REN3          : 1; /*  */
    unsigned char P3REN4          : 1; /*  */
    unsigned char P3REN5          : 1; /*  */
    unsigned char P3REN6          : 1; /*  */
    unsigned char P3REN7          : 1; /*  */
  }P3REN_bit;
} @0x0226;


enum {
  P3REN0          = 0x0001,
  P3REN1          = 0x0002,
  P3REN2          = 0x0004,
  P3REN3          = 0x0008,
  P3REN4          = 0x0010,
  P3REN5          = 0x0020,
  P3REN6          = 0x0040,
  P3REN7          = 0x0080
};


__no_init volatile union
{
  unsigned char P3DS;   /*  */

  struct
  {
    unsigned char P3DS0           : 1; /*  */
    unsigned char P3DS1           : 1; /*  */
    unsigned char P3DS2           : 1; /*  */
    unsigned char P3DS3           : 1; /*  */
    unsigned char P3DS4           : 1; /*  */
    unsigned char P3DS5           : 1; /*  */
    unsigned char P3DS6           : 1; /*  */
    unsigned char P3DS7           : 1; /*  */
  }P3DS_bit;
} @0x0228;


enum {
  P3DS0           = 0x0001,
  P3DS1           = 0x0002,
  P3DS2           = 0x0004,
  P3DS3           = 0x0008,
  P3DS4           = 0x0010,
  P3DS5           = 0x0020,
  P3DS6           = 0x0040,
  P3DS7           = 0x0080
};


__no_init volatile union
{
  unsigned char P3SEL;   /*  */

  struct
  {
    unsigned char P3SEL0          : 1; /*  */
    unsigned char P3SEL1          : 1; /*  */
    unsigned char P3SEL2          : 1; /*  */
    unsigned char P3SEL3          : 1; /*  */
    unsigned char P3SEL4          : 1; /*  */
    unsigned char P3SEL5          : 1; /*  */
    unsigned char P3SEL6          : 1; /*  */
    unsigned char P3SEL7          : 1; /*  */
  }P3SEL_bit;
} @ 0x022A;


enum {
  P3SEL0          = 0x0001,
  P3SEL1          = 0x0002,
  P3SEL2          = 0x0004,
  P3SEL3          = 0x0008,
  P3SEL4          = 0x0010,
  P3SEL5          = 0x0020,
  P3SEL6          = 0x0040,
  P3SEL7          = 0x0080
};


/*-------------------------------------------------------------------------
 *   Port J
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned __READ short PJIN;   /* Port J Input */

  struct
  {
    unsigned __READ short PJIN0           : 1; /*  */
    unsigned __READ short PJIN1           : 1; /*  */
    unsigned __READ short PJIN2           : 1; /*  */
    unsigned __READ short PJIN3           : 1; /*  */
    unsigned __READ short PJIN4           : 1; /*  */
    unsigned __READ short PJIN5           : 1; /*  */
    unsigned __READ short PJIN6           : 1; /*  */
  }PJIN_bit;
} @0x0320;


enum {
  PJIN0           = 0x0001,
  PJIN1           = 0x0002,
  PJIN2           = 0x0004,
  PJIN3           = 0x0008,
  PJIN4           = 0x0010,
  PJIN5           = 0x0020,
  PJIN6           = 0x0040
};


__no_init volatile union
{
  unsigned short PJOUT;   /* Port J Output */

  struct
  {
    unsigned short PJOUT0          : 1; /*  */
    unsigned short PJOUT1          : 1; /*  */
    unsigned short PJOUT2          : 1; /*  */
    unsigned short PJOUT3          : 1; /*  */
    unsigned short PJOUT4          : 1; /*  */
    unsigned short PJOUT5          : 1; /*  */
    unsigned short PJOUT6          : 1; /*  */
  }PJOUT_bit;
} @0x0322;


enum {
  PJOUT0          = 0x0001,
  PJOUT1          = 0x0002,
  PJOUT2          = 0x0004,
  PJOUT3          = 0x0008,
  PJOUT4          = 0x0010,
  PJOUT5          = 0x0020,
  PJOUT6          = 0x0040
};


__no_init volatile union
{
  unsigned short PJDIR;   /* Port J Direction */

  struct
  {
    unsigned short PJDIR0          : 1; /*  */
    unsigned short PJDIR1          : 1; /*  */
    unsigned short PJDIR2          : 1; /*  */
    unsigned short PJDIR3          : 1; /*  */
    unsigned short PJDIR4          : 1; /*  */
    unsigned short PJDIR5          : 1; /*  */
    unsigned short PJDIR6          : 1; /*  */
  }PJDIR_bit;
} @0x0324;


enum {
  PJDIR0          = 0x0001,
  PJDIR1          = 0x0002,
  PJDIR2          = 0x0004,
  PJDIR3          = 0x0008,
  PJDIR4          = 0x0010,
  PJDIR5          = 0x0020,
  PJDIR6          = 0x0040
};


__no_init volatile union
{
  unsigned short PJREN;   /* Port J Resistor Enable */

  struct
  {
    unsigned short PJREN0          : 1; /*  */
    unsigned short PJREN1          : 1; /*  */
    unsigned short PJREN2          : 1; /*  */
    unsigned short PJREN3          : 1; /*  */
    unsigned short PJREN4          : 1; /*  */
    unsigned short PJREN5          : 1; /*  */
    unsigned short PJREN6          : 1; /*  */
  }PJREN_bit;
} @0x0326;


enum {
  PJREN0          = 0x0001,
  PJREN1          = 0x0002,
  PJREN2          = 0x0004,
  PJREN3          = 0x0008,
  PJREN4          = 0x0010,
  PJREN5          = 0x0020,
  PJREN6          = 0x0040
};


__no_init volatile union
{
  unsigned short PJDS;   /* Port J Resistor Drive Strenght */

  struct
  {
    unsigned short PJDS0           : 1; /*  */
    unsigned short PJDS1           : 1; /*  */
    unsigned short PJDS2           : 1; /*  */
    unsigned short PJDS3           : 1; /*  */
    unsigned short PJDS4           : 1; /*  */
    unsigned short PJDS5           : 1; /*  */
    unsigned short PJDS6           : 1; /*  */
  }PJDS_bit;
} @0x0328;


enum {
  PJDS0           = 0x0001,
  PJDS1           = 0x0002,
  PJDS2           = 0x0004,
  PJDS3           = 0x0008,
  PJDS4           = 0x0010,
  PJDS5           = 0x0020,
  PJDS6           = 0x0040
};


__no_init volatile union
{
  unsigned short PJSEL;   /* Port J Selection */

  struct
  {
    unsigned short PJSEL0          : 1; /*  */
    unsigned short PJSEL1          : 1; /*  */
    unsigned short PJSEL2          : 1; /*  */
    unsigned short PJSEL3          : 1; /*  */
    unsigned short PJSEL4          : 1; /*  */
    unsigned short PJSEL5          : 1; /*  */
    unsigned short PJSEL6          : 1; /*  */
  }PJSEL_bit;
} @ 0x032A;


enum {
  PJSEL0          = 0x0001,
  PJSEL1          = 0x0002,
  PJSEL2          = 0x0004,
  PJSEL3          = 0x0008,
  PJSEL4          = 0x0010,
  PJSEL5          = 0x0020,
  PJSEL6          = 0x0040
};


#define __MSP430_HAS_PORTJ_R__        /* Definition to show that Module is available */


/*-------------------------------------------------------------------------
 *   Port Mapping Control
 *-------------------------------------------------------------------------*/


  /* Port Mapping Key register */
__no_init volatile unsigned short PMAPKEYID @ 0x01C0;


__no_init volatile union
{
  unsigned short PMAPCTL;   /* Port Mapping control register */

  struct
  {
    unsigned short PMAPLOCKED      : 1; /* Port Mapping Lock bit. Read only */
    unsigned short PMAPRECFG       : 1; /* Port Mapping re-configuration control bit */
  }PMAPCTL_bit;
} @ 0x01C2;


enum {
  PMAPLOCKED      = 0x0001,
  PMAPRECFG       = 0x0002
};


#define __MSP430_HAS_PORT_MAPPING__   /* Definition to show that Module is available */


#define  PMAPKEY             (0x2D52u)  /* Port Mapping Key */
#define  PMAPPWD             PMAPKEYID /* Legacy Definition: Mapping Key register */
#define  PMAPPW              (0x2D52u)  /* Legacy Definition: Port Mapping Password */

/* PMAPCTL Control Bits */
#define PMAPLOCKED_L        (0x0001u)  /* Port Mapping Lock bit. Read only */
#define PMAPRECFG_L         (0x0002u)  /* Port Mapping re-configuration control bit */

/*-------------------------------------------------------------------------
 *   Port Mapping Port 1
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short P1MAP01;   /* Port P1.0/1 mapping register */

  struct
  {
    unsigned short PMAP01_0        : 1; /*  */
    unsigned short PMAP01_1        : 1; /*  */
    unsigned short PMAP01_2        : 1; /*  */
    unsigned short PMAP01_3        : 1; /*  */
    unsigned short PMAP01_4        : 1; /*  */
    unsigned short PMAP01_5        : 1; /*  */
    unsigned short PMAP01_6        : 1; /*  */
    unsigned short PMAP01_7        : 1; /*  */
    unsigned short PMAP01_8        : 1; /*  */
    unsigned short PMAP01_9        : 1; /*  */
    unsigned short PMAP01_10       : 1; /*  */
    unsigned short PMAP01_11       : 1; /*  */
    unsigned short PMAP01_12       : 1; /*  */
    unsigned short PMAP01_13       : 1; /*  */
    unsigned short PMAP01_14       : 1; /*  */
    unsigned short PMAP01_15       : 1; /*  */
  }P1MAP01_bit;
} @0x01C8;


enum {
  PMAP01_0        = 0x0001,
  PMAP01_1        = 0x0002,
  PMAP01_2        = 0x0004,
  PMAP01_3        = 0x0008,
  PMAP01_4        = 0x0010,
  PMAP01_5        = 0x0020,
  PMAP01_6        = 0x0040,
  PMAP01_7        = 0x0080,
  PMAP01_8        = 0x0100,
  PMAP01_9        = 0x0200,
  PMAP01_10       = 0x0400,
  PMAP01_11       = 0x0800,
  PMAP01_12       = 0x1000,
  PMAP01_13       = 0x2000,
  PMAP01_14       = 0x4000,
  PMAP01_15       = 0x8000
};


__no_init volatile union
{
  unsigned short P1MAP23;   /* Port P1.2/3 mapping register */

  struct
  {
    unsigned short PMAP23_0        : 1; /*  */
    unsigned short PMAP23_1        : 1; /*  */
    unsigned short PMAP23_2        : 1; /*  */
    unsigned short PMAP23_3        : 1; /*  */
    unsigned short PMAP23_4        : 1; /*  */
    unsigned short PMAP23_5        : 1; /*  */
    unsigned short PMAP23_6        : 1; /*  */
    unsigned short PMAP23_7        : 1; /*  */
    unsigned short PMAP23_8        : 1; /*  */
    unsigned short PMAP23_9        : 1; /*  */
    unsigned short PMAP23_10       : 1; /*  */
    unsigned short PMAP23_11       : 1; /*  */
    unsigned short PMAP23_12       : 1; /*  */
    unsigned short PMAP23_13       : 1; /*  */
    unsigned short PMAP23_14       : 1; /*  */
    unsigned short PMAP23_15       : 1; /*  */
  }P1MAP23_bit;
} @0x01CA;


enum {
  PMAP23_0        = 0x0001,
  PMAP23_1        = 0x0002,
  PMAP23_2        = 0x0004,
  PMAP23_3        = 0x0008,
  PMAP23_4        = 0x0010,
  PMAP23_5        = 0x0020,
  PMAP23_6        = 0x0040,
  PMAP23_7        = 0x0080,
  PMAP23_8        = 0x0100,
  PMAP23_9        = 0x0200,
  PMAP23_10       = 0x0400,
  PMAP23_11       = 0x0800,
  PMAP23_12       = 0x1000,
  PMAP23_13       = 0x2000,
  PMAP23_14       = 0x4000,
  PMAP23_15       = 0x8000
};


__no_init volatile union
{
  unsigned short P1MAP45;   /* Port P1.4/5 mapping register */

  struct
  {
    unsigned short PMAP45_0        : 1; /*  */
    unsigned short PMAP45_1        : 1; /*  */
    unsigned short PMAP45_2        : 1; /*  */
    unsigned short PMAP45_3        : 1; /*  */
    unsigned short PMAP45_4        : 1; /*  */
    unsigned short PMAP45_5        : 1; /*  */
    unsigned short PMAP45_6        : 1; /*  */
    unsigned short PMAP45_7        : 1; /*  */
    unsigned short PMAP45_8        : 1; /*  */
    unsigned short PMAP45_9        : 1; /*  */
    unsigned short PMAP45_10       : 1; /*  */
    unsigned short PMAP45_11       : 1; /*  */
    unsigned short PMAP45_12       : 1; /*  */
    unsigned short PMAP45_13       : 1; /*  */
    unsigned short PMAP45_14       : 1; /*  */
    unsigned short PMAP45_15       : 1; /*  */
  }P1MAP45_bit;
} @0x01CC;


enum {
  PMAP45_0        = 0x0001,
  PMAP45_1        = 0x0002,
  PMAP45_2        = 0x0004,
  PMAP45_3        = 0x0008,
  PMAP45_4        = 0x0010,
  PMAP45_5        = 0x0020,
  PMAP45_6        = 0x0040,
  PMAP45_7        = 0x0080,
  PMAP45_8        = 0x0100,
  PMAP45_9        = 0x0200,
  PMAP45_10       = 0x0400,
  PMAP45_11       = 0x0800,
  PMAP45_12       = 0x1000,
  PMAP45_13       = 0x2000,
  PMAP45_14       = 0x4000,
  PMAP45_15       = 0x8000
};


__no_init volatile union
{
  unsigned short P1MAP67;   /* Port P1.6/7 mapping register */

  struct
  {
    unsigned short PMAP67_0        : 1; /*  */
    unsigned short PMAP67_1        : 1; /*  */
    unsigned short PMAP67_2        : 1; /*  */
    unsigned short PMAP67_3        : 1; /*  */
    unsigned short PMAP67_4        : 1; /*  */
    unsigned short PMAP67_5        : 1; /*  */
    unsigned short PMAP67_6        : 1; /*  */
    unsigned short PMAP67_7        : 1; /*  */
    unsigned short PMAP67_8        : 1; /*  */
    unsigned short PMAP67_9        : 1; /*  */
    unsigned short PMAP67_10       : 1; /*  */
    unsigned short PMAP67_11       : 1; /*  */
    unsigned short PMAP67_12       : 1; /*  */
    unsigned short PMAP67_13       : 1; /*  */
    unsigned short PMAP67_14       : 1; /*  */
    unsigned short PMAP67_15       : 1; /*  */
  }P1MAP67_bit;
} @0x01CE;


enum {
  PMAP67_0        = 0x0001,
  PMAP67_1        = 0x0002,
  PMAP67_2        = 0x0004,
  PMAP67_3        = 0x0008,
  PMAP67_4        = 0x0010,
  PMAP67_5        = 0x0020,
  PMAP67_6        = 0x0040,
  PMAP67_7        = 0x0080,
  PMAP67_8        = 0x0100,
  PMAP67_9        = 0x0200,
  PMAP67_10       = 0x0400,
  PMAP67_11       = 0x0800,
  PMAP67_12       = 0x1000,
  PMAP67_13       = 0x2000,
  PMAP67_14       = 0x4000,
  PMAP67_15       = 0x8000
};


__no_init volatile union
{
  unsigned char P1MAP0;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP0_bit;
} @0x01C8;


enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080
};


__no_init volatile union
{
  unsigned char P1MAP1;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP1_bit;
} @0x01C9;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP2;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP2_bit;
} @0x01CA;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP3;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP3_bit;
} @0x01CB;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP4;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP4_bit;
} @0x01CC;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP5;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP5_bit;
} @0x01CD;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP6;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP6_bit;
} @0x01CE;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P1MAP7;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P1MAP7_bit;
} @ 0x01CF;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080
};
*/


#define __MSP430_HAS_PORT1_MAPPING__   /* Definition to show that Module is available */

#define  P1MAP0              P1MAP01_L /* Port P1.0 mapping register */
#define  P1MAP1              P1MAP01_H /* Port P1.1 mapping register */
#define  P1MAP2              P1MAP23_L /* Port P1.2 mapping register */
#define  P1MAP3              P1MAP23_H /* Port P1.3 mapping register */
#define  P1MAP4              P1MAP45_L /* Port P1.4 mapping register */
#define  P1MAP5              P1MAP45_H /* Port P1.5 mapping register */
#define  P1MAP6              P1MAP67_L /* Port P1.6 mapping register */
#define  P1MAP7              P1MAP67_H /* Port P1.7 mapping register */

/*-------------------------------------------------------------------------
 *   Port Mapping Port 2
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short P2MAP01;   /* Port P2.0/1 mapping register */

  struct
  {
    unsigned short PMAP01_0        : 1; /*  */
    unsigned short PMAP01_1        : 1; /*  */
    unsigned short PMAP01_2        : 1; /*  */
    unsigned short PMAP01_3        : 1; /*  */
    unsigned short PMAP01_4        : 1; /*  */
    unsigned short PMAP01_5        : 1; /*  */
    unsigned short PMAP01_6        : 1; /*  */
    unsigned short PMAP01_7        : 1; /*  */
    unsigned short PMAP01_8        : 1; /*  */
    unsigned short PMAP01_9        : 1; /*  */
    unsigned short PMAP01_10       : 1; /*  */
    unsigned short PMAP01_11       : 1; /*  */
    unsigned short PMAP01_12       : 1; /*  */
    unsigned short PMAP01_13       : 1; /*  */
    unsigned short PMAP01_14       : 1; /*  */
    unsigned short PMAP01_15       : 1; /*  */
  }P2MAP01_bit;
} @0x01D0;


/*
enum {
  PMAP01_0        = 0x0001,
  PMAP01_1        = 0x0002,
  PMAP01_2        = 0x0004,
  PMAP01_3        = 0x0008,
  PMAP01_4        = 0x0010,
  PMAP01_5        = 0x0020,
  PMAP01_6        = 0x0040,
  PMAP01_7        = 0x0080,
  PMAP01_8        = 0x0100,
  PMAP01_9        = 0x0200,
  PMAP01_10       = 0x0400,
  PMAP01_11       = 0x0800,
  PMAP01_12       = 0x1000,
  PMAP01_13       = 0x2000,
  PMAP01_14       = 0x4000,
  PMAP01_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P2MAP23;   /* Port P2.2/3 mapping register */

  struct
  {
    unsigned short PMAP23_0        : 1; /*  */
    unsigned short PMAP23_1        : 1; /*  */
    unsigned short PMAP23_2        : 1; /*  */
    unsigned short PMAP23_3        : 1; /*  */
    unsigned short PMAP23_4        : 1; /*  */
    unsigned short PMAP23_5        : 1; /*  */
    unsigned short PMAP23_6        : 1; /*  */
    unsigned short PMAP23_7        : 1; /*  */
    unsigned short PMAP23_8        : 1; /*  */
    unsigned short PMAP23_9        : 1; /*  */
    unsigned short PMAP23_10       : 1; /*  */
    unsigned short PMAP23_11       : 1; /*  */
    unsigned short PMAP23_12       : 1; /*  */
    unsigned short PMAP23_13       : 1; /*  */
    unsigned short PMAP23_14       : 1; /*  */
    unsigned short PMAP23_15       : 1; /*  */
  }P2MAP23_bit;
} @0x01D2;


/*
enum {
  PMAP23_0        = 0x0001,
  PMAP23_1        = 0x0002,
  PMAP23_2        = 0x0004,
  PMAP23_3        = 0x0008,
  PMAP23_4        = 0x0010,
  PMAP23_5        = 0x0020,
  PMAP23_6        = 0x0040,
  PMAP23_7        = 0x0080,
  PMAP23_8        = 0x0100,
  PMAP23_9        = 0x0200,
  PMAP23_10       = 0x0400,
  PMAP23_11       = 0x0800,
  PMAP23_12       = 0x1000,
  PMAP23_13       = 0x2000,
  PMAP23_14       = 0x4000,
  PMAP23_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P2MAP45;   /* Port P2.4/5 mapping register */

  struct
  {
    unsigned short PMAP45_0        : 1; /*  */
    unsigned short PMAP45_1        : 1; /*  */
    unsigned short PMAP45_2        : 1; /*  */
    unsigned short PMAP45_3        : 1; /*  */
    unsigned short PMAP45_4        : 1; /*  */
    unsigned short PMAP45_5        : 1; /*  */
    unsigned short PMAP45_6        : 1; /*  */
    unsigned short PMAP45_7        : 1; /*  */
    unsigned short PMAP45_8        : 1; /*  */
    unsigned short PMAP45_9        : 1; /*  */
    unsigned short PMAP45_10       : 1; /*  */
    unsigned short PMAP45_11       : 1; /*  */
    unsigned short PMAP45_12       : 1; /*  */
    unsigned short PMAP45_13       : 1; /*  */
    unsigned short PMAP45_14       : 1; /*  */
    unsigned short PMAP45_15       : 1; /*  */
  }P2MAP45_bit;
} @0x01D4;


/*
enum {
  PMAP45_0        = 0x0001,
  PMAP45_1        = 0x0002,
  PMAP45_2        = 0x0004,
  PMAP45_3        = 0x0008,
  PMAP45_4        = 0x0010,
  PMAP45_5        = 0x0020,
  PMAP45_6        = 0x0040,
  PMAP45_7        = 0x0080,
  PMAP45_8        = 0x0100,
  PMAP45_9        = 0x0200,
  PMAP45_10       = 0x0400,
  PMAP45_11       = 0x0800,
  PMAP45_12       = 0x1000,
  PMAP45_13       = 0x2000,
  PMAP45_14       = 0x4000,
  PMAP45_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P2MAP67;   /* Port P2.6/7 mapping register */

  struct
  {
    unsigned short PMAP67_0        : 1; /*  */
    unsigned short PMAP67_1        : 1; /*  */
    unsigned short PMAP67_2        : 1; /*  */
    unsigned short PMAP67_3        : 1; /*  */
    unsigned short PMAP67_4        : 1; /*  */
    unsigned short PMAP67_5        : 1; /*  */
    unsigned short PMAP67_6        : 1; /*  */
    unsigned short PMAP67_7        : 1; /*  */
    unsigned short PMAP67_8        : 1; /*  */
    unsigned short PMAP67_9        : 1; /*  */
    unsigned short PMAP67_10       : 1; /*  */
    unsigned short PMAP67_11       : 1; /*  */
    unsigned short PMAP67_12       : 1; /*  */
    unsigned short PMAP67_13       : 1; /*  */
    unsigned short PMAP67_14       : 1; /*  */
    unsigned short PMAP67_15       : 1; /*  */
  }P2MAP67_bit;
} @0x01D6;


/*
enum {
  PMAP67_0        = 0x0001,
  PMAP67_1        = 0x0002,
  PMAP67_2        = 0x0004,
  PMAP67_3        = 0x0008,
  PMAP67_4        = 0x0010,
  PMAP67_5        = 0x0020,
  PMAP67_6        = 0x0040,
  PMAP67_7        = 0x0080,
  PMAP67_8        = 0x0100,
  PMAP67_9        = 0x0200,
  PMAP67_10       = 0x0400,
  PMAP67_11       = 0x0800,
  PMAP67_12       = 0x1000,
  PMAP67_13       = 0x2000,
  PMAP67_14       = 0x4000,
  PMAP67_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned char P2MAP0;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP0_bit;
} @0x01D0;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP1;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP1_bit;
} @0x01D1;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP2;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP2_bit;
} @0x01D2;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP3;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP3_bit;
} @0x01D3;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP4;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP4_bit;
} @0x01D4;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP5;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP5_bit;
} @0x01D5;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP6;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP6_bit;
} @0x01D6;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P2MAP7;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P2MAP7_bit;
} @ 0x01D7;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080
};
*/


#define __MSP430_HAS_PORT2_MAPPING__   /* Definition to show that Module is available */

#define  P2MAP0              P2MAP01_L /* Port P2.0 mapping register */
#define  P2MAP1              P2MAP01_H /* Port P2.1 mapping register */
#define  P2MAP2              P2MAP23_L /* Port P2.2 mapping register */
#define  P2MAP3              P2MAP23_H /* Port P2.3 mapping register */
#define  P2MAP4              P2MAP45_L /* Port P2.4 mapping register */
#define  P2MAP5              P2MAP45_H /* Port P2.5 mapping register */
#define  P2MAP6              P2MAP67_L /* Port P2.6 mapping register */
#define  P2MAP7              P2MAP67_H /* Port P2.7 mapping register */

/*-------------------------------------------------------------------------
 *   Port Mapping Port 3
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short P3MAP01;   /* Port P3.0/1 mapping register */

  struct
  {
    unsigned short PMAP01_0        : 1; /*  */
    unsigned short PMAP01_1        : 1; /*  */
    unsigned short PMAP01_2        : 1; /*  */
    unsigned short PMAP01_3        : 1; /*  */
    unsigned short PMAP01_4        : 1; /*  */
    unsigned short PMAP01_5        : 1; /*  */
    unsigned short PMAP01_6        : 1; /*  */
    unsigned short PMAP01_7        : 1; /*  */
    unsigned short PMAP01_8        : 1; /*  */
    unsigned short PMAP01_9        : 1; /*  */
    unsigned short PMAP01_10       : 1; /*  */
    unsigned short PMAP01_11       : 1; /*  */
    unsigned short PMAP01_12       : 1; /*  */
    unsigned short PMAP01_13       : 1; /*  */
    unsigned short PMAP01_14       : 1; /*  */
    unsigned short PMAP01_15       : 1; /*  */
  }P3MAP01_bit;
} @0x01D8;


/*
enum {
  PMAP01_0        = 0x0001,
  PMAP01_1        = 0x0002,
  PMAP01_2        = 0x0004,
  PMAP01_3        = 0x0008,
  PMAP01_4        = 0x0010,
  PMAP01_5        = 0x0020,
  PMAP01_6        = 0x0040,
  PMAP01_7        = 0x0080,
  PMAP01_8        = 0x0100,
  PMAP01_9        = 0x0200,
  PMAP01_10       = 0x0400,
  PMAP01_11       = 0x0800,
  PMAP01_12       = 0x1000,
  PMAP01_13       = 0x2000,
  PMAP01_14       = 0x4000,
  PMAP01_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P3MAP23;   /* Port P3.2/3 mapping register */

  struct
  {
    unsigned short PMAP23_0        : 1; /*  */
    unsigned short PMAP23_1        : 1; /*  */
    unsigned short PMAP23_2        : 1; /*  */
    unsigned short PMAP23_3        : 1; /*  */
    unsigned short PMAP23_4        : 1; /*  */
    unsigned short PMAP23_5        : 1; /*  */
    unsigned short PMAP23_6        : 1; /*  */
    unsigned short PMAP23_7        : 1; /*  */
    unsigned short PMAP23_8        : 1; /*  */
    unsigned short PMAP23_9        : 1; /*  */
    unsigned short PMAP23_10       : 1; /*  */
    unsigned short PMAP23_11       : 1; /*  */
    unsigned short PMAP23_12       : 1; /*  */
    unsigned short PMAP23_13       : 1; /*  */
    unsigned short PMAP23_14       : 1; /*  */
    unsigned short PMAP23_15       : 1; /*  */
  }P3MAP23_bit;
} @0x01DA;


/*
enum {
  PMAP23_0        = 0x0001,
  PMAP23_1        = 0x0002,
  PMAP23_2        = 0x0004,
  PMAP23_3        = 0x0008,
  PMAP23_4        = 0x0010,
  PMAP23_5        = 0x0020,
  PMAP23_6        = 0x0040,
  PMAP23_7        = 0x0080,
  PMAP23_8        = 0x0100,
  PMAP23_9        = 0x0200,
  PMAP23_10       = 0x0400,
  PMAP23_11       = 0x0800,
  PMAP23_12       = 0x1000,
  PMAP23_13       = 0x2000,
  PMAP23_14       = 0x4000,
  PMAP23_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P3MAP45;   /* Port P3.4/5 mapping register */

  struct
  {
    unsigned short PMAP45_0        : 1; /*  */
    unsigned short PMAP45_1        : 1; /*  */
    unsigned short PMAP45_2        : 1; /*  */
    unsigned short PMAP45_3        : 1; /*  */
    unsigned short PMAP45_4        : 1; /*  */
    unsigned short PMAP45_5        : 1; /*  */
    unsigned short PMAP45_6        : 1; /*  */
    unsigned short PMAP45_7        : 1; /*  */
    unsigned short PMAP45_8        : 1; /*  */
    unsigned short PMAP45_9        : 1; /*  */
    unsigned short PMAP45_10       : 1; /*  */
    unsigned short PMAP45_11       : 1; /*  */
    unsigned short PMAP45_12       : 1; /*  */
    unsigned short PMAP45_13       : 1; /*  */
    unsigned short PMAP45_14       : 1; /*  */
    unsigned short PMAP45_15       : 1; /*  */
  }P3MAP45_bit;
} @0x01DC;


/*
enum {
  PMAP45_0        = 0x0001,
  PMAP45_1        = 0x0002,
  PMAP45_2        = 0x0004,
  PMAP45_3        = 0x0008,
  PMAP45_4        = 0x0010,
  PMAP45_5        = 0x0020,
  PMAP45_6        = 0x0040,
  PMAP45_7        = 0x0080,
  PMAP45_8        = 0x0100,
  PMAP45_9        = 0x0200,
  PMAP45_10       = 0x0400,
  PMAP45_11       = 0x0800,
  PMAP45_12       = 0x1000,
  PMAP45_13       = 0x2000,
  PMAP45_14       = 0x4000,
  PMAP45_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short P3MAP67;   /* Port P3.6/7 mapping register */

  struct
  {
    unsigned short PMAP67_0        : 1; /*  */
    unsigned short PMAP67_1        : 1; /*  */
    unsigned short PMAP67_2        : 1; /*  */
    unsigned short PMAP67_3        : 1; /*  */
    unsigned short PMAP67_4        : 1; /*  */
    unsigned short PMAP67_5        : 1; /*  */
    unsigned short PMAP67_6        : 1; /*  */
    unsigned short PMAP67_7        : 1; /*  */
    unsigned short PMAP67_8        : 1; /*  */
    unsigned short PMAP67_9        : 1; /*  */
    unsigned short PMAP67_10       : 1; /*  */
    unsigned short PMAP67_11       : 1; /*  */
    unsigned short PMAP67_12       : 1; /*  */
    unsigned short PMAP67_13       : 1; /*  */
    unsigned short PMAP67_14       : 1; /*  */
    unsigned short PMAP67_15       : 1; /*  */
  }P3MAP67_bit;
} @0x01DE;


/*
enum {
  PMAP67_0        = 0x0001,
  PMAP67_1        = 0x0002,
  PMAP67_2        = 0x0004,
  PMAP67_3        = 0x0008,
  PMAP67_4        = 0x0010,
  PMAP67_5        = 0x0020,
  PMAP67_6        = 0x0040,
  PMAP67_7        = 0x0080,
  PMAP67_8        = 0x0100,
  PMAP67_9        = 0x0200,
  PMAP67_10       = 0x0400,
  PMAP67_11       = 0x0800,
  PMAP67_12       = 0x1000,
  PMAP67_13       = 0x2000,
  PMAP67_14       = 0x4000,
  PMAP67_15       = 0x8000,
};
*/


__no_init volatile union
{
  unsigned char P3MAP0;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP0_bit;
} @0x01D8;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP1;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP1_bit;
} @0x01D9;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP2;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP2_bit;
} @0x01DA;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP3;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP3_bit;
} @0x01DB;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP4;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP4_bit;
} @0x01DC;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP5;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP5_bit;
} @0x01DD;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP6;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP6_bit;
} @0x01DE;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char P3MAP7;   /*  */

  struct
  {
    unsigned char PMAP0           : 1; /*  */
    unsigned char PMAP1           : 1; /*  */
    unsigned char PMAP2           : 1; /*  */
    unsigned char PMAP3           : 1; /*  */
    unsigned char PMAP4           : 1; /*  */
    unsigned char PMAP5           : 1; /*  */
    unsigned char PMAP6           : 1; /*  */
    unsigned char PMAP7           : 1; /*  */
  }P3MAP7_bit;
} @ 0x01DF;


/*
enum {
  PMAP0           = 0x0001,
  PMAP1           = 0x0002,
  PMAP2           = 0x0004,
  PMAP3           = 0x0008,
  PMAP4           = 0x0010,
  PMAP5           = 0x0020,
  PMAP6           = 0x0040,
  PMAP7           = 0x0080
};
*/


#define __MSP430_HAS_PORT3_MAPPING__   /* Definition to show that Module is available */

#define  P3MAP0              P3MAP01_L /* Port P3.0 mapping register */
#define  P3MAP1              P3MAP01_H /* Port P3.1 mapping register */
#define  P3MAP2              P3MAP23_L /* Port P3.2 mapping register */
#define  P3MAP3              P3MAP23_H /* Port P3.3 mapping register */
#define  P3MAP4              P3MAP45_L /* Port P3.4 mapping register */
#define  P3MAP5              P3MAP45_H /* Port P3.5 mapping register */
#define  P3MAP6              P3MAP67_L /* Port P3.6 mapping register */
#define  P3MAP7              P3MAP67_H /* Port P3.7 mapping register */

#define PM_NONE       0
#define PM_UCA0CLK    1
#define PM_UCB0STE    1
#define PM_UCA0TXD    2
#define PM_UCA0SIMO   2
#define PM_UCB0SOMO   3
#define PM_UCB0SCL    3
#define PM_UCA0RXD    4
#define PM_UCA0SOMI   4
#define PM_UCB0SIMO   5
#define PM_UCB0SDA    5
#define PM_UCB0CLK    6
#define PM_UCA0STE    6
#define PM_TD0_0      7
#define PM_TD0_1      8
#define PM_TD0_2      9
#define PM_TD1_0      10
#define PM_TD1_1      11
#define PM_TD1_2      12
#define PM_CLR1TD0_0  13
#define PM_FLT1_2TD0_0 13
#define PM_FLT1_0TD0_1 14
#define PM_FLT1_1TD0_2 15
#define PM_CLR2TD1_0   16
#define PM_FLT2_1TD1_0 16
#define PM_FLT2_2TD1_1 17
#define PM_FLT2_0TD1_2 18
#define PM_TD0_0SMCLK  19
#define PM_TA0CLKCBOUT 20
#define PM_TD0CLKMCLK  21
#define PM_TA0_0       22
#define PM_TA0_1       23
#define PM_TA0_2       24
#define PM_DMAE0SMCLK  25
#define PM_DMAE1MCLK   26
#define PM_DMAE2SVM    27
#define PM_TD0OUTH     28
#define PM_TD1OUTH     29
#define PM_ANALOG      31

/*-------------------------------------------------------------------------
 *   PMM  Power Management System
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short PMMCTL0;   /* PMM Control 0 */

  struct
  {
    unsigned short PMMCOREV0       : 1; /* PMM Core Voltage Bit: 0 */
    unsigned short PMMCOREV1       : 1; /* PMM Core Voltage Bit: 1 */
    unsigned short PMMSWBOR        : 1; /* PMM Software BOR */
    unsigned short PMMSWPOR        : 1; /* PMM Software POR */
    unsigned short PMMREGOFF       : 1; /* PMM Turn Regulator off */
    unsigned short                : 2;
    unsigned short PMMHPMRE        : 1; /* PMM Global High Power Module Request Enable */
  }PMMCTL0_bit;
} @0x0120;


enum {
  PMMCOREV0       = 0x0001,
  PMMCOREV1       = 0x0002,
  PMMSWBOR        = 0x0004,
  PMMSWPOR        = 0x0008,
  PMMREGOFF       = 0x0010,
  PMMHPMRE        = 0x0080
};


__no_init volatile union
{
  unsigned short PMMCTL1;   /* PMM Control 1 */

  struct
  {
    unsigned short PMMREFMD        : 1; /* PMM Reference Mode */
    unsigned short                : 3;
    unsigned short PMMCMD0         : 1; /* PMM Voltage Regulator Current Mode Bit: 0 */
    unsigned short PMMCMD1         : 1; /* PMM Voltage Regulator Current Mode Bit: 1 */
  }PMMCTL1_bit;
} @0x0122;


enum {
  PMMREFMD        = 0x0001,
  PMMCMD0         = 0x0010,
  PMMCMD1         = 0x0020
};


__no_init volatile union
{
  unsigned short SVSMHCTL;   /* SVS and SVM high side control register */

  struct
  {
    unsigned short SVSMHRRL0       : 1; /* SVS and SVM high side Reset Release Voltage Level Bit: 0 */
    unsigned short SVSMHRRL1       : 1; /* SVS and SVM high side Reset Release Voltage Level Bit: 1 */
    unsigned short SVSMHRRL2       : 1; /* SVS and SVM high side Reset Release Voltage Level Bit: 2 */
    unsigned short SVSMHDLYST      : 1; /* SVS and SVM high side delay status */
    unsigned short SVSHMD          : 1; /* SVS high side mode */
    unsigned short                : 1;
    unsigned short SVSMHEVM        : 1; /* SVS and SVM high side event mask */
    unsigned short SVSMHACE        : 1; /* SVS and SVM high side auto control enable */
    unsigned short SVSHRVL0        : 1; /* SVS high side reset voltage level Bit: 0 */
    unsigned short SVSHRVL1        : 1; /* SVS high side reset voltage level Bit: 1 */
    unsigned short SVSHE           : 1; /* SVS high side enable */
    unsigned short SVSHFP          : 1; /* SVS high side full performace mode */
    unsigned short SVMHOVPE        : 1; /* SVM high side over-voltage enable */
    unsigned short                : 1;
    unsigned short SVMHE           : 1; /* SVM high side enable */
    unsigned short SVMHFP          : 1; /* SVM high side full performace mode */
  }SVSMHCTL_bit;
} @0x0124;


enum {
  SVSMHRRL0       = 0x0001,
  SVSMHRRL1       = 0x0002,
  SVSMHRRL2       = 0x0004,
  SVSMHDLYST      = 0x0008,
  SVSHMD          = 0x0010,
  SVSMHEVM        = 0x0040,
  SVSMHACE        = 0x0080,
  SVSHRVL0        = 0x0100,
  SVSHRVL1        = 0x0200,
  SVSHE           = 0x0400,
  SVSHFP          = 0x0800,
  SVMHOVPE        = 0x1000,
  SVMHE           = 0x4000,
  SVMHFP          = 0x8000
};


__no_init volatile union
{
  unsigned short SVSMLCTL;   /* SVS and SVM low side control register */

  struct
  {
    unsigned short SVSMLRRL0       : 1; /* SVS and SVM low side Reset Release Voltage Level Bit: 0 */
    unsigned short SVSMLRRL1       : 1; /* SVS and SVM low side Reset Release Voltage Level Bit: 1 */
    unsigned short SVSMLRRL2       : 1; /* SVS and SVM low side Reset Release Voltage Level Bit: 2 */
    unsigned short SVSMLDLYST      : 1; /* SVS and SVM low side delay status */
    unsigned short SVSLMD          : 1; /* SVS low side mode */
    unsigned short                : 1;
    unsigned short SVSMLEVM        : 1; /* SVS and SVM low side event mask */
    unsigned short SVSMLACE        : 1; /* SVS and SVM low side auto control enable */
    unsigned short SVSLRVL0        : 1; /* SVS low side reset voltage level Bit: 0 */
    unsigned short SVSLRVL1        : 1; /* SVS low side reset voltage level Bit: 1 */
    unsigned short SVSLE           : 1; /* SVS low side enable */
    unsigned short SVSLFP          : 1; /* SVS low side full performace mode */
    unsigned short SVMLOVPE        : 1; /* SVM low side over-voltage enable */
    unsigned short                : 1;
    unsigned short SVMLE           : 1; /* SVM low side enable */
    unsigned short SVMLFP          : 1; /* SVM low side full performace mode */
  }SVSMLCTL_bit;
} @0x0126;


enum {
  SVSMLRRL0       = 0x0001,
  SVSMLRRL1       = 0x0002,
  SVSMLRRL2       = 0x0004,
  SVSMLDLYST      = 0x0008,
  SVSLMD          = 0x0010,
  SVSMLEVM        = 0x0040,
  SVSMLACE        = 0x0080,
  SVSLRVL0        = 0x0100,
  SVSLRVL1        = 0x0200,
  SVSLE           = 0x0400,
  SVSLFP          = 0x0800,
  SVMLOVPE        = 0x1000,
  SVMLE           = 0x4000,
  SVMLFP          = 0x8000
};


__no_init volatile union
{
  unsigned short SVSMIO;   /* SVSIN and SVSOUT control register */

  struct
  {
    unsigned short                : 3;
    unsigned short SVMLOE          : 1; /* SVM low side output enable */
    unsigned short SVMLVLROE       : 1; /* SVM low side voltage level reached output enable */
    unsigned short SVMOUTPOL       : 1; /* SVMOUT pin polarity */
    unsigned short                : 5;
    unsigned short SVMHOE          : 1; /* SVM high side output enable */
    unsigned short SVMHVLROE       : 1; /* SVM high side voltage level reached output enable */
  }SVSMIO_bit;
} @0x0128;


enum {
  SVMLOE          = 0x0008,
  SVMLVLROE       = 0x0010,
  SVMOUTPOL       = 0x0020,
  SVMHOE          = 0x0800,
  SVMHVLROE       = 0x1000
};


__no_init volatile union
{
  unsigned short PMMIFG;   /* PMM Interrupt Flag */

  struct
  {
    unsigned short SVSMLDLYIFG     : 1; /* SVS and SVM low side Delay expired interrupt flag */
    unsigned short SVMLIFG         : 1; /* SVM low side interrupt flag */
    unsigned short SVMLVLRIFG      : 1; /* SVM low side Voltage Level Reached interrupt flag */
    unsigned short                : 1;
    unsigned short SVSMHDLYIFG     : 1; /* SVS and SVM high side Delay expired interrupt flag */
    unsigned short SVMHIFG         : 1; /* SVM high side interrupt flag */
    unsigned short SVMHVLRIFG      : 1; /* SVM high side Voltage Level Reached interrupt flag */
    unsigned short                : 1;
    unsigned short PMMBORIFG       : 1; /* PMM Software BOR interrupt flag */
    unsigned short PMMRSTIFG       : 1; /* PMM RESET pin interrupt flag */
    unsigned short PMMPORIFG       : 1; /* PMM Software POR interrupt flag */
    unsigned short                : 1;
    unsigned short SVSHIFG         : 1; /* SVS low side interrupt flag */
    unsigned short SVSLIFG         : 1; /* SVS high side interrupt flag */
    unsigned short                : 1;
    unsigned short PMMLPM5IFG      : 1; /* LPM5 indication Flag */
  }PMMIFG_bit;
} @0x012C;


enum {
  SVSMLDLYIFG     = 0x0001,
  SVMLIFG         = 0x0002,
  SVMLVLRIFG      = 0x0004,
  SVSMHDLYIFG     = 0x0010,
  SVMHIFG         = 0x0020,
  SVMHVLRIFG      = 0x0040,
  PMMBORIFG       = 0x0100,
  PMMRSTIFG       = 0x0200,
  PMMPORIFG       = 0x0400,
  SVSHIFG         = 0x1000,
  SVSLIFG         = 0x2000,
  PMMLPM5IFG      = 0x8000
};


__no_init volatile union
{
  unsigned short PMMRIE;   /* PMM and RESET Interrupt Enable */

  struct
  {
    unsigned short SVSMLDLYIE      : 1; /* SVS and SVM low side Delay expired interrupt enable */
    unsigned short SVMLIE          : 1; /* SVM low side interrupt enable */
    unsigned short SVMLVLRIE       : 1; /* SVM low side Voltage Level Reached interrupt enable */
    unsigned short                : 1;
    unsigned short SVSMHDLYIE      : 1; /* SVS and SVM high side Delay expired interrupt enable */
    unsigned short SVMHIE          : 1; /* SVM high side interrupt enable */
    unsigned short SVMHVLRIE       : 1; /* SVM high side Voltage Level Reached interrupt enable */
    unsigned short                : 1;
    unsigned short SVSLPE          : 1; /* SVS low side POR enable */
    unsigned short SVMLVLRPE       : 1; /* SVM low side Voltage Level reached POR enable */
    unsigned short                : 2;
    unsigned short SVSHPE          : 1; /* SVS high side POR enable */
    unsigned short SVMHVLRPE       : 1; /* SVM high side Voltage Level reached POR enable */
  }PMMRIE_bit;
} @0x012E;


enum {
  SVSMLDLYIE      = 0x0001,
  SVMLIE          = 0x0002,
  SVMLVLRIE       = 0x0004,
  SVSMHDLYIE      = 0x0010,
  SVMHIE          = 0x0020,
  SVMHVLRIE       = 0x0040,
  SVSLPE          = 0x0100,
  SVMLVLRPE       = 0x0200,
  SVSHPE          = 0x1000,
  SVMHVLRPE       = 0x2000
};


__no_init volatile union
{
  unsigned short PM5CTL0;   /* PMM Power Mode 5 Control Register 0 */

  struct
  {
    unsigned short LOCKLPM5        : 1; /* Lock I/O pin configuration upon entry/exit to/from LPM5 */
  }PM5CTL0_bit;
} @ 0x0130;


enum {
  LOCKLPM5        = 0x0001
};


#define __MSP430_HAS_PMM__            /* Definition to show that Module is available */


#define PMMPW               (0xA500u)  /* PMM Register Write Password */
#define PMMPW_H             (0xA5)    /* PMM Register Write Password for high word access */

/* PMMCTL0 Control Bits */
#define PMMCOREV0_L         (0x0001u)  /* PMM Core Voltage Bit: 0 */
#define PMMCOREV1_L         (0x0002u)  /* PMM Core Voltage Bit: 1 */
#define PMMSWBOR_L          (0x0004u)  /* PMM Software BOR */
#define PMMSWPOR_L          (0x0008u)  /* PMM Software POR */
#define PMMREGOFF_L         (0x0010u)  /* PMM Turn Regulator off */
#define PMMHPMRE_L          (0x0080u)  /* PMM Global High Power Module Request Enable */

#define PMMCOREV_0          (0x0000u)  /* PMM Core Voltage 0 (1.35V) */
#define PMMCOREV_1          (0x0001u)  /* PMM Core Voltage 1 (1.55V) */
#define PMMCOREV_2          (0x0002u)  /* PMM Core Voltage 2 (1.75V) */
#define PMMCOREV_3          (0x0003u)  /* PMM Core Voltage 3 (1.85V) */

/* PMMCTL1 Control Bits */
#define PMMREFMD_L          (0x0001u)  /* PMM Reference Mode */
#define PMMCMD0_L           (0x0010u)  /* PMM Voltage Regulator Current Mode Bit: 0 */
#define PMMCMD1_L           (0x0020u)  /* PMM Voltage Regulator Current Mode Bit: 1 */

/* SVSMHCTL Control Bits */
#define SVSMHRRL0_L         (0x0001u)  /* SVS and SVM high side Reset Release Voltage Level Bit: 0 */
#define SVSMHRRL1_L         (0x0002u)  /* SVS and SVM high side Reset Release Voltage Level Bit: 1 */
#define SVSMHRRL2_L         (0x0004u)  /* SVS and SVM high side Reset Release Voltage Level Bit: 2 */
#define SVSMHDLYST_L        (0x0008u)  /* SVS and SVM high side delay status */
#define SVSHMD_L            (0x0010u)  /* SVS high side mode */
#define SVSMHEVM_L          (0x0040u)  /* SVS and SVM high side event mask */
#define SVSMHACE_L          (0x0080u)  /* SVS and SVM high side auto control enable */

/* SVSMHCTL Control Bits */
#define SVSHRVL0_H          (0x0001u)  /* SVS high side reset voltage level Bit: 0 */
#define SVSHRVL1_H          (0x0002u)  /* SVS high side reset voltage level Bit: 1 */
#define SVSHE_H             (0x0004u)  /* SVS high side enable */
#define SVSHFP_H            (0x0008u)  /* SVS high side full performace mode */
#define SVMHOVPE_H          (0x0010u)  /* SVM high side over-voltage enable */
#define SVMHE_H             (0x0040u)  /* SVM high side enable */
#define SVMHFP_H            (0x0080u)  /* SVM high side full performace mode */

#define SVSMHRRL_0          (0x0000u)  /* SVS and SVM high side Reset Release Voltage Level 0 */
#define SVSMHRRL_1          (0x0001u)  /* SVS and SVM high side Reset Release Voltage Level 1 */
#define SVSMHRRL_2          (0x0002u)  /* SVS and SVM high side Reset Release Voltage Level 2 */
#define SVSMHRRL_3          (0x0003u)  /* SVS and SVM high side Reset Release Voltage Level 3 */
#define SVSMHRRL_4          (0x0004u)  /* SVS and SVM high side Reset Release Voltage Level 4 */
#define SVSMHRRL_5          (0x0005u)  /* SVS and SVM high side Reset Release Voltage Level 5 */
#define SVSMHRRL_6          (0x0006u)  /* SVS and SVM high side Reset Release Voltage Level 6 */
#define SVSMHRRL_7          (0x0007u)  /* SVS and SVM high side Reset Release Voltage Level 7 */

#define SVSHRVL_0           (0x0000u)  /* SVS high side Reset Release Voltage Level 0 */
#define SVSHRVL_1           (0x0100u)  /* SVS high side Reset Release Voltage Level 1 */
#define SVSHRVL_2           (0x0200u)  /* SVS high side Reset Release Voltage Level 2 */
#define SVSHRVL_3           (0x0300u)  /* SVS high side Reset Release Voltage Level 3 */

/* SVSMLCTL Control Bits */
#define SVSMLRRL0_L         (0x0001u)  /* SVS and SVM low side Reset Release Voltage Level Bit: 0 */
#define SVSMLRRL1_L         (0x0002u)  /* SVS and SVM low side Reset Release Voltage Level Bit: 1 */
#define SVSMLRRL2_L         (0x0004u)  /* SVS and SVM low side Reset Release Voltage Level Bit: 2 */
#define SVSMLDLYST_L        (0x0008u)  /* SVS and SVM low side delay status */
#define SVSLMD_L            (0x0010u)  /* SVS low side mode */
#define SVSMLEVM_L          (0x0040u)  /* SVS and SVM low side event mask */
#define SVSMLACE_L          (0x0080u)  /* SVS and SVM low side auto control enable */

/* SVSMLCTL Control Bits */
#define SVSLRVL0_H          (0x0001u)  /* SVS low side reset voltage level Bit: 0 */
#define SVSLRVL1_H          (0x0002u)  /* SVS low side reset voltage level Bit: 1 */
#define SVSLE_H             (0x0004u)  /* SVS low side enable */
#define SVSLFP_H            (0x0008u)  /* SVS low side full performace mode */
#define SVMLOVPE_H          (0x0010u)  /* SVM low side over-voltage enable */
#define SVMLE_H             (0x0040u)  /* SVM low side enable */
#define SVMLFP_H            (0x0080u)  /* SVM low side full performace mode */

#define SVSMLRRL_0          (0x0000u)  /* SVS and SVM low side Reset Release Voltage Level 0 */
#define SVSMLRRL_1          (0x0001u)  /* SVS and SVM low side Reset Release Voltage Level 1 */
#define SVSMLRRL_2          (0x0002u)  /* SVS and SVM low side Reset Release Voltage Level 2 */
#define SVSMLRRL_3          (0x0003u)  /* SVS and SVM low side Reset Release Voltage Level 3 */
#define SVSMLRRL_4          (0x0004u)  /* SVS and SVM low side Reset Release Voltage Level 4 */
#define SVSMLRRL_5          (0x0005u)  /* SVS and SVM low side Reset Release Voltage Level 5 */
#define SVSMLRRL_6          (0x0006u)  /* SVS and SVM low side Reset Release Voltage Level 6 */
#define SVSMLRRL_7          (0x0007u)  /* SVS and SVM low side Reset Release Voltage Level 7 */

#define SVSLRVL_0           (0x0000u)  /* SVS low side Reset Release Voltage Level 0 */
#define SVSLRVL_1           (0x0100u)  /* SVS low side Reset Release Voltage Level 1 */
#define SVSLRVL_2           (0x0200u)  /* SVS low side Reset Release Voltage Level 2 */
#define SVSLRVL_3           (0x0300u)  /* SVS low side Reset Release Voltage Level 3 */

/* SVSMIO Control Bits */
#define SVMLOE_L            (0x0008u)  /* SVM low side output enable */
#define SVMLVLROE_L         (0x0010u)  /* SVM low side voltage level reached output enable */
#define SVMOUTPOL_L         (0x0020u)  /* SVMOUT pin polarity */

/* SVSMIO Control Bits */
#define SVMHOE_H            (0x0008u)  /* SVM high side output enable */
#define SVMHVLROE_H         (0x0010u)  /* SVM high side voltage level reached output enable */

/* PMMIFG Control Bits */
#define SVSMLDLYIFG_L       (0x0001u)  /* SVS and SVM low side Delay expired interrupt flag */
#define SVMLIFG_L           (0x0002u)  /* SVM low side interrupt flag */
#define SVMLVLRIFG_L        (0x0004u)  /* SVM low side Voltage Level Reached interrupt flag */
#define SVSMHDLYIFG_L       (0x0010u)  /* SVS and SVM high side Delay expired interrupt flag */
#define SVMHIFG_L           (0x0020u)  /* SVM high side interrupt flag */
#define SVMHVLRIFG_L        (0x0040u)  /* SVM high side Voltage Level Reached interrupt flag */

/* PMMIFG Control Bits */
#define PMMBORIFG_H         (0x0001u)  /* PMM Software BOR interrupt flag */
#define PMMRSTIFG_H         (0x0002u)  /* PMM RESET pin interrupt flag */
#define PMMPORIFG_H         (0x0004u)  /* PMM Software POR interrupt flag */
#define SVSHIFG_H           (0x0010u)  /* SVS low side interrupt flag */
#define SVSLIFG_H           (0x0020u)  /* SVS high side interrupt flag */
#define PMMLPM5IFG_H        (0x0080u)  /* LPM5 indication Flag */

#define PMMRSTLPM5IFG       PMMLPM5IFG /* LPM5 indication Flag */

/* PMMIE and RESET Control Bits */
#define SVSMLDLYIE_L        (0x0001u)  /* SVS and SVM low side Delay expired interrupt enable */
#define SVMLIE_L            (0x0002u)  /* SVM low side interrupt enable */
#define SVMLVLRIE_L         (0x0004u)  /* SVM low side Voltage Level Reached interrupt enable */
#define SVSMHDLYIE_L        (0x0010u)  /* SVS and SVM high side Delay expired interrupt enable */
#define SVMHIE_L            (0x0020u)  /* SVM high side interrupt enable */
#define SVMHVLRIE_L         (0x0040u)  /* SVM high side Voltage Level Reached interrupt enable */

/* PMMIE and RESET Control Bits */
#define SVSLPE_H            (0x0001u)  /* SVS low side POR enable */
#define SVMLVLRPE_H         (0x0002u)  /* SVM low side Voltage Level reached POR enable */
#define SVSHPE_H            (0x0010u)  /* SVS high side POR enable */
#define SVMHVLRPE_H         (0x0020u)  /* SVM high side Voltage Level reached POR enable */

/* PM5CTL0 Power Mode 5 Control Bits */
#define LOCKLPM5_L          (0x0001u)  /* Lock I/O pin configuration upon entry/exit to/from LPM5 */

/* PM5CTL0 Power Mode 5 Control Bits */
#define LOCKIO              LOCKLPM5  /* Lock I/O pin configuration upon entry/exit to/from LPM5 */

/*-------------------------------------------------------------------------
 *   RC  RAM Control Module
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short RCCTL0;   /* Ram Controller Control Register */

  struct
  {
    unsigned short RCRS0OFF        : 1; /* RAM Controller RAM Sector 0 Off */
    unsigned short RCRS1OFF        : 1; /* RAM Controller RAM Sector 1 Off */
    unsigned short RCRS2OFF        : 1; /* RAM Controller RAM Sector 2 Off */
    unsigned short RCRS3OFF        : 1; /* RAM Controller RAM Sector 3 Off */
  }RCCTL0_bit;
} @ 0x0158;


enum {
  RCRS0OFF        = 0x0001,
  RCRS1OFF        = 0x0002,
  RCRS2OFF        = 0x0004,
  RCRS3OFF        = 0x0008
};


#define __MSP430_HAS_RC__             /* Definition to show that Module is available */


/* RCCTL0 Control Bits */
#define RCRS0OFF_L          (0x0001u)  /* RAM Controller RAM Sector 0 Off */
#define RCRS1OFF_L          (0x0002u)  /* RAM Controller RAM Sector 1 Off */
#define RCRS2OFF_L          (0x0004u)  /* RAM Controller RAM Sector 2 Off */
#define RCRS3OFF_L          (0x0008u)  /* RAM Controller RAM Sector 3 Off */

#define RCKEY               (0x5A00u)

/*-------------------------------------------------------------------------
 *   Shared Reference
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short REFCTL0;   /* REF Shared Reference control register 0 */

  struct
  {
    unsigned short REFON           : 1; /* REF Reference On */
    unsigned short REFOUT          : 1; /* REF Reference output Buffer On */
    unsigned short                : 1;
    unsigned short REFTCOFF        : 1; /* REF Temp.Sensor off */
    unsigned short REFVSEL0        : 1; /* REF Reference Voltage Level Select Bit:0 */
    unsigned short REFVSEL1        : 1; /* REF Reference Voltage Level Select Bit:1 */
    unsigned short                : 1;
    unsigned short REFMSTR         : 1; /* REF Master Control */
    unsigned short REFGENACT       : 1; /* REF Reference generator active */
    unsigned short REFBGACT        : 1; /* REF Reference bandgap active */
    unsigned short REFGENBUSY      : 1; /* REF Reference generator busy */
    unsigned short BGMODE          : 1; /* REF Bandgap mode */
  }REFCTL0_bit;
} @ 0x01B0;


enum {
  REFON           = 0x0001,
  REFOUT          = 0x0002,
  REFTCOFF        = 0x0008,
  REFVSEL0        = 0x0010,
  REFVSEL1        = 0x0020,
  REFMSTR         = 0x0080,
  REFGENACT       = 0x0100,
  REFBGACT        = 0x0200,
  REFGENBUSY      = 0x0400,
  BGMODE          = 0x0800
};


#define __MSP430_HAS_REF__          /* Definition to show that Module is available */


/* REFCTL0 Control Bits */
#define REFON_L             (0x0001u)  /* REF Reference On */
#define REFOUT_L            (0x0002u)  /* REF Reference output Buffer On */
#define REFTCOFF_L          (0x0008u)  /* REF Temp.Sensor off */
#define REFVSEL0_L          (0x0010u)  /* REF Reference Voltage Level Select Bit:0 */
#define REFVSEL1_L          (0x0020u)  /* REF Reference Voltage Level Select Bit:1 */
#define REFMSTR_L           (0x0080u)  /* REF Master Control */
#define REFGENACT_H         (0x0001u)  /* REF Reference generator active */
#define REFBGACT_H          (0x0002u)  /* REF Reference bandgap active */
#define REFGENBUSY_H        (0x0004u)  /* REF Reference generator busy */
#define BGMODE_H            (0x0008u)  /* REF Bandgap mode */

#define REFVSEL_0           (0x0000u)  /* REF Reference Voltage Level Select 1.5V */
#define REFVSEL_1           (0x0010u)  /* REF Reference Voltage Level Select 2.0V */
#define REFVSEL_2           (0x0020u)  /* REF Reference Voltage Level Select 2.5V */
#define REFVSEL_3           (0x0030u)  /* REF Reference Voltage Level Select 2.5V */

/*-------------------------------------------------------------------------
 *   SFR  Special Function Registers
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short SFRIE1;   /* Interrupt Enable 1 */

  struct
  {
    unsigned short WDTIE           : 1; /* WDT Interrupt Enable */
    unsigned short OFIE            : 1; /* Osc Fault Enable */
    unsigned short                : 1;
    unsigned short VMAIE           : 1; /* Vacant Memory Interrupt Enable */
    unsigned short NMIIE           : 1; /* NMI Interrupt Enable */
    unsigned short ACCVIE          : 1; /* Flash Access Violation Interrupt Enable */
    unsigned short JMBINIE         : 1; /* JTAG Mail Box input Interrupt Enable */
    unsigned short JMBOUTIE        : 1; /* JTAG Mail Box output Interrupt Enable */
  }SFRIE1_bit;
} @0x0100;


enum {
  WDTIE           = 0x0001,
  OFIE            = 0x0002,
  VMAIE           = 0x0008,
  NMIIE           = 0x0010,
  ACCVIE          = 0x0020,
  JMBINIE         = 0x0040,
  JMBOUTIE        = 0x0080
};


__no_init volatile union
{
  unsigned short SFRIFG1;   /* Interrupt Flag 1 */

  struct
  {
    unsigned short WDTIFG          : 1; /* WDT Interrupt Flag */
    unsigned short OFIFG           : 1; /* Osc Fault Flag */
    unsigned short                : 1;
    unsigned short VMAIFG          : 1; /* Vacant Memory Interrupt Flag */
    unsigned short NMIIFG          : 1; /* NMI Interrupt Flag */
    unsigned short                : 1;
    unsigned short JMBINIFG        : 1; /* JTAG Mail Box input Interrupt Flag */
    unsigned short JMBOUTIFG       : 1; /* JTAG Mail Box output Interrupt Flag */
  }SFRIFG1_bit;
} @0x0102;


enum {
  WDTIFG          = 0x0001,
  OFIFG           = 0x0002,
  VMAIFG          = 0x0008,
  NMIIFG          = 0x0010,
  JMBINIFG        = 0x0040,
  JMBOUTIFG       = 0x0080
};


__no_init volatile union
{
  unsigned short SFRRPCR;   /* RESET Pin Control Register */

  struct
  {
    unsigned short SYSNMI          : 1; /* NMI select */
    unsigned short SYSNMIIES       : 1; /* NMI edge select */
    unsigned short SYSRSTUP        : 1; /* RESET Pin pull down/up select */
    unsigned short SYSRSTRE        : 1; /* RESET Pin Resistor enable */
  }SFRRPCR_bit;
} @ 0x0104;


enum {
  SYSNMI          = 0x0001,
  SYSNMIIES       = 0x0002,
  SYSRSTUP        = 0x0004,
  SYSRSTRE        = 0x0008
};


#define __MSP430_HAS_SFR__            /* Definition to show that Module is available */


#define WDTIE_L             (0x0001u)  /* WDT Interrupt Enable */
#define OFIE_L              (0x0002u)  /* Osc Fault Enable */
#define VMAIE_L             (0x0008u)  /* Vacant Memory Interrupt Enable */
#define NMIIE_L             (0x0010u)  /* NMI Interrupt Enable */
#define ACCVIE_L            (0x0020u)  /* Flash Access Violation Interrupt Enable */
#define JMBINIE_L           (0x0040u)  /* JTAG Mail Box input Interrupt Enable */
#define JMBOUTIE_L          (0x0080u)  /* JTAG Mail Box output Interrupt Enable */

#define WDTIFG_L            (0x0001u)  /* WDT Interrupt Flag */
#define OFIFG_L             (0x0002u)  /* Osc Fault Flag */
#define VMAIFG_L            (0x0008u)  /* Vacant Memory Interrupt Flag */
#define NMIIFG_L            (0x0010u)  /* NMI Interrupt Flag */
#define JMBINIFG_L          (0x0040u)  /* JTAG Mail Box input Interrupt Flag */
#define JMBOUTIFG_L         (0x0080u)  /* JTAG Mail Box output Interrupt Flag */

#define SYSNMI_L            (0x0001u)  /* NMI select */
#define SYSNMIIES_L         (0x0002u)  /* NMI edge select */
#define SYSRSTUP_L          (0x0004u)  /* RESET Pin pull down/up select */
#define SYSRSTRE_L          (0x0008u)  /* RESET Pin Resistor enable */

/*-------------------------------------------------------------------------
 *   SYS  System Module
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short SYSCTL;   /* System control */

  struct
  {
    unsigned short SYSRIVECT       : 1; /* SYS - RAM based interrupt vectors */
    unsigned short                : 1;
    unsigned short SYSPMMPE        : 1; /* SYS - PMM access protect */
    unsigned short                : 1;
    unsigned short SYSBSLIND       : 1; /* SYS - TCK/RST indication detected */
    unsigned short SYSJTAGPIN      : 1; /* SYS - Dedicated JTAG pins enabled */
  }SYSCTL_bit;
} @0x0180;


enum {
  SYSRIVECT       = 0x0001,
  SYSPMMPE        = 0x0004,
  SYSBSLIND       = 0x0010,
  SYSJTAGPIN      = 0x0020
};


__no_init volatile union
{
  unsigned short SYSBSLC;   /* Boot strap configuration area */

  struct
  {
    unsigned short SYSBSLSIZE0     : 1; /* SYS - BSL Protection Size 0 */
    unsigned short SYSBSLSIZE1     : 1; /* SYS - BSL Protection Size 1 */
    unsigned short SYSBSLR         : 1; /* SYS - RAM assigned to BSL */
    unsigned short                : 11;
    unsigned short SYSBSLOFF       : 1; /* SYS - BSL Memeory disabled */
    unsigned short SYSBSLPE        : 1; /* SYS - BSL Memory protection enabled */
  }SYSBSLC_bit;
} @0x0182;


enum {
  SYSBSLSIZE0     = 0x0001,
  SYSBSLSIZE1     = 0x0002,
  SYSBSLR         = 0x0004,
  SYSBSLOFF       = 0x4000,
  SYSBSLPE        = 0x8000
};


__no_init volatile union
{
  unsigned short SYSJMBC;   /* JTAG mailbox control */

  struct
  {
    unsigned short JMBIN0FG        : 1; /* SYS - Incoming JTAG Mailbox 0 Flag */
    unsigned short JMBIN1FG        : 1; /* SYS - Incoming JTAG Mailbox 1 Flag */
    unsigned short JMBOUT0FG       : 1; /* SYS - Outgoing JTAG Mailbox 0 Flag */
    unsigned short JMBOUT1FG       : 1; /* SYS - Outgoing JTAG Mailbox 1 Flag */
    unsigned short JMBMODE         : 1; /* SYS - JMB 16/32 Bit Mode */
    unsigned short                : 1;
    unsigned short JMBCLR0OFF      : 1; /* SYS - Incoming JTAG Mailbox 0 Flag auto-clear disalbe */
    unsigned short JMBCLR1OFF      : 1; /* SYS - Incoming JTAG Mailbox 1 Flag auto-clear disalbe */
  }SYSJMBC_bit;
} @0x0186;


enum {
  JMBIN0FG        = 0x0001,
  JMBIN1FG        = 0x0002,
  JMBOUT0FG       = 0x0004,
  JMBOUT1FG       = 0x0008,
  JMBMODE         = 0x0010,
  JMBCLR0OFF      = 0x0040,
  JMBCLR1OFF      = 0x0080
};


  /* JTAG mailbox input 0 */
__no_init volatile unsigned short SYSJMBI0 @ 0x0188;


  /* JTAG mailbox input 1 */
__no_init volatile unsigned short SYSJMBI1 @ 0x018A;


  /* JTAG mailbox output 0 */
__no_init volatile unsigned short SYSJMBO0 @ 0x018C;


  /* JTAG mailbox output 1 */
__no_init volatile unsigned short SYSJMBO1 @ 0x018E;


  /* Bus Error vector generator */
__no_init volatile unsigned short SYSBERRIV @ 0x0198;


  /* User NMI vector generator */
__no_init volatile unsigned short SYSUNIV @ 0x019A;


  /* System NMI vector generator */
__no_init volatile unsigned short SYSSNIV @ 0x019C;


  /* Reset vector generator */
__no_init volatile unsigned short SYSRSTIV @ 0x019E;


#define __MSP430_HAS_SYS__            /* Definition to show that Module is available */


/* SYSCTL Control Bits */
#define SYSRIVECT_L         (0x0001u)  /* SYS - RAM based interrupt vectors */
#define SYSPMMPE_L          (0x0004u)  /* SYS - PMM access protect */
#define SYSBSLIND_L         (0x0010u)  /* SYS - TCK/RST indication detected */
#define SYSJTAGPIN_L        (0x0020u)  /* SYS - Dedicated JTAG pins enabled */

/* SYSBSLC Control Bits */
#define SYSBSLSIZE0_L       (0x0001u)  /* SYS - BSL Protection Size 0 */
#define SYSBSLSIZE1_L       (0x0002u)  /* SYS - BSL Protection Size 1 */
#define SYSBSLR_L           (0x0004u)  /* SYS - RAM assigned to BSL */
#define SYSBSLOFF_H         (0x0040u)  /* SYS - BSL Memeory disabled */
#define SYSBSLPE_H          (0x0080u)  /* SYS - BSL Memory protection enabled */

/* SYSJMBC Control Bits */
#define JMBIN0FG_L          (0x0001u)  /* SYS - Incoming JTAG Mailbox 0 Flag */
#define JMBIN1FG_L          (0x0002u)  /* SYS - Incoming JTAG Mailbox 1 Flag */
#define JMBOUT0FG_L         (0x0004u)  /* SYS - Outgoing JTAG Mailbox 0 Flag */
#define JMBOUT1FG_L         (0x0008u)  /* SYS - Outgoing JTAG Mailbox 1 Flag */
#define JMBMODE_L           (0x0010u)  /* SYS - JMB 16/32 Bit Mode */
#define JMBCLR0OFF_L        (0x0040u)  /* SYS - Incoming JTAG Mailbox 0 Flag auto-clear disalbe */
#define JMBCLR1OFF_L        (0x0080u)  /* SYS - Incoming JTAG Mailbox 1 Flag auto-clear disalbe */

/* SYSUNIV Definitions */
#define SYSUNIV_NONE       (0x0000u)    /* No Interrupt pending */
#define SYSUNIV_NMIIFG     (0x0002u)    /* SYSUNIV : NMIIFG */
#define SYSUNIV_OFIFG      (0x0004u)    /* SYSUNIV : Osc. Fail - OFIFG */
#define SYSUNIV_ACCVIFG    (0x0006u)    /* SYSUNIV : Access Violation - ACCVIFG */
#define SYSUNIV_SYSBERRIV  (0x0008u)    /* SYSUNIV : Bus Error - SYSBERRIV */

/* SYSSNIV Definitions */
#define SYSSNIV_NONE       (0x0000u)    /* No Interrupt pending */
#define SYSSNIV_SVMLIFG    (0x0002u)    /* SYSSNIV : SVMLIFG */
#define SYSSNIV_SVMHIFG    (0x0004u)    /* SYSSNIV : SVMHIFG */
#define SYSSNIV_DLYLIFG    (0x0006u)    /* SYSSNIV : DLYLIFG */
#define SYSSNIV_DLYHIFG    (0x0008u)    /* SYSSNIV : DLYHIFG */
#define SYSSNIV_VMAIFG     (0x000Au)    /* SYSSNIV : VMAIFG */
#define SYSSNIV_JMBINIFG   (0x000Cu)    /* SYSSNIV : JMBINIFG */
#define SYSSNIV_JMBOUTIFG  (0x000Eu)    /* SYSSNIV : JMBOUTIFG */
#define SYSSNIV_VLRLIFG    (0x0010u)    /* SYSSNIV : VLRLIFG */
#define SYSSNIV_VLRHIFG    (0x0012u)    /* SYSSNIV : VLRHIFG */

/* SYSRSTIV Definitions */
#define SYSRSTIV_NONE      (0x0000u)    /* No Interrupt pending */
#define SYSRSTIV_BOR       (0x0002u)    /* SYSRSTIV : BOR */
#define SYSRSTIV_RSTNMI    (0x0004u)    /* SYSRSTIV : RST/NMI */
#define SYSRSTIV_DOBOR     (0x0006u)    /* SYSRSTIV : Do BOR */
#define SYSRSTIV_LPM5WU    (0x0008u)    /* SYSRSTIV : Port LPM5 Wake Up */
#define SYSRSTIV_SECYV     (0x000Au)    /* SYSRSTIV : Security violation */
#define SYSRSTIV_SVSL      (0x000Cu)    /* SYSRSTIV : SVSL */
#define SYSRSTIV_SVSH      (0x000Eu)    /* SYSRSTIV : SVSH */
#define SYSRSTIV_SVML_OVP  (0x0010u)    /* SYSRSTIV : SVML_OVP */
#define SYSRSTIV_SVMH_OVP  (0x0012u)    /* SYSRSTIV : SVMH_OVP */
#define SYSRSTIV_DOPOR     (0x0014u)    /* SYSRSTIV : Do POR */
#define SYSRSTIV_WDTTO     (0x0016u)    /* SYSRSTIV : WDT Time out */
#define SYSRSTIV_WDTKEY    (0x0018u)    /* SYSRSTIV : WDTKEY violation */
#define SYSRSTIV_KEYV      (0x001Au)    /* SYSRSTIV : Flash Key violation */
#define SYSRSTIV_PLLUL     (0x001Cu)    /* SYSRSTIV : PLL unlock */
#define SYSRSTIV_PERF      (0x001Eu)    /* SYSRSTIV : peripheral/config area fetch */
#define SYSRSTIV_PMMKEY    (0x0020u)    /* SYSRSTIV : PMMKEY violation */

/*-------------------------------------------------------------------------
 *   Timer0_A3
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short TA0CTL;   /* Timer0_A3 Control */

  struct
  {
    unsigned short TAIFG           : 1; /* Timer A counter interrupt flag */
    unsigned short TAIE            : 1; /* Timer A counter interrupt enable */
    unsigned short TACLR           : 1; /* Timer A counter clear */
    unsigned short                : 1;
    unsigned short MC0             : 1; /* Timer A mode control 0 */
    unsigned short MC1             : 1; /* Timer A mode control 1 */
    unsigned short ID0             : 1; /* Timer A clock input divider 0 */
    unsigned short ID1             : 1; /* Timer A clock input divider 1 */
    unsigned short TASSEL0         : 1; /* Timer A clock source select 1 */
    unsigned short TASSEL1         : 1; /* Timer A clock source select 0 */
  }TA0CTL_bit;
} @0x03C0;


enum {
  TAIFG           = 0x0001,
  TAIE            = 0x0002,
  TACLR           = 0x0004,
  MC0             = 0x0010,
  MC1             = 0x0020,
  ID0             = 0x0040,
  ID1             = 0x0080,
  TASSEL0         = 0x0100,
  TASSEL1         = 0x0200
};


__no_init volatile union
{
  unsigned short TA0CCTL0;   /* Timer0_A3 Capture/Compare Control 0 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short                : 1;
    unsigned short SCCI            : 1; /* Latched capture signal (read) */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TA0CCTL0_bit;
} @0x03C2;


enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  SCCI            = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000
};


__no_init volatile union
{
  unsigned short TA0CCTL1;   /* Timer0_A3 Capture/Compare Control 1 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short                : 1;
    unsigned short SCCI            : 1; /* Latched capture signal (read) */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TA0CCTL1_bit;
} @0x03C4;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  SCCI            = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short TA0CCTL2;   /* Timer0_A3 Capture/Compare Control 2 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short                : 1;
    unsigned short SCCI            : 1; /* Latched capture signal (read) */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TA0CCTL2_bit;
} @0x03C6;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  SCCI            = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer0_A3 */
__no_init volatile unsigned short TA0R @ 0x03D0;


  /* Timer0_A3 Capture/Compare 0 */
__no_init volatile unsigned short TA0CCR0 @ 0x03D2;


  /* Timer0_A3 Capture/Compare 1 */
__no_init volatile unsigned short TA0CCR1 @ 0x03D4;


  /* Timer0_A3 Capture/Compare 2 */
__no_init volatile unsigned short TA0CCR2 @ 0x03D6;


  /* Timer0_A3 Interrupt Vector Word */
__no_init volatile unsigned short TA0IV @ 0x03EE;


__no_init volatile union
{
  unsigned short TA0EX0;   /* Timer0_A3 Expansion Register 0 */

  struct
  {
    unsigned short TAIDEX0         : 1; /* Timer A Input divider expansion Bit: 0 */
    unsigned short TAIDEX1         : 1; /* Timer A Input divider expansion Bit: 1 */
    unsigned short TAIDEX2         : 1; /* Timer A Input divider expansion Bit: 2 */
  }TA0EX0_bit;
} @ 0x03E0;


enum {
  TAIDEX0         = 0x0001,
  TAIDEX1         = 0x0002,
  TAIDEX2         = 0x0004
};


#define __MSP430_HAS_T0A3__           /* Definition to show that Module is available */


#define MC_0                (0*0x10u)  /* Timer A mode control: 0 - Stop */
#define MC_1                (1*0x10u)  /* Timer A mode control: 1 - Up to CCR0 */
#define MC_2                (2*0x10u)  /* Timer A mode control: 2 - Continous up */
#define MC_3                (3*0x10u)  /* Timer A mode control: 3 - Up/Down */
#define ID_0                (0*0x40u)  /* Timer A input divider: 0 - /1 */
#define ID_1                (1*0x40u)  /* Timer A input divider: 1 - /2 */
#define ID_2                (2*0x40u)  /* Timer A input divider: 2 - /4 */
#define ID_3                (3*0x40u)  /* Timer A input divider: 3 - /8 */
#define TASSEL_0            (0*0x100u) /* Timer A clock source select: 0 - TACLK */
#define TASSEL_1            (1*0x100u) /* Timer A clock source select: 1 - ACLK  */
#define TASSEL_2            (2*0x100u) /* Timer A clock source select: 2 - SMCLK */
#define TASSEL_3            (3*0x100u) /* Timer A clock source select: 3 - INCLK */
#define MC__STOP            (0*0x10u)  /* Timer A mode control: 0 - Stop */
#define MC__UP              (1*0x10u)  /* Timer A mode control: 1 - Up to CCR0 */
#define MC__CONTINOUS       (2*0x10u)  /* Timer A mode control: 2 - Continous up */
#define MC__UPDOWN          (3*0x10u)  /* Timer A mode control: 3 - Up/Down */
#define ID__1               (0*0x40u)  /* Timer A input divider: 0 - /1 */
#define ID__2               (1*0x40u)  /* Timer A input divider: 1 - /2 */
#define ID__4               (2*0x40u)  /* Timer A input divider: 2 - /4 */
#define ID__8               (3*0x40u)  /* Timer A input divider: 3 - /8 */
#define TASSEL__TACLK       (0*0x100u) /* Timer A clock source select: 0 - TACLK */
#define TASSEL__ACLK        (1*0x100u) /* Timer A clock source select: 1 - ACLK  */
#define TASSEL__SMCLK       (2*0x100u) /* Timer A clock source select: 2 - SMCLK */
#define TASSEL__INCLK       (3*0x100u) /* Timer A clock source select: 3 - INCLK */

#define OUTMOD_0            (0*0x20u)  /* PWM output mode: 0 - output only */
#define OUTMOD_1            (1*0x20u)  /* PWM output mode: 1 - set */
#define OUTMOD_2            (2*0x20u)  /* PWM output mode: 2 - PWM toggle/reset */
#define OUTMOD_3            (3*0x20u)  /* PWM output mode: 3 - PWM set/reset */
#define OUTMOD_4            (4*0x20u)  /* PWM output mode: 4 - toggle */
#define OUTMOD_5            (5*0x20u)  /* PWM output mode: 5 - Reset */
#define OUTMOD_6            (6*0x20u)  /* PWM output mode: 6 - PWM toggle/set */
#define OUTMOD_7            (7*0x20u)  /* PWM output mode: 7 - PWM reset/set */
#define CCIS_0              (0*0x1000u) /* Capture input select: 0 - CCIxA */
#define CCIS_1              (1*0x1000u) /* Capture input select: 1 - CCIxB */
#define CCIS_2              (2*0x1000u) /* Capture input select: 2 - GND */
#define CCIS_3              (3*0x1000u) /* Capture input select: 3 - Vcc */
#define CM_0                (0*0x4000u) /* Capture mode: 0 - disabled */
#define CM_1                (1*0x4000u) /* Capture mode: 1 - pos. edge */
#define CM_2                (2*0x4000u) /* Capture mode: 1 - neg. edge */
#define CM_3                (3*0x4000u) /* Capture mode: 1 - both edges */

#define TAIDEX_0            (0*0x0001u) /* Timer A Input divider expansion : /1 */
#define TAIDEX_1            (1*0x0001u) /* Timer A Input divider expansion : /2 */
#define TAIDEX_2            (2*0x0001u) /* Timer A Input divider expansion : /3 */
#define TAIDEX_3            (3*0x0001u) /* Timer A Input divider expansion : /4 */
#define TAIDEX_4            (4*0x0001u) /* Timer A Input divider expansion : /5 */
#define TAIDEX_5            (5*0x0001u) /* Timer A Input divider expansion : /6 */
#define TAIDEX_6            (6*0x0001u) /* Timer A Input divider expansion : /7 */
#define TAIDEX_7            (7*0x0001u) /* Timer A Input divider expansion : /8 */

/* T0A3IV Definitions */
#define TA0IV_NONE          (0x0000u)    /* No Interrupt pending */
#define TA0IV_TA0CCR1       (0x0002u)    /* TA0CCR1_CCIFG */
#define TA0IV_TA0CCR2       (0x0004u)    /* TA0CCR2_CCIFG */
#define TA0IV_3             (0x0006u)    /* Reserved */
#define TA0IV_4             (0x0008u)    /* Reserved */
#define TA0IV_5             (0x000Au)    /* Reserved */
#define TA0IV_6             (0x000Cu)    /* Reserved */
#define TA0IV_TA0IFG        (0x000Eu)    /* TA0IFG */

/*-------------------------------------------------------------------------
 *   Timer0_D3
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short TD0CTL0;   /* Timer0_D3 Control 0 */

  struct
  {
    unsigned short TDIFG           : 1; /* Timer0_D3 interrupt flag */
    unsigned short TDIE            : 1; /* Timer0_D3 interrupt enable */
    unsigned short TDCLR           : 1; /* Timer0_D3 counter clear */
    unsigned short                : 1;
    unsigned short MC0             : 1; /* Timer A mode control 0 */
    unsigned short MC1             : 1; /* Timer A mode control 1 */
    unsigned short ID0             : 1; /* Timer A clock input divider 0 */
    unsigned short ID1             : 1; /* Timer A clock input divider 1 */
    unsigned short TDSSEL0         : 1; /* Clock source 0 */
    unsigned short TDSSEL1         : 1; /* Clock source 1 */
    unsigned short                : 1;
    unsigned short CNTL0           : 1; /* Counter lenght 0 */
    unsigned short CNTL1           : 1; /* Counter lenght 1 */
    unsigned short TDCLGRP0        : 1; /* Timer0_D3 Compare latch load group 0 */
    unsigned short TDCLGRP1        : 1; /* Timer0_D3 Compare latch load group 1 */
  }TD0CTL0_bit;
} @0x0B00;


enum {
  TDIFG           = 0x0001,
  TDIE            = 0x0002,
  TDCLR           = 0x0004,
/*  MC0             = 0x0010, */
/*  MC1             = 0x0020, */
/*  ID0             = 0x0040, */
/*  ID1             = 0x0080, */
  TDSSEL0         = 0x0100,
  TDSSEL1         = 0x0200,
  CNTL0           = 0x0800,
  CNTL1           = 0x1000,
  TDCLGRP0        = 0x2000,
  TDCLGRP1        = 0x4000
};


__no_init volatile union
{
  unsigned short TD0CTL1;   /* Timer0_D3 Control 1 */

  struct
  {
    unsigned short TDCLKM0         : 1; /* Timer0_D3 Clocking Mode Bit: 0 */
    unsigned short TDCLKM1         : 1; /* Timer0_D3 Clocking Mode Bit: 1 */
    unsigned short                : 2;
    unsigned short TD2CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD2 */
    unsigned short TD4CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD4 */
    unsigned short TD6CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD6 */
    unsigned short                : 1;
    unsigned short TDIDEX0         : 1; /* Timer0_D3 Input divider expansion Bit: 0 */
    unsigned short TDIDEX1         : 1; /* Timer0_D3 Input divider expansion Bit: 1 */
    unsigned short TDIDEX2         : 1; /* Timer0_D3 Input divider expansion Bit: 2 */
  }TD0CTL1_bit;
} @0x0B02;


enum {
  TDCLKM0         = 0x0001,
  TDCLKM1         = 0x0002,
  TD2CMB          = 0x0010,
  TD4CMB          = 0x0020,
  TD6CMB          = 0x0040,
  TDIDEX0         = 0x0100,
  TDIDEX1         = 0x0200,
  TDIDEX2         = 0x0400
};


__no_init volatile union
{
  unsigned short TD0CTL2;   /* Timer0_D3 Control 2 */

  struct
  {
    unsigned short TDCAPM0         : 1; /* Timer0_D3 Capture Mode of Channel 0 */
    unsigned short TDCAPM1         : 1; /* Timer0_D3 Capture Mode of Channel 1 */
    unsigned short TDCAPM2         : 1; /* Timer0_D3 Capture Mode of Channel 2 */
    unsigned short TDCAPM3         : 1; /* Timer0_D3 Capture Mode of Channel 3 */
    unsigned short TDCAPM4         : 1; /* Timer0_D3 Capture Mode of Channel 4 */
    unsigned short TDCAPM5         : 1; /* Timer0_D3 Capture Mode of Channel 5 */
    unsigned short TDCAPM6         : 1; /* Timer0_D3 Capture Mode of Channel 6 */
  }TD0CTL2_bit;
} @0x0B04;


enum {
  TDCAPM0         = 0x0001,
  TDCAPM1         = 0x0002,
  TDCAPM2         = 0x0004,
  TDCAPM3         = 0x0008,
  TDCAPM4         = 0x0010,
  TDCAPM5         = 0x0020,
  TDCAPM6         = 0x0040
};


  /* Timer0_D3 Counter */
__no_init volatile unsigned short TD0R @ 0x0B06;


__no_init volatile union
{
  unsigned short TD0CCTL0;   /* Timer0_D3 Capture/Compare Control 0 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD0CCTL0_bit;
} @0x0B08;


enum {
/*  CCIFG           = 0x0001, */
/*  COV             = 0x0002, */
/*  OUT             = 0x0004, */
/*  CCI             = 0x0008, */
/*  CCIE            = 0x0010, */
/*  OUTMOD0         = 0x0020, */
/*  OUTMOD1         = 0x0040, */
/*  OUTMOD2         = 0x0080, */
/*  CAP             = 0x0100, */
  CLLD0           = 0x0200,
  CLLD1           = 0x0400
/*  SCS             = 0x0800, */
/*  CCIS0           = 0x1000, */
/*  CCIS1           = 0x2000, */
/*  CM0             = 0x4000, */
/*  CM1             = 0x8000, */
};


  /* Timer0_D3 Capture/Compare 0 */
__no_init volatile unsigned short TD0CCR0 @ 0x0B0A;


  /* Timer0_D3 Capture/Compare Latch 0 */
__no_init volatile unsigned short TD0CL0 @ 0x0B0C;


__no_init volatile union
{
  unsigned short TD0CCTL1;   /* Timer0_D3 Capture/Compare Control 1 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD0CCTL1_bit;
} @0x0B0E;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  CLLD0           = 0x0200,
  CLLD1           = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer0_D3 Capture/Compare 1 */
__no_init volatile unsigned short TD0CCR1 @ 0x0B10;


  /* Timer0_D3 Capture/Compare Latch 1 */
__no_init volatile unsigned short TD0CL1 @ 0x0B12;


__no_init volatile union
{
  unsigned short TD0CCTL2;   /* Timer0_D3 Capture/Compare Control 2 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD0CCTL2_bit;
} @0x0B14;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  CLLD0           = 0x0200,
  CLLD1           = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer0_D3 Capture/Compare 2 */
__no_init volatile unsigned short TD0CCR2 @ 0x0B16;


  /* Timer0_D3 Capture/Compare Latch 2 */
__no_init volatile unsigned short TD0CL2 @ 0x0B18;


__no_init volatile union
{
  unsigned short TD0HCTL0;   /* Timer0_D3 High-resolution Control Register 0 */

  struct
  {
    unsigned short TDHEN           : 1; /* Timer0_D3 High-Resolution Enable */
    unsigned short TDHREGEN        : 1; /* Timer0_D3 High-Resolution Regulated Mode */
    unsigned short TDHEAEN         : 1; /* Timer0_D3 High-Resolution clock error accum. enable */
    unsigned short TDHRON          : 1; /* Timer0_D3 High-Resolution Generator forced o */
    unsigned short TDHM0           : 1; /* Timer0_D3 High-Resoltuion Clock Mult. Bit: 0 */
    unsigned short TDHM1           : 1; /* Timer0_D3 High-Resoltuion Clock Mult. Bit: 1 */
    unsigned short TDHD0           : 1; /* Timer0_D3 High-Resolution clock divider Bit: 0 */
    unsigned short TDHD1           : 1; /* Timer0_D3 High-Resolution clock divider Bit: 1 */
    unsigned short TDHFW           : 1; /* Timer0_D7 High-resolution generator fast wakeup enable */
  }TD0HCTL0_bit;
} @0x0B38;


enum {
  TDHEN           = 0x0001,
  TDHREGEN        = 0x0002,
  TDHEAEN         = 0x0004,
  TDHRON          = 0x0008,
  TDHM0           = 0x0010,
  TDHM1           = 0x0020,
  TDHD0           = 0x0040,
  TDHD1           = 0x0080,
  TDHFW           = 0x0100
};


__no_init volatile union
{
  unsigned short TD0HCTL1;   /* Timer0_D3 High-resolution Control Register 1 */

  struct
  {
    unsigned short                : 1;
    unsigned short TDHCLKTRIM0     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 0 */
    unsigned short TDHCLKTRIM1     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 1 */
    unsigned short TDHCLKTRIM2     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 2 */
    unsigned short TDHCLKTRIM3     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 3 */
    unsigned short TDHCLKTRIM4     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 4 */
    unsigned short TDHCLKTRIM5     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 5 */
    unsigned short TDHCLKTRIM6     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 6 */
    unsigned short TDHCLKSR0       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 0 */
    unsigned short TDHCLKSR1       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 1 */
    unsigned short TDHCLKSR2       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 2 */
    unsigned short TDHCLKSR3       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 3 */
    unsigned short TDHCLKSR4       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 4 */
    unsigned short TDHCLKR0        : 1; /* Timer0_D3 High-Resolution Clock Range Bit: 0 */
    unsigned short TDHCLKR1        : 1; /* Timer0_D3 High-Resolution Clock Range Bit: 1 */
    unsigned short TDHCLKCR        : 1; /* Timer0_D3 High-Resolution Coarse Clock Range */
  }TD0HCTL1_bit;
} @0x0B3A;


enum {
  TDHCLKTRIM0     = 0x0002,
  TDHCLKTRIM1     = 0x0004,
  TDHCLKTRIM2     = 0x0008,
  TDHCLKTRIM3     = 0x0010,
  TDHCLKTRIM4     = 0x0020,
  TDHCLKTRIM5     = 0x0040,
  TDHCLKTRIM6     = 0x0080,
  TDHCLKSR0       = 0x0100,
  TDHCLKSR1       = 0x0200,
  TDHCLKSR2       = 0x0400,
  TDHCLKSR3       = 0x0800,
  TDHCLKSR4       = 0x1000,
  TDHCLKR0        = 0x2000,
  TDHCLKR1        = 0x4000,
  TDHCLKCR        = 0x8000
};


__no_init volatile union
{
  unsigned short TD0HINT;   /* Timer0_D3 High-resolution Interrupt Register */

  struct
  {
    unsigned short TDHFLIFG        : 1; /* Timer0_D3 High-Res. fail low Interrupt Flag */
    unsigned short TDHFHIFG        : 1; /* Timer0_D3 High-Res. fail high Interrupt Flag */
    unsigned short TDHLKIFG        : 1; /* Timer0_D3 High-Res. frequency lock Interrupt Flag */
    unsigned short TDHUNLKIFG      : 1; /* Timer0_D3 High-Res. frequency unlock Interrupt Flag */
    unsigned short                : 4;
    unsigned short TDHFLIE         : 1; /* Timer0_D3 High-Res. fail low Interrupt Enable */
    unsigned short TDHFHIE         : 1; /* Timer0_D3 High-Res. fail high Interrupt Enable */
    unsigned short TDHLKIE         : 1; /* Timer0_D3 High-Res. frequency lock Interrupt Enable */
    unsigned short TDHUNLKIE       : 1; /* Timer0_D3 High-Res. frequency unlock Interrupt Enable */
  }TD0HINT_bit;
} @0x0B3C;


enum {
  TDHFLIFG        = 0x0001,
  TDHFHIFG        = 0x0002,
  TDHLKIFG        = 0x0004,
  TDHUNLKIFG      = 0x0008,
  TDHFLIE         = 0x0100,
  TDHFHIE         = 0x0200,
  TDHLKIE         = 0x0400,
  TDHUNLKIE       = 0x0800
};


  /* Timer0_D3 Interrupt Vector Word */
__no_init volatile unsigned short TD0IV @ 0x0B3E;


#define __MSP430_HAS_T0D3__            /* Definition to show that Module is available */


#define SHR1                (0x4000u)  /* Timer0_D3 Compare latch load group 1 */
#define SHR0                (0x2000u)  /* Timer0_D3 Compare latch load group 0 */

#define TDSSEL_0            (0*0x0100u)  /* Clock Source: TDCLK */
#define TDSSEL_1            (1*0x0100u)  /* Clock Source: ACLK  */
#define TDSSEL_2            (2*0x0100u)  /* Clock Source: SMCLK */
#define TDSSEL_3            (3*0x0100u)  /* Clock Source: INCLK */
#define CNTL_0              (0*0x0800u)  /* Counter lenght: 16 bit */
#define CNTL_1              (1*0x0800u)  /* Counter lenght: 12 bit */
#define CNTL_2              (2*0x0800u)  /* Counter lenght: 10 bit */
#define CNTL_3              (3*0x0800u)  /* Counter lenght:  8 bit */
#define SHR_0               (0*0x2000u)  /* Timer0_D3 Group: 0 - individually */
#define SHR_1               (1*0x2000u)  /* Timer0_D3 Group: 1 - 3 groups (1-2, 3-4, 5-6) */
#define SHR_2               (2*0x2000u)  /* Timer0_D3 Group: 2 - 2 groups (1-3, 4-6)*/
#define SHR_3               (3*0x2000u)  /* Timer0_D3 Group: 3 - 1 group (all) */
#define TDCLGRP_0           (0*0x2000u)  /* Timer0_D3 Group: 0 - individually */
#define TDCLGRP_1           (1*0x2000u)  /* Timer0_D3 Group: 1 - 3 groups (1-2, 3-4, 5-6) */
#define TDCLGRP_2           (2*0x2000u)  /* Timer0_D3 Group: 2 - 2 groups (1-3, 4-6)*/
#define TDCLGRP_3           (3*0x2000u)  /* Timer0_D3 Group: 3 - 1 group (all) */
#define TDSSEL__TACLK       (0*0x0100u)  /* Timer0_D3 clock source select: 0 - TACLK */
#define TDSSEL__ACLK        (1*0x0100u)  /* Timer0_D3 clock source select: 1 - ACLK  */
#define TDSSEL__SMCLK       (2*0x0100u)  /* Timer0_D3 clock source select: 2 - SMCLK */
#define TDSSEL__INCLK       (3*0x0100u)  /* Timer0_D3 clock source select: 3 - INCLK */
#define CNTL__16            (0*0x0800u)  /* Counter lenght: 16 bit */
#define CNTL__12            (1*0x0800u)  /* Counter lenght: 12 bit */
#define CNTL__10            (2*0x0800u)  /* Counter lenght: 10 bit */
#define CNTL__8             (3*0x0800u)  /* Counter lenght:  8 bit */

#define TDCLKM_0            (0x0000u)   /* Timer0_D3 Clocking Mode: External */
#define TDCLKM_1            (0x0001u)   /* Timer0_D3 Clocking Mode: High-Res. local clock */
#define TDCLKM_2            (0x0002u)   /* Timer0_D3 Clocking Mode: Aux Clock */
#define TDCLKM__EXT         (0x0000u)   /* Timer0_D3 Clocking Mode: External */
#define TDCLKM__HIGHRES     (0x0001u)   /* Timer0_D3 Clocking Mode: High-Res. local clock */
#define TDCLKM__AUX         (0x0002u)   /* Timer0_D3 Clocking Mode: Aux Clock */

#define TDIDEX_0            (0*0x0100u) /* Timer0_D3 Input divider expansion : /1 */
#define TDIDEX_1            (1*0x0100u) /* Timer0_D3 Input divider expansion : /2 */
#define TDIDEX_2            (2*0x0100u) /* Timer0_D3 Input divider expansion : /3 */
#define TDIDEX_3            (3*0x0100u) /* Timer0_D3 Input divider expansion : /4 */
#define TDIDEX_4            (4*0x0100u) /* Timer0_D3 Input divider expansion : /5 */
#define TDIDEX_5            (5*0x0100u) /* Timer0_D3 Input divider expansion : /6 */
#define TDIDEX_6            (6*0x0100u) /* Timer0_D3 Input divider expansion : /7 */
#define TDIDEX_7            (7*0x0100u) /* Timer0_D3 Input divider expansion : /8 */
#define TDIDEX__1           (0*0x0100u) /* Timer0_D3 Input divider expansion : /1 */
#define TDIDEX__2           (1*0x0100u) /* Timer0_D3 Input divider expansion : /2 */
#define TDIDEX__3           (2*0x0100u) /* Timer0_D3 Input divider expansion : /3 */
#define TDIDEX__4           (3*0x0100u) /* Timer0_D3 Input divider expansion : /4 */
#define TDIDEX__5           (4*0x0100u) /* Timer0_D3 Input divider expansion : /5 */
#define TDIDEX__6           (5*0x0100u) /* Timer0_D3 Input divider expansion : /6 */
#define TDIDEX__7           (6*0x0100u) /* Timer0_D3 Input divider expansion : /7 */
#define TDIDEX__8           (7*0x0100u) /* Timer0_D3 Input divider expansion : /8 */

#define SLSHR1              (0x0400u)  /* Compare latch load source 1 */
#define SLSHR0              (0x0200u)  /* Compare latch load source 0 */

#define SLSHR_0             (0*0x0200u) /* Compare latch load sourec : 0 - immediate */
#define SLSHR_1             (1*0x0200u) /* Compare latch load sourec : 1 - TDR counts to 0 */
#define SLSHR_2             (2*0x0200u) /* Compare latch load sourec : 2 - up/down */
#define SLSHR_3             (3*0x0200u) /* Compare latch load sourec : 3 - TDR counts to TDCTL0 */

#define CLLD_0              (0*0x0200u) /* Compare latch load sourec : 0 - immediate */
#define CLLD_1              (1*0x0200u) /* Compare latch load sourec : 1 - TDR counts to 0 */
#define CLLD_2              (2*0x0200u) /* Compare latch load sourec : 2 - up/down */
#define CLLD_3              (3*0x0200u) /* Compare latch load sourec : 3 - TDR counts to TDCTL0 */

#define TDHCALEN            TDHREGEN   /* Timer0_D3 Lagacy Definition */

#define TDHM_0              (0x0000u)   /* Timer0_D3 High-Resoltuion Clock Mult.: 8x TimerD clock */
#define TDHM_1              (0x0010u)   /* Timer0_D3 High-Resoltuion Clock Mult.: 16x TimerD clock */
#define TDHM__8             (0x0000u)   /* Timer0_D3 High-Resoltuion Clock Mult.: 8x TimerD clock */
#define TDHM__16            (0x0010u)   /* Timer0_D3 High-Resoltuion Clock Mult.: 16x TimerD clock */
#define TDHD_0              (0x0000u)   /* Timer0_D3 High-Resolution clock divider: /1 */
#define TDHD_1              (0x0040u)   /* Timer0_D3 High-Resolution clock divider: /2 */
#define TDHD_2              (0x0080u)   /* Timer0_D3 High-Resolution clock divider: /4 */
#define TDHD_3              (0x00C0u)   /* Timer0_D3 High-Resolution clock divider: /8 */
#define TDHD__1             (0x0000u)   /* Timer0_D3 High-Resolution clock divider: /1 */
#define TDHD__2             (0x0040u)   /* Timer0_D3 High-Resolution clock divider: /2 */
#define TDHD__4             (0x0080u)   /* Timer0_D3 High-Resolution clock divider: /4 */
#define TDHD__8             (0x00C0u)   /* Timer0_D3 High-Resolution clock divider: /8 */

/* TD0IV Definitions */
#define TD0IV_NONE          (0x0000u)    /* No Interrupt pending */
#define TD0IV_TD0CCR1       (0x0002u)    /* TD0CCR1_CCIFG */
#define TD0IV_TD0CCR2       (0x0004u)    /* TD0CCR2_CCIFG */
#define TD0IV_TD0CCR3       (0x0006u)    /* TD0CCR3_CCIFG */
#define TD0IV_TD0CCR4       (0x0008u)    /* TD0CCR4_CCIFG */
#define TD0IV_TD0CCR5       (0x000Au)    /* TD0CCR5_CCIFG */
#define TD0IV_TD0CCR6       (0x000Cu)    /* TD0CCR6_CCIFG */
#define TD0IV_RES_14        (0x000Eu)    /* Reserverd */
#define TD0IV_TD0IFG        (0x0010u)    /* TD0IFG */
#define TD0IV_TDHFLIFG      (0x0012u)    /* TDHFLIFG Clock fail low */
#define TD0IV_TDHFHIFG      (0x0014u)    /* TDHFLIFG Clock fail high */
#define TD0IV_TDHLKIFG      (0x0016u)    /* TDHLKIE Clock lock*/
#define TD0IV_TDHUNLKIFG    (0x0018u)    /* TDHUNLKIE Clock unlock */

/*-------------------------------------------------------------------------
 *   Timer1_D3
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short TD1CTL0;   /* Timer1_D3 Control 0 */

  struct
  {
    unsigned short TDIFG           : 1; /* Timer0_D3 interrupt flag */
    unsigned short TDIE            : 1; /* Timer0_D3 interrupt enable */
    unsigned short TDCLR           : 1; /* Timer0_D3 counter clear */
    unsigned short                : 1;
    unsigned short MC0             : 1; /* Timer A mode control 0 */
    unsigned short MC1             : 1; /* Timer A mode control 1 */
    unsigned short ID0             : 1; /* Timer A clock input divider 0 */
    unsigned short ID1             : 1; /* Timer A clock input divider 1 */
    unsigned short TDSSEL0         : 1; /* Clock source 0 */
    unsigned short TDSSEL1         : 1; /* Clock source 1 */
    unsigned short                : 1;
    unsigned short CNTL0           : 1; /* Counter lenght 0 */
    unsigned short CNTL1           : 1; /* Counter lenght 1 */
    unsigned short TDCLGRP0        : 1; /* Timer0_D3 Compare latch load group 0 */
    unsigned short TDCLGRP1        : 1; /* Timer0_D3 Compare latch load group 1 */
  }TD1CTL0_bit;
} @0x0B40;


/*
enum {
  TDIFG           = 0x0001,
  TDIE            = 0x0002,
  TDCLR           = 0x0004,
  MC0             = 0x0010,
  MC1             = 0x0020,
  ID0             = 0x0040,
  ID1             = 0x0080,
  TDSSEL0         = 0x0100,
  TDSSEL1         = 0x0200,
  CNTL0           = 0x0800,
  CNTL1           = 0x1000,
  TDCLGRP0        = 0x2000,
  TDCLGRP1        = 0x4000,
};
*/


__no_init volatile union
{
  unsigned short TD1CTL1;   /* Timer1_D3 Control 1 */

  struct
  {
    unsigned short TDCLKM0         : 1; /* Timer0_D3 Clocking Mode Bit: 0 */
    unsigned short TDCLKM1         : 1; /* Timer0_D3 Clocking Mode Bit: 1 */
    unsigned short                : 2;
    unsigned short TD2CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD2 */
    unsigned short TD4CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD4 */
    unsigned short TD6CMB          : 1; /* Timer0_D3 TD0CCR Combination in TD6 */
    unsigned short                : 1;
    unsigned short TDIDEX0         : 1; /* Timer0_D3 Input divider expansion Bit: 0 */
    unsigned short TDIDEX1         : 1; /* Timer0_D3 Input divider expansion Bit: 1 */
    unsigned short TDIDEX2         : 1; /* Timer0_D3 Input divider expansion Bit: 2 */
  }TD1CTL1_bit;
} @0x0B42;


/*
enum {
  TDCLKM0         = 0x0001,
  TDCLKM1         = 0x0002,
  TD2CMB          = 0x0010,
  TD4CMB          = 0x0020,
  TD6CMB          = 0x0040,
  TDIDEX0         = 0x0100,
  TDIDEX1         = 0x0200,
  TDIDEX2         = 0x0400,
};
*/


__no_init volatile union
{
  unsigned short TD1CTL2;   /* Timer1_D3 Control 2 */

  struct
  {
    unsigned short TDCAPM0         : 1; /* Timer0_D3 Capture Mode of Channel 0 */
    unsigned short TDCAPM1         : 1; /* Timer0_D3 Capture Mode of Channel 1 */
    unsigned short TDCAPM2         : 1; /* Timer0_D3 Capture Mode of Channel 2 */
    unsigned short TDCAPM3         : 1; /* Timer0_D3 Capture Mode of Channel 3 */
    unsigned short TDCAPM4         : 1; /* Timer0_D3 Capture Mode of Channel 4 */
    unsigned short TDCAPM5         : 1; /* Timer0_D3 Capture Mode of Channel 5 */
    unsigned short TDCAPM6         : 1; /* Timer0_D3 Capture Mode of Channel 6 */
  }TD1CTL2_bit;
} @0x0B44;


/*
enum {
  TDCAPM0         = 0x0001,
  TDCAPM1         = 0x0002,
  TDCAPM2         = 0x0004,
  TDCAPM3         = 0x0008,
  TDCAPM4         = 0x0010,
  TDCAPM5         = 0x0020,
  TDCAPM6         = 0x0040,
};
*/


  /* Timer1_D3 Counter */
__no_init volatile unsigned short TD1R @ 0x0B46;


__no_init volatile union
{
  unsigned short TD1CCTL0;   /* Timer1_D3 Capture/Compare Control 0 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD1CCTL0_bit;
} @0x0B48;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  CLLD0           = 0x0200,
  CLLD1           = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer1_D3 Capture/Compare 0 */
__no_init volatile unsigned short TD1CCR0 @ 0x0B4A;


  /* Timer1_D3 Capture/Compare Latch 0 */
__no_init volatile unsigned short TD1CL0 @ 0x0B4C;


__no_init volatile union
{
  unsigned short TD1CCTL1;   /* Timer1_D3 Capture/Compare Control 1 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD1CCTL1_bit;
} @0x0B4E;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  CLLD0           = 0x0200,
  CLLD1           = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer1_D3 Capture/Compare 1 */
__no_init volatile unsigned short TD1CCR1 @ 0x0B50;


  /* Timer1_D3 Capture/Compare Latch 1 */
__no_init volatile unsigned short TD1CL1 @ 0x0B52;


__no_init volatile union
{
  unsigned short TD1CCTL2;   /* Timer1_D3 Capture/Compare Control 2 */

  struct
  {
    unsigned short CCIFG           : 1; /* Capture/compare interrupt flag */
    unsigned short COV             : 1; /* Capture/compare overflow flag */
    unsigned short OUT             : 1; /* PWM Output signal if output mode 0 */
    unsigned short CCI             : 1; /* Capture input signal (read) */
    unsigned short CCIE            : 1; /* Capture/compare interrupt enable */
    unsigned short OUTMOD0         : 1; /* Output mode 0 */
    unsigned short OUTMOD1         : 1; /* Output mode 1 */
    unsigned short OUTMOD2         : 1; /* Output mode 2 */
    unsigned short CAP             : 1; /* Capture mode: 1 /Compare mode : 0 */
    unsigned short CLLD0           : 1; /* Compare latch load source 0 */
    unsigned short CLLD1           : 1; /* Compare latch load source 1 */
    unsigned short SCS             : 1; /* Capture sychronize */
    unsigned short CCIS0           : 1; /* Capture input select 0 */
    unsigned short CCIS1           : 1; /* Capture input select 1 */
    unsigned short CM0             : 1; /* Capture mode 0 */
    unsigned short CM1             : 1; /* Capture mode 1 */
  }TD1CCTL2_bit;
} @0x0B54;


/*
enum {
  CCIFG           = 0x0001,
  COV             = 0x0002,
  OUT             = 0x0004,
  CCI             = 0x0008,
  CCIE            = 0x0010,
  OUTMOD0         = 0x0020,
  OUTMOD1         = 0x0040,
  OUTMOD2         = 0x0080,
  CAP             = 0x0100,
  CLLD0           = 0x0200,
  CLLD1           = 0x0400,
  SCS             = 0x0800,
  CCIS0           = 0x1000,
  CCIS1           = 0x2000,
  CM0             = 0x4000,
  CM1             = 0x8000,
};
*/


  /* Timer1_D3 Capture/Compare 2 */
__no_init volatile unsigned short TD1CCR2 @ 0x0B56;


  /* Timer1_D3 Capture/Compare Latch 2 */
__no_init volatile unsigned short TD1CL2 @ 0x0B58;


__no_init volatile union
{
  unsigned short TD1HCTL0;   /* Timer1_D3 High-resolution Control Register 0 */

  struct
  {
    unsigned short TDHEN           : 1; /* Timer0_D3 High-Resolution Enable */
    unsigned short TDHREGEN        : 1; /* Timer0_D3 High-Resolution Regulated Mode */
    unsigned short TDHEAEN         : 1; /* Timer0_D3 High-Resolution clock error accum. enable */
    unsigned short TDHRON          : 1; /* Timer0_D3 High-Resolution Generator forced o */
    unsigned short TDHM0           : 1; /* Timer0_D3 High-Resoltuion Clock Mult. Bit: 0 */
    unsigned short TDHM1           : 1; /* Timer0_D3 High-Resoltuion Clock Mult. Bit: 1 */
    unsigned short TDHD0           : 1; /* Timer0_D3 High-Resolution clock divider Bit: 0 */
    unsigned short TDHD1           : 1; /* Timer0_D3 High-Resolution clock divider Bit: 1 */
    unsigned short TDHFW           : 1; /* Timer0_D7 High-resolution generator fast wakeup enable */
  }TD1HCTL0_bit;
} @0x0B78;


/*
enum {
  TDHEN           = 0x0001,
  TDHREGEN        = 0x0002,
  TDHEAEN         = 0x0004,
  TDHRON          = 0x0008,
  TDHM0           = 0x0010,
  TDHM1           = 0x0020,
  TDHD0           = 0x0040,
  TDHD1           = 0x0080,
  TDHFW           = 0x0100,
};
*/


__no_init volatile union
{
  unsigned short TD1HCTL1;   /* Timer1_D3 High-resolution Control Register 1 */

  struct
  {
    unsigned short                : 1;
    unsigned short TDHCLKTRIM0     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 0 */
    unsigned short TDHCLKTRIM1     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 1 */
    unsigned short TDHCLKTRIM2     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 2 */
    unsigned short TDHCLKTRIM3     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 3 */
    unsigned short TDHCLKTRIM4     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 4 */
    unsigned short TDHCLKTRIM5     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 5 */
    unsigned short TDHCLKTRIM6     : 1; /* Timer0_D3 High-Resolution Clock Trim Bit: 6 */
    unsigned short TDHCLKSR0       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 0 */
    unsigned short TDHCLKSR1       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 1 */
    unsigned short TDHCLKSR2       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 2 */
    unsigned short TDHCLKSR3       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 3 */
    unsigned short TDHCLKSR4       : 1; /* Timer0_D3 High-Resolution Clock Sub-Range Bit: 4 */
    unsigned short TDHCLKR0        : 1; /* Timer0_D3 High-Resolution Clock Range Bit: 0 */
    unsigned short TDHCLKR1        : 1; /* Timer0_D3 High-Resolution Clock Range Bit: 1 */
    unsigned short TDHCLKCR        : 1; /* Timer0_D3 High-Resolution Coarse Clock Range */
  }TD1HCTL1_bit;
} @0x0B7A;


/*
enum {
  TDHCLKTRIM0     = 0x0002,
  TDHCLKTRIM1     = 0x0004,
  TDHCLKTRIM2     = 0x0008,
  TDHCLKTRIM3     = 0x0010,
  TDHCLKTRIM4     = 0x0020,
  TDHCLKTRIM5     = 0x0040,
  TDHCLKTRIM6     = 0x0080,
  TDHCLKSR0       = 0x0100,
  TDHCLKSR1       = 0x0200,
  TDHCLKSR2       = 0x0400,
  TDHCLKSR3       = 0x0800,
  TDHCLKSR4       = 0x1000,
  TDHCLKR0        = 0x2000,
  TDHCLKR1        = 0x4000,
  TDHCLKCR        = 0x8000,
};
*/


__no_init volatile union
{
  unsigned short TD1HINT;   /* Timer1_D3 High-resolution Interrupt Register */

  struct
  {
    unsigned short TDHFLIFG        : 1; /* Timer0_D3 High-Res. fail low Interrupt Flag */
    unsigned short TDHFHIFG        : 1; /* Timer0_D3 High-Res. fail high Interrupt Flag */
    unsigned short TDHLKIFG        : 1; /* Timer0_D3 High-Res. frequency lock Interrupt Flag */
    unsigned short TDHUNLKIFG      : 1; /* Timer0_D3 High-Res. frequency unlock Interrupt Flag */
    unsigned short                : 4;
    unsigned short TDHFLIE         : 1; /* Timer0_D3 High-Res. fail low Interrupt Enable */
    unsigned short TDHFHIE         : 1; /* Timer0_D3 High-Res. fail high Interrupt Enable */
    unsigned short TDHLKIE         : 1; /* Timer0_D3 High-Res. frequency lock Interrupt Enable */
    unsigned short TDHUNLKIE       : 1; /* Timer0_D3 High-Res. frequency unlock Interrupt Enable */
  }TD1HINT_bit;
} @0x0B7C;


/*
enum {
  TDHFLIFG        = 0x0001,
  TDHFHIFG        = 0x0002,
  TDHLKIFG        = 0x0004,
  TDHUNLKIFG      = 0x0008,
  TDHFLIE         = 0x0100,
  TDHFHIE         = 0x0200,
  TDHLKIE         = 0x0400,
  TDHUNLKIE       = 0x0800,
};
*/


  /* Timer1_D3 Interrupt Vector Word */
__no_init volatile unsigned short TD1IV @ 0x0B7E;


#define __MSP430_HAS_T1D3__            /* Definition to show that Module is available */


/* TD1IV Definitions */
#define TD1IV_NONE           (0x0000u)    /* No Interrupt pending */
#define TD1IV_TD1CCR1       (0x0002u)    /* TD1CCR1_CCIFG */
#define TD1IV_TD1CCR2       (0x0004u)    /* TD1CCR2_CCIFG */
#define TD1IV_TD1CCR3       (0x0006u)    /* TD1CCR3_CCIFG */
#define TD1IV_TD1CCR4       (0x0008u)    /* TD1CCR4_CCIFG */
#define TD1IV_TD1CCR5       (0x000Au)    /* TD1CCR5_CCIFG */
#define TD1IV_TD1CCR6       (0x000Cu)    /* TD1CCR6_CCIFG */
#define TD1IV_RES_14         (0x000Eu)    /* Reserverd */
#define TD1IV_TD1IFG        (0x0010u)    /* TD1IFG */
#define TD1IV_TDHFLIFG       (0x0012u)    /* TDHFLIFG Clock fail low */
#define TD1IV_TDHFHIFG       (0x0014u)    /* TDHFLIFG Clock fail high */
#define TD1IV_TDHLKIFG       (0x0016u)    /* TDHLKIE Clock lock*/
#define TD1IV_TDHUNLKIFG     (0x0018u)    /* TDHUNLKIE Clock unlock */

/*-------------------------------------------------------------------------
 *   Timer_Event_Control
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short TEC0XCTL0;   /* Timer Event Control 0 External Control 0 */

  struct
  {
    unsigned short TECXFLTHLD0     : 1; /* TEV Ext. fault signal hold for CE0 */
    unsigned short TECXFLTHLD1     : 1; /* TEV Ext. fault signal hold for CE1 */
    unsigned short TECXFLTHLD2     : 1; /* TEV Ext. fault signal hold for CE2 */
    unsigned short TECXFLTHLD3     : 1; /* TEV Ext. fault signal hold for CE3 */
    unsigned short TECXFLTHLD4     : 1; /* TEV Ext. fault signal hold for CE4 */
    unsigned short TECXFLTHLD5     : 1; /* TEV Ext. fault signal hold for CE5 */
    unsigned short TECXFLTHLD6     : 1; /* TEV Ext. fault signal hold for CE6 */
    unsigned short                : 1;
    unsigned short TECXFLTEN0      : 1; /* TEV Ext. fault signal enable for CE0 */
    unsigned short TECXFLTEN1      : 1; /* TEV Ext. fault signal enable for CE1 */
    unsigned short TECXFLTEN2      : 1; /* TEV Ext. fault signal enable for CE2 */
    unsigned short TECXFLTEN3      : 1; /* TEV Ext. fault signal enable for CE3 */
    unsigned short TECXFLTEN4      : 1; /* TEV Ext. fault signal enable for CE4 */
    unsigned short TECXFLTEN5      : 1; /* TEV Ext. fault signal enable for CE5 */
    unsigned short TECXFLTEN6      : 1; /* TEV Ext. fault signal enable for CE6 */
  }TEC0XCTL0_bit;
} @0x0C00;


enum {
  TECXFLTHLD0     = 0x0001,
  TECXFLTHLD1     = 0x0002,
  TECXFLTHLD2     = 0x0004,
  TECXFLTHLD3     = 0x0008,
  TECXFLTHLD4     = 0x0010,
  TECXFLTHLD5     = 0x0020,
  TECXFLTHLD6     = 0x0040,
  TECXFLTEN0      = 0x0100,
  TECXFLTEN1      = 0x0200,
  TECXFLTEN2      = 0x0400,
  TECXFLTEN3      = 0x0800,
  TECXFLTEN4      = 0x1000,
  TECXFLTEN5      = 0x2000,
  TECXFLTEN6      = 0x4000
};


__no_init volatile union
{
  unsigned short TEC0XCTL1;   /* Timer Event Control 0 External Control 1 */

  struct
  {
    unsigned short TECXFLTPOL0     : 1; /* TEV Polarity Bit of ext. fault 0 */
    unsigned short TECXFLTPOL1     : 1; /* TEV Polarity Bit of ext. fault 1 */
    unsigned short TECXFLTPOL2     : 1; /* TEV Polarity Bit of ext. fault 2 */
    unsigned short TECXFLTPOL3     : 1; /* TEV Polarity Bit of ext. fault 3 */
    unsigned short TECXFLTPOL4     : 1; /* TEV Polarity Bit of ext. fault 4 */
    unsigned short TECXFLTPOL5     : 1; /* TEV Polarity Bit of ext. fault 5 */
    unsigned short TECXFLTPOL6     : 1; /* TEV Polarity Bit of ext. fault 6 */
    unsigned short                : 1;
    unsigned short TECXFLTLVS0     : 1; /* TEV Signal Type of Ext. fault 0 */
    unsigned short TECXFLTLVS1     : 1; /* TEV Signal Type of Ext. fault 1 */
    unsigned short TECXFLTLVS2     : 1; /* TEV Signal Type of Ext. fault 2 */
    unsigned short TECXFLTLVS3     : 1; /* TEV Signal Type of Ext. fault 3 */
    unsigned short TECXFLTLVS4     : 1; /* TEV Signal Type of Ext. fault 4 */
    unsigned short TECXFLTLVS5     : 1; /* TEV Signal Type of Ext. fault 5 */
    unsigned short TECXFLTLVS6     : 1; /* TEV Signal Type of Ext. fault 6 */
  }TEC0XCTL1_bit;
} @0x0C02;


enum {
  TECXFLTPOL0     = 0x0001,
  TECXFLTPOL1     = 0x0002,
  TECXFLTPOL2     = 0x0004,
  TECXFLTPOL3     = 0x0008,
  TECXFLTPOL4     = 0x0010,
  TECXFLTPOL5     = 0x0020,
  TECXFLTPOL6     = 0x0040,
  TECXFLTLVS0     = 0x0100,
  TECXFLTLVS1     = 0x0200,
  TECXFLTLVS2     = 0x0400,
  TECXFLTLVS3     = 0x0800,
  TECXFLTLVS4     = 0x1000,
  TECXFLTLVS5     = 0x2000,
  TECXFLTLVS6     = 0x4000
};


__no_init volatile union
{
  unsigned short TEC0XCTL2;   /* Timer Event Control 0 External Control 2 */

  struct
  {
    unsigned short TECCLKSEL0      : 1; /* TEV Aux. Clock Select Bit: 0 */
    unsigned short TECCLKSEL1      : 1; /* TEV Aux. Clock Select Bit: 1 */
    unsigned short TECAXCLREN      : 1; /* TEV Auxilary clear signal control */
    unsigned short TECEXCLREN      : 1; /* TEV Ext. clear signal control */
    unsigned short TECEXCLRHLD     : 1; /* TEV External clear signal hold bit */
    unsigned short TECEXCLRPOL     : 1; /* TEV Polarity Bit of ext. clear */
    unsigned short TECEXCLRLVS     : 1; /* TEV Signal Type of Ext. clear */
  }TEC0XCTL2_bit;
} @0x0C04;


enum {
  TECCLKSEL0      = 0x0001,
  TECCLKSEL1      = 0x0002,
  TECAXCLREN      = 0x0004,
  TECEXCLREN      = 0x0008,
  TECEXCLRHLD     = 0x0010,
  TECEXCLRPOL     = 0x0020,
  TECEXCLRLVS     = 0x0040
};


__no_init volatile union
{
  unsigned short TEC0STA;   /* Timer Event Control 0 Status */

  struct
  {
    unsigned short TECXFLT0STA     : 1; /* TEV External fault status flag for CE0 */
    unsigned short TECXFLT1STA     : 1; /* TEV External fault status flag for CE1 */
    unsigned short TECXFLT2STA     : 1; /* TEV External fault status flag for CE2 */
    unsigned short TECXFLT3STA     : 1; /* TEV External fault status flag for CE3 */
    unsigned short TECXFLT4STA     : 1; /* TEV External fault status flag for CE4 */
    unsigned short TECXFLT5STA     : 1; /* TEV External fault status flag for CE5 */
    unsigned short TECXFLT6STA     : 1; /* TEV External fault status flag for CE6 */
    unsigned short                : 1;
    unsigned short TECXCLRSTA      : 1; /* TEC External clear status flag */
  }TEC0STA_bit;
} @0x0C06;


enum {
  TECXFLT0STA     = 0x0001,
  TECXFLT1STA     = 0x0002,
  TECXFLT2STA     = 0x0004,
  TECXFLT3STA     = 0x0008,
  TECXFLT4STA     = 0x0010,
  TECXFLT5STA     = 0x0020,
  TECXFLT6STA     = 0x0040,
  TECXCLRSTA      = 0x0100
};


__no_init volatile union
{
  unsigned short TEC0XINT;   /* Timer Event Control 0 External Interrupt */

  struct
  {
    unsigned short TECAXCLRIFG     : 1; /* TEC Aux. Clear Interrupt Flag */
    unsigned short TECEXCLRIFG     : 1; /* TEC External Clear Interrupt Flag */
    unsigned short TECXFLTIFG      : 1; /* TEC External Fault Interrupt Flag */
    unsigned short                : 5;
    unsigned short TECAXCLRIE      : 1; /* TEC Aux. Clear Interrupt Enable */
    unsigned short TECEXCLRIE      : 1; /* TEC External Clear Interrupt Enable */
    unsigned short TECXFLTIE       : 1; /* TEC External Fault Interrupt Enable */
  }TEC0XINT_bit;
} @0x0C08;


enum {
  TECAXCLRIFG     = 0x0001,
  TECEXCLRIFG     = 0x0002,
  TECXFLTIFG      = 0x0004,
  TECAXCLRIE      = 0x0100,
  TECEXCLRIE      = 0x0200,
  TECXFLTIE       = 0x0400
};


  /* Timer Event Control 0 Interrupt Vector */
__no_init volatile unsigned short TEC0IV @ 0x0C0A;


#define __MSP430_HAS_TEV0__             /* Definition to show that Module is available */


/* TECxXCTL0 Control Bits */
#define TECXFLTHLD0_L       (0x0001u)  /* TEV Ext. fault signal hold for CE0 */
#define TECXFLTHLD1_L       (0x0002u)  /* TEV Ext. fault signal hold for CE1 */
#define TECXFLTHLD2_L       (0x0004u)  /* TEV Ext. fault signal hold for CE2 */
#define TECXFLTHLD3_L       (0x0008u)  /* TEV Ext. fault signal hold for CE3 */
#define TECXFLTHLD4_L       (0x0010u)  /* TEV Ext. fault signal hold for CE4 */
#define TECXFLTHLD5_L       (0x0020u)  /* TEV Ext. fault signal hold for CE5 */
#define TECXFLTHLD6_L       (0x0040u)  /* TEV Ext. fault signal hold for CE6 */

/* TECxXCTL0 Control Bits */
#define TECXFLTEN0_H        (0x0001u)  /* TEV Ext. fault signal enable for CE0 */
#define TECXFLTEN1_H        (0x0002u)  /* TEV Ext. fault signal enable for CE1 */
#define TECXFLTEN2_H        (0x0004u)  /* TEV Ext. fault signal enable for CE2 */
#define TECXFLTEN3_H        (0x0008u)  /* TEV Ext. fault signal enable for CE3 */
#define TECXFLTEN4_H        (0x0010u)  /* TEV Ext. fault signal enable for CE4 */
#define TECXFLTEN5_H        (0x0020u)  /* TEV Ext. fault signal enable for CE5 */
#define TECXFLTEN6_H        (0x0040u)  /* TEV Ext. fault signal enable for CE6 */

/* TECxXCTL1 Control Bits */
#define TECXFLTPOL0_L       (0x0001u)  /* TEV Polarity Bit of ext. fault 0 */
#define TECXFLTPOL1_L       (0x0002u)  /* TEV Polarity Bit of ext. fault 1 */
#define TECXFLTPOL2_L       (0x0004u)  /* TEV Polarity Bit of ext. fault 2 */
#define TECXFLTPOL3_L       (0x0008u)  /* TEV Polarity Bit of ext. fault 3 */
#define TECXFLTPOL4_L       (0x0010u)  /* TEV Polarity Bit of ext. fault 4 */
#define TECXFLTPOL5_L       (0x0020u)  /* TEV Polarity Bit of ext. fault 5 */
#define TECXFLTPOL6_L       (0x0040u)  /* TEV Polarity Bit of ext. fault 6 */

/* TECxXCTL1 Control Bits */
#define TECXFLTLVS0_H       (0x0001u)  /* TEV Signal Type of Ext. fault 0 */
#define TECXFLTLVS1_H       (0x0002u)  /* TEV Signal Type of Ext. fault 1 */
#define TECXFLTLVS2_H       (0x0004u)  /* TEV Signal Type of Ext. fault 2 */
#define TECXFLTLVS3_H       (0x0008u)  /* TEV Signal Type of Ext. fault 3 */
#define TECXFLTLVS4_H       (0x0010u)  /* TEV Signal Type of Ext. fault 4 */
#define TECXFLTLVS5_H       (0x0020u)  /* TEV Signal Type of Ext. fault 5 */
#define TECXFLTLVS6_H       (0x0040u)  /* TEV Signal Type of Ext. fault 6 */

/* TECxXCTL2 Control Bits */
#define TECCLKSEL0_L        (0x0001u)  /* TEV Aux. Clock Select Bit: 0 */
#define TECCLKSEL1_L        (0x0002u)  /* TEV Aux. Clock Select Bit: 1 */
#define TECAXCLREN_L        (0x0004u)  /* TEV Auxilary clear signal control */
#define TECEXCLREN_L        (0x0008u)  /* TEV Ext. clear signal control */
#define TECEXCLRHLD_L       (0x0010u)  /* TEV External clear signal hold bit */
#define TECEXCLRPOL_L       (0x0020u)  /* TEV Polarity Bit of ext. clear */
#define TECEXCLRLVS_L       (0x0040u)  /* TEV Signal Type of Ext. clear */

#define TECCLKSEL_0         (0x0000u)  /* TEV Aux. Clock Select: CLK0 */
#define TECCLKSEL_1         (0x0001u)  /* TEV Aux. Clock Select: CLK1 */
#define TECCLKSEL_2         (0x0002u)  /* TEV Aux. Clock Select: CLK2 */
#define TECCLKSEL_3         (0x0003u)  /* TEV Aux. Clock Select: CLK3 */

/* TECxSTA Control Bits */
#define TECXFLT0STA_L       (0x0001u)  /* TEV External fault status flag for CE0 */
#define TECXFLT1STA_L       (0x0002u)  /* TEV External fault status flag for CE1 */
#define TECXFLT2STA_L       (0x0004u)  /* TEV External fault status flag for CE2 */
#define TECXFLT3STA_L       (0x0008u)  /* TEV External fault status flag for CE3 */
#define TECXFLT4STA_L       (0x0010u)  /* TEV External fault status flag for CE4 */
#define TECXFLT5STA_L       (0x0020u)  /* TEV External fault status flag for CE5 */
#define TECXFLT6STA_L       (0x0040u)  /* TEV External fault status flag for CE6 */

/* TECxSTA Control Bits */
#define TECXCLRSTA_H        (0x0001u)  /* TEC External clear status flag */

/* TECxXINT Control Bits */
#define TECAXCLRIFG_L       (0x0001u)   /* TEC Aux. Clear Interrupt Flag */
#define TECEXCLRIFG_L       (0x0002u)   /* TEC External Clear Interrupt Flag */
#define TECXFLTIFG_L        (0x0004u)   /* TEC External Fault Interrupt Flag */

/* TECxXINT Control Bits */
#define TECAXCLRIE_H        (0x0001u)   /* TEC Aux. Clear Interrupt Enable */
#define TECEXCLRIE_H        (0x0002u)   /* TEC External Clear Interrupt Enable */
#define TECXFLTIE_H         (0x0004u)   /* TEC External Fault Interrupt Enable */

/* TEC0IV Definitions */
#define TEC0IV_NONE         (0x0000u)    /* No Interrupt pending */
#define TEC0IV_TECXFLTIFG   (0x0002u)    /* TEC0XFLTIFG */
#define TEC0IV_TECEXCLRIFG  (0x0004u)    /* TEC0EXCLRIFG */
#define TEC0IV_TECAXCLRIFG  (0x0006u)    /* TEC0AXCLRIFG */

/*-------------------------------------------------------------------------
 *   Timer_Event_Control
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short TEC1XCTL0;   /* Timer Event Control 1 External Control 0 */

  struct
  {
    unsigned short TECXFLTHLD0     : 1; /* TEV Ext. fault signal hold for CE0 */
    unsigned short TECXFLTHLD1     : 1; /* TEV Ext. fault signal hold for CE1 */
    unsigned short TECXFLTHLD2     : 1; /* TEV Ext. fault signal hold for CE2 */
    unsigned short TECXFLTHLD3     : 1; /* TEV Ext. fault signal hold for CE3 */
    unsigned short TECXFLTHLD4     : 1; /* TEV Ext. fault signal hold for CE4 */
    unsigned short TECXFLTHLD5     : 1; /* TEV Ext. fault signal hold for CE5 */
    unsigned short TECXFLTHLD6     : 1; /* TEV Ext. fault signal hold for CE6 */
    unsigned short                : 1;
    unsigned short TECXFLTEN0      : 1; /* TEV Ext. fault signal enable for CE0 */
    unsigned short TECXFLTEN1      : 1; /* TEV Ext. fault signal enable for CE1 */
    unsigned short TECXFLTEN2      : 1; /* TEV Ext. fault signal enable for CE2 */
    unsigned short TECXFLTEN3      : 1; /* TEV Ext. fault signal enable for CE3 */
    unsigned short TECXFLTEN4      : 1; /* TEV Ext. fault signal enable for CE4 */
    unsigned short TECXFLTEN5      : 1; /* TEV Ext. fault signal enable for CE5 */
    unsigned short TECXFLTEN6      : 1; /* TEV Ext. fault signal enable for CE6 */
  }TEC1XCTL0_bit;
} @0x0C20;


/*
enum {
  TECXFLTHLD0     = 0x0001,
  TECXFLTHLD1     = 0x0002,
  TECXFLTHLD2     = 0x0004,
  TECXFLTHLD3     = 0x0008,
  TECXFLTHLD4     = 0x0010,
  TECXFLTHLD5     = 0x0020,
  TECXFLTHLD6     = 0x0040,
  TECXFLTEN0      = 0x0100,
  TECXFLTEN1      = 0x0200,
  TECXFLTEN2      = 0x0400,
  TECXFLTEN3      = 0x0800,
  TECXFLTEN4      = 0x1000,
  TECXFLTEN5      = 0x2000,
  TECXFLTEN6      = 0x4000,
};
*/


__no_init volatile union
{
  unsigned short TEC1XCTL1;   /* Timer Event Control 1 External Control 1 */

  struct
  {
    unsigned short TECXFLTPOL0     : 1; /* TEV Polarity Bit of ext. fault 0 */
    unsigned short TECXFLTPOL1     : 1; /* TEV Polarity Bit of ext. fault 1 */
    unsigned short TECXFLTPOL2     : 1; /* TEV Polarity Bit of ext. fault 2 */
    unsigned short TECXFLTPOL3     : 1; /* TEV Polarity Bit of ext. fault 3 */
    unsigned short TECXFLTPOL4     : 1; /* TEV Polarity Bit of ext. fault 4 */
    unsigned short TECXFLTPOL5     : 1; /* TEV Polarity Bit of ext. fault 5 */
    unsigned short TECXFLTPOL6     : 1; /* TEV Polarity Bit of ext. fault 6 */
    unsigned short                : 1;
    unsigned short TECXFLTLVS0     : 1; /* TEV Signal Type of Ext. fault 0 */
    unsigned short TECXFLTLVS1     : 1; /* TEV Signal Type of Ext. fault 1 */
    unsigned short TECXFLTLVS2     : 1; /* TEV Signal Type of Ext. fault 2 */
    unsigned short TECXFLTLVS3     : 1; /* TEV Signal Type of Ext. fault 3 */
    unsigned short TECXFLTLVS4     : 1; /* TEV Signal Type of Ext. fault 4 */
    unsigned short TECXFLTLVS5     : 1; /* TEV Signal Type of Ext. fault 5 */
    unsigned short TECXFLTLVS6     : 1; /* TEV Signal Type of Ext. fault 6 */
  }TEC1XCTL1_bit;
} @0x0C22;


/*
enum {
  TECXFLTPOL0     = 0x0001,
  TECXFLTPOL1     = 0x0002,
  TECXFLTPOL2     = 0x0004,
  TECXFLTPOL3     = 0x0008,
  TECXFLTPOL4     = 0x0010,
  TECXFLTPOL5     = 0x0020,
  TECXFLTPOL6     = 0x0040,
  TECXFLTLVS0     = 0x0100,
  TECXFLTLVS1     = 0x0200,
  TECXFLTLVS2     = 0x0400,
  TECXFLTLVS3     = 0x0800,
  TECXFLTLVS4     = 0x1000,
  TECXFLTLVS5     = 0x2000,
  TECXFLTLVS6     = 0x4000,
};
*/


__no_init volatile union
{
  unsigned short TEC1XCTL2;   /* Timer Event Control 1 External Control 2 */

  struct
  {
    unsigned short TECCLKSEL0      : 1; /* TEV Aux. Clock Select Bit: 0 */
    unsigned short TECCLKSEL1      : 1; /* TEV Aux. Clock Select Bit: 1 */
    unsigned short TECAXCLREN      : 1; /* TEV Auxilary clear signal control */
    unsigned short TECEXCLREN      : 1; /* TEV Ext. clear signal control */
    unsigned short TECEXCLRHLD     : 1; /* TEV External clear signal hold bit */
    unsigned short TECEXCLRPOL     : 1; /* TEV Polarity Bit of ext. clear */
    unsigned short TECEXCLRLVS     : 1; /* TEV Signal Type of Ext. clear */
  }TEC1XCTL2_bit;
} @0x0C24;


/*
enum {
  TECCLKSEL0      = 0x0001,
  TECCLKSEL1      = 0x0002,
  TECAXCLREN      = 0x0004,
  TECEXCLREN      = 0x0008,
  TECEXCLRHLD     = 0x0010,
  TECEXCLRPOL     = 0x0020,
  TECEXCLRLVS     = 0x0040,
};
*/


__no_init volatile union
{
  unsigned short TEC1STA;   /* Timer Event Control 1 Status */

  struct
  {
    unsigned short TECXFLT0STA     : 1; /* TEV External fault status flag for CE0 */
    unsigned short TECXFLT1STA     : 1; /* TEV External fault status flag for CE1 */
    unsigned short TECXFLT2STA     : 1; /* TEV External fault status flag for CE2 */
    unsigned short TECXFLT3STA     : 1; /* TEV External fault status flag for CE3 */
    unsigned short TECXFLT4STA     : 1; /* TEV External fault status flag for CE4 */
    unsigned short TECXFLT5STA     : 1; /* TEV External fault status flag for CE5 */
    unsigned short TECXFLT6STA     : 1; /* TEV External fault status flag for CE6 */
    unsigned short                : 1;
    unsigned short TECXCLRSTA      : 1; /* TEC External clear status flag */
  }TEC1STA_bit;
} @0x0C26;


/*
enum {
  TECXFLT0STA     = 0x0001,
  TECXFLT1STA     = 0x0002,
  TECXFLT2STA     = 0x0004,
  TECXFLT3STA     = 0x0008,
  TECXFLT4STA     = 0x0010,
  TECXFLT5STA     = 0x0020,
  TECXFLT6STA     = 0x0040,
  TECXCLRSTA      = 0x0100,
};
*/


__no_init volatile union
{
  unsigned short TEC1XINT;   /* Timer Event Control 1 External Interrupt */

  struct
  {
    unsigned short TECAXCLRIFG     : 1; /* TEC Aux. Clear Interrupt Flag */
    unsigned short TECEXCLRIFG     : 1; /* TEC External Clear Interrupt Flag */
    unsigned short TECXFLTIFG      : 1; /* TEC External Fault Interrupt Flag */
    unsigned short                : 5;
    unsigned short TECAXCLRIE      : 1; /* TEC Aux. Clear Interrupt Enable */
    unsigned short TECEXCLRIE      : 1; /* TEC External Clear Interrupt Enable */
    unsigned short TECXFLTIE       : 1; /* TEC External Fault Interrupt Enable */
  }TEC1XINT_bit;
} @0x0C28;


/*
enum {
  TECAXCLRIFG     = 0x0001,
  TECEXCLRIFG     = 0x0002,
  TECXFLTIFG      = 0x0004,
  TECAXCLRIE      = 0x0100,
  TECEXCLRIE      = 0x0200,
  TECXFLTIE       = 0x0400,
};
*/


  /* Timer Event Control 1 Interrupt Vector */
__no_init volatile unsigned short TEC1IV @ 0x0C2A;


#define __MSP430_HAS_TEV1__             /* Definition to show that Module is available */


/* TECIV Definitions */
#define TEC1IV_NONE         (0x0000u)    /* No Interrupt pending */
#define TEC1IV_TECXFLTIFG   (0x0002u)    /* TEC1XFLTIFG */
#define TEC1IV_TECEXCLRIFG  (0x0004u)    /* TEC1EXCLRIFG */
#define TEC1IV_TECAXCLRIFG  (0x0006u)    /* TEC1AXCLRIFG */

/*-------------------------------------------------------------------------
 *   UCS  Unified System Clock
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short UCSCTL0;   /* UCS Control Register 0 */

  struct
  {
    unsigned short                : 3;
    unsigned short MOD0            : 1; /* Modulation Bit Counter Bit : 0 */
    unsigned short MOD1            : 1; /* Modulation Bit Counter Bit : 1 */
    unsigned short MOD2            : 1; /* Modulation Bit Counter Bit : 2 */
    unsigned short MOD3            : 1; /* Modulation Bit Counter Bit : 3 */
    unsigned short MOD4            : 1; /* Modulation Bit Counter Bit : 4 */
    unsigned short DCO0            : 1; /* DCO TAP Bit : 0 */
    unsigned short DCO1            : 1; /* DCO TAP Bit : 1 */
    unsigned short DCO2            : 1; /* DCO TAP Bit : 2 */
    unsigned short DCO3            : 1; /* DCO TAP Bit : 3 */
    unsigned short DCO4            : 1; /* DCO TAP Bit : 4 */
  }UCSCTL0_bit;
} @0x0160;


enum {
  MOD0            = 0x0008,
  MOD1            = 0x0010,
  MOD2            = 0x0020,
  MOD3            = 0x0040,
  MOD4            = 0x0080,
  DCO0            = 0x0100,
  DCO1            = 0x0200,
  DCO2            = 0x0400,
  DCO3            = 0x0800,
  DCO4            = 0x1000
};


__no_init volatile union
{
  unsigned short UCSCTL1;   /* UCS Control Register 1 */

  struct
  {
    unsigned short DISMOD          : 1; /* Disable Modulation */
    unsigned short                : 3;
    unsigned short DCORSEL0        : 1; /* DCO Freq. Range Select Bit : 0 */
    unsigned short DCORSEL1        : 1; /* DCO Freq. Range Select Bit : 1 */
    unsigned short DCORSEL2        : 1; /* DCO Freq. Range Select Bit : 2 */
  }UCSCTL1_bit;
} @0x0162;


enum {
  DISMOD          = 0x0001,
  DCORSEL0        = 0x0010,
  DCORSEL1        = 0x0020,
  DCORSEL2        = 0x0040
};


__no_init volatile union
{
  unsigned short UCSCTL2;   /* UCS Control Register 2 */

  struct
  {
    unsigned short FLLN0           : 1; /* FLL Multipier Bit : 0 */
    unsigned short FLLN1           : 1; /* FLL Multipier Bit : 1 */
    unsigned short FLLN2           : 1; /* FLL Multipier Bit : 2 */
    unsigned short FLLN3           : 1; /* FLL Multipier Bit : 3 */
    unsigned short FLLN4           : 1; /* FLL Multipier Bit : 4 */
    unsigned short FLLN5           : 1; /* FLL Multipier Bit : 5 */
    unsigned short FLLN6           : 1; /* FLL Multipier Bit : 6 */
    unsigned short FLLN7           : 1; /* FLL Multipier Bit : 7 */
    unsigned short FLLN8           : 1; /* FLL Multipier Bit : 8 */
    unsigned short FLLN9           : 1; /* FLL Multipier Bit : 9 */
    unsigned short                : 2;
    unsigned short FLLD0           : 1; /* Loop Divider Bit : 0 */
    unsigned short FLLD1           : 1; /* Loop Divider Bit : 1 */
    unsigned short FLLD2           : 1; /* Loop Divider Bit : 1 */
  }UCSCTL2_bit;
} @0x0164;


enum {
  FLLN0           = 0x0001,
  FLLN1           = 0x0002,
  FLLN2           = 0x0004,
  FLLN3           = 0x0008,
  FLLN4           = 0x0010,
  FLLN5           = 0x0020,
  FLLN6           = 0x0040,
  FLLN7           = 0x0080,
  FLLN8           = 0x0100,
  FLLN9           = 0x0200,
  FLLD0           = 0x1000,
  FLLD1           = 0x2000,
  FLLD2           = 0x4000
};


__no_init volatile union
{
  unsigned short UCSCTL3;   /* UCS Control Register 3 */

  struct
  {
    unsigned short FLLREFDIV0      : 1; /* Reference Divider Bit : 0 */
    unsigned short FLLREFDIV1      : 1; /* Reference Divider Bit : 1 */
    unsigned short FLLREFDIV2      : 1; /* Reference Divider Bit : 2 */
    unsigned short                : 1;
    unsigned short SELREF0         : 1; /* FLL Reference Clock Select Bit : 0 */
    unsigned short SELREF1         : 1; /* FLL Reference Clock Select Bit : 1 */
    unsigned short SELREF2         : 1; /* FLL Reference Clock Select Bit : 2 */
  }UCSCTL3_bit;
} @0x0166;


enum {
  FLLREFDIV0      = 0x0001,
  FLLREFDIV1      = 0x0002,
  FLLREFDIV2      = 0x0004,
  SELREF0         = 0x0010,
  SELREF1         = 0x0020,
  SELREF2         = 0x0040
};


__no_init volatile union
{
  unsigned short UCSCTL4;   /* UCS Control Register 4 */

  struct
  {
    unsigned short SELM0           : 1; /* MCLK Source Select Bit: 0 */
    unsigned short SELM1           : 1; /* MCLK Source Select Bit: 1 */
    unsigned short SELM2           : 1; /* MCLK Source Select Bit: 2 */
    unsigned short                : 1;
    unsigned short SELS0           : 1; /* SMCLK Source Select Bit: 0 */
    unsigned short SELS1           : 1; /* SMCLK Source Select Bit: 1 */
    unsigned short SELS2           : 1; /* SMCLK Source Select Bit: 2 */
    unsigned short                : 1;
    unsigned short SELA0           : 1; /* ACLK Source Select Bit: 0 */
    unsigned short SELA1           : 1; /* ACLK Source Select Bit: 1 */
    unsigned short SELA2           : 1; /* ACLK Source Select Bit: 2 */
  }UCSCTL4_bit;
} @0x0168;


enum {
  SELM0           = 0x0001,
  SELM1           = 0x0002,
  SELM2           = 0x0004,
  SELS0           = 0x0010,
  SELS1           = 0x0020,
  SELS2           = 0x0040,
  SELA0           = 0x0100,
  SELA1           = 0x0200,
  SELA2           = 0x0400
};


__no_init volatile union
{
  unsigned short UCSCTL5;   /* UCS Control Register 5 */

  struct
  {
    unsigned short DIVM0           : 1; /* MCLK Divider Bit: 0 */
    unsigned short DIVM1           : 1; /* MCLK Divider Bit: 1 */
    unsigned short DIVM2           : 1; /* MCLK Divider Bit: 2 */
    unsigned short                : 1;
    unsigned short DIVS0           : 1; /* SMCLK Divider Bit: 0 */
    unsigned short DIVS1           : 1; /* SMCLK Divider Bit: 1 */
    unsigned short DIVS2           : 1; /* SMCLK Divider Bit: 2 */
    unsigned short                : 1;
    unsigned short DIVA0           : 1; /* ACLK Divider Bit: 0 */
    unsigned short DIVA1           : 1; /* ACLK Divider Bit: 1 */
    unsigned short DIVA2           : 1; /* ACLK Divider Bit: 2 */
    unsigned short                : 1;
    unsigned short DIVPA0          : 1; /* ACLK from Pin Divider Bit: 0 */
    unsigned short DIVPA1          : 1; /* ACLK from Pin Divider Bit: 1 */
    unsigned short DIVPA2          : 1; /* ACLK from Pin Divider Bit: 2 */
  }UCSCTL5_bit;
} @0x016A;


enum {
  DIVM0           = 0x0001,
  DIVM1           = 0x0002,
  DIVM2           = 0x0004,
  DIVS0           = 0x0010,
  DIVS1           = 0x0020,
  DIVS2           = 0x0040,
  DIVA0           = 0x0100,
  DIVA1           = 0x0200,
  DIVA2           = 0x0400,
  DIVPA0          = 0x1000,
  DIVPA1          = 0x2000,
  DIVPA2          = 0x4000
};


__no_init volatile union
{
  unsigned short UCSCTL6;   /* UCS Control Register 6 */

  struct
  {
    unsigned short XT1OFF          : 1; /* High Frequency Oscillator 1 (XT1) disable */
    unsigned short SMCLKOFF        : 1; /* SMCLK Off */
    unsigned short XCAP0           : 1; /* XIN/XOUT Cap Bit: 0 */
    unsigned short XCAP1           : 1; /* XIN/XOUT Cap Bit: 1 */
    unsigned short XT1BYPASS       : 1; /* XT1 bypass mode : 0: internal 1:sourced from external pin */
    unsigned short XTS             : 1; /* 1: Selects high-freq. oscillator */
    unsigned short XT1DRIVE0       : 1; /* XT1 Drive Level mode Bit 0 */
    unsigned short XT1DRIVE1       : 1; /* XT1 Drive Level mode Bit 1 */
    unsigned short XT2OFF          : 1; /* High Frequency Oscillator 2 (XT2) disable */
  }UCSCTL6_bit;
} @0x016C;


enum {
  XT1OFF          = 0x0001,
  SMCLKOFF        = 0x0002,
  XCAP0           = 0x0004,
  XCAP1           = 0x0008,
  XT1BYPASS       = 0x0010,
  XTS             = 0x0020,
  XT1DRIVE0       = 0x0040,
  XT1DRIVE1       = 0x0080,
  XT2OFF          = 0x0100
};


__no_init volatile union
{
  unsigned short UCSCTL7;   /* UCS Control Register 7 */

  struct
  {
    unsigned short DCOFFG          : 1; /* DCO Fault Flag */
    unsigned short XT1LFOFFG       : 1; /* XT1 Low Frequency Oscillator Fault Flag */
    unsigned short XT1HFOFFG       : 1; /* XT1 High Frequency Oscillator 1 Fault Flag */
  }UCSCTL7_bit;
} @0x016E;


enum {
  DCOFFG          = 0x0001,
  XT1LFOFFG       = 0x0002,
  XT1HFOFFG       = 0x0004
};


__no_init volatile union
{
  unsigned short UCSCTL8;   /* UCS Control Register 8 */

  struct
  {
    unsigned short ACLKREQEN       : 1; /* ACLK Clock Request Enable */
    unsigned short MCLKREQEN       : 1; /* MCLK Clock Request Enable */
    unsigned short SMCLKREQEN      : 1; /* SMCLK Clock Request Enable */
    unsigned short MODOSCREQEN     : 1; /* MODOSC Clock Request Enable */
  }UCSCTL8_bit;
} @ 0x0170;


enum {
  ACLKREQEN       = 0x0001,
  MCLKREQEN       = 0x0002,
  SMCLKREQEN      = 0x0004,
  MODOSCREQEN     = 0x0008
};


#define __MSP430_HAS_UCS__            /* Definition to show that Module is available */

#define MOD0_L              (0x0008u)    /* Modulation Bit Counter Bit : 0 */
#define MOD1_L              (0x0010u)    /* Modulation Bit Counter Bit : 1 */
#define MOD2_L              (0x0020u)    /* Modulation Bit Counter Bit : 2 */
#define MOD3_L              (0x0040u)    /* Modulation Bit Counter Bit : 3 */
#define MOD4_L              (0x0080u)    /* Modulation Bit Counter Bit : 4 */
#define DCO0_H              (0x0001u)    /* DCO TAP Bit : 0 */
#define DCO1_H              (0x0002u)    /* DCO TAP Bit : 1 */
#define DCO2_H              (0x0004u)    /* DCO TAP Bit : 2 */
#define DCO3_H              (0x0008u)    /* DCO TAP Bit : 3 */
#define DCO4_H              (0x0010u)    /* DCO TAP Bit : 4 */

/* UCSCTL1 Control Bits */
#define DISMOD_L            (0x0001u)    /* Disable Modulation */
#define DCORSEL0_L          (0x0010u)    /* DCO Freq. Range Select Bit : 0 */
#define DCORSEL1_L          (0x0020u)    /* DCO Freq. Range Select Bit : 1 */
#define DCORSEL2_L          (0x0040u)    /* DCO Freq. Range Select Bit : 2 */

#define DCORSEL_0           (0x0000u)    /* DCO RSEL 0 */
#define DCORSEL_1           (0x0010u)    /* DCO RSEL 1 */
#define DCORSEL_2           (0x0020u)    /* DCO RSEL 2 */
#define DCORSEL_3           (0x0030u)    /* DCO RSEL 3 */
#define DCORSEL_4           (0x0040u)    /* DCO RSEL 4 */
#define DCORSEL_5           (0x0050u)    /* DCO RSEL 5 */
#define DCORSEL_6           (0x0060u)    /* DCO RSEL 6 */
#define DCORSEL_7           (0x0070u)    /* DCO RSEL 7 */

/* UCSCTL2 Control Bits */
#define FLLN0_L             (0x0001u)    /* FLL Multipier Bit : 0 */
#define FLLN1_L             (0x0002u)    /* FLL Multipier Bit : 1 */
#define FLLN2_L             (0x0004u)    /* FLL Multipier Bit : 2 */
#define FLLN3_L             (0x0008u)    /* FLL Multipier Bit : 3 */
#define FLLN4_L             (0x0010u)    /* FLL Multipier Bit : 4 */
#define FLLN5_L             (0x0020u)    /* FLL Multipier Bit : 5 */
#define FLLN6_L             (0x0040u)    /* FLL Multipier Bit : 6 */
#define FLLN7_L             (0x0080u)    /* FLL Multipier Bit : 7 */

/* UCSCTL2 Control Bits */
#define FLLN8_H             (0x0001u)    /* FLL Multipier Bit : 8 */
#define FLLN9_H             (0x0002u)    /* FLL Multipier Bit : 9 */
#define FLLD0_H             (0x0010u)    /* Loop Divider Bit : 0 */
#define FLLD1_H             (0x0020u)    /* Loop Divider Bit : 1 */
#define FLLD2_H             (0x0040u)    /* Loop Divider Bit : 1 */

#define FLLD_0             (0x0000u)    /* Multiply Selected Loop Freq. 1 */
#define FLLD_1             (0x1000u)    /* Multiply Selected Loop Freq. 2 */
#define FLLD_2             (0x2000u)    /* Multiply Selected Loop Freq. 4 */
#define FLLD_3             (0x3000u)    /* Multiply Selected Loop Freq. 8 */
#define FLLD_4             (0x4000u)    /* Multiply Selected Loop Freq. 16 */
#define FLLD_5             (0x5000u)    /* Multiply Selected Loop Freq. 32 */
#define FLLD_6             (0x6000u)    /* Multiply Selected Loop Freq. 32 */
#define FLLD_7             (0x7000u)    /* Multiply Selected Loop Freq. 32 */
#define FLLD__1            (0x0000u)    /* Multiply Selected Loop Freq. By 1 */
#define FLLD__2            (0x1000u)    /* Multiply Selected Loop Freq. By 2 */
#define FLLD__4            (0x2000u)    /* Multiply Selected Loop Freq. By 4 */
#define FLLD__8            (0x3000u)    /* Multiply Selected Loop Freq. By 8 */
#define FLLD__16           (0x4000u)    /* Multiply Selected Loop Freq. By 16 */
#define FLLD__32           (0x5000u)    /* Multiply Selected Loop Freq. By 32 */

/* UCSCTL3 Control Bits */
#define FLLREFDIV0_L        (0x0001u)    /* Reference Divider Bit : 0 */
#define FLLREFDIV1_L        (0x0002u)    /* Reference Divider Bit : 1 */
#define FLLREFDIV2_L        (0x0004u)    /* Reference Divider Bit : 2 */
#define SELREF0_L           (0x0010u)    /* FLL Reference Clock Select Bit : 0 */
#define SELREF1_L           (0x0020u)    /* FLL Reference Clock Select Bit : 1 */
#define SELREF2_L           (0x0040u)    /* FLL Reference Clock Select Bit : 2 */

#define FLLREFDIV_0         (0x0000u)    /* Reference Divider: f(LFCLK)/1 */
#define FLLREFDIV_1         (0x0001u)    /* Reference Divider: f(LFCLK)/2 */
#define FLLREFDIV_2         (0x0002u)    /* Reference Divider: f(LFCLK)/4 */
#define FLLREFDIV_3         (0x0003u)    /* Reference Divider: f(LFCLK)/8 */
#define FLLREFDIV_4         (0x0004u)    /* Reference Divider: f(LFCLK)/12 */
#define FLLREFDIV_5         (0x0005u)    /* Reference Divider: f(LFCLK)/16 */
#define FLLREFDIV_6         (0x0006u)    /* Reference Divider: f(LFCLK)/16 */
#define FLLREFDIV_7         (0x0007u)    /* Reference Divider: f(LFCLK)/16 */
#define FLLREFDIV__1        (0x0000u)    /* Reference Divider: f(LFCLK)/1 */
#define FLLREFDIV__2        (0x0001u)    /* Reference Divider: f(LFCLK)/2 */
#define FLLREFDIV__4        (0x0002u)    /* Reference Divider: f(LFCLK)/4 */
#define FLLREFDIV__8        (0x0003u)    /* Reference Divider: f(LFCLK)/8 */
#define FLLREFDIV__12       (0x0004u)    /* Reference Divider: f(LFCLK)/12 */
#define FLLREFDIV__16       (0x0005u)    /* Reference Divider: f(LFCLK)/16 */
#define SELREF_0            (0x0000u)    /* FLL Reference Clock Select 0 */
#define SELREF_1            (0x0010u)    /* FLL Reference Clock Select 1 */
#define SELREF_2            (0x0020u)    /* FLL Reference Clock Select 2 */
#define SELREF_3            (0x0030u)    /* FLL Reference Clock Select 3 */
#define SELREF_4            (0x0040u)    /* FLL Reference Clock Select 4 */
#define SELREF_5            (0x0050u)    /* FLL Reference Clock Select 5 */
#define SELREF_6            (0x0060u)    /* FLL Reference Clock Select 6 */
#define SELREF_7            (0x0070u)    /* FLL Reference Clock Select 7 */
#define SELREF__XT1CLK      (0x0000u)    /* Multiply Selected Loop Freq. By XT1CLK */
#define SELREF__REFOCLK     (0x0020u)    /* Multiply Selected Loop Freq. By REFOCLK */

/* UCSCTL4 Control Bits */
#define SELM0_L             (0x0001u)   /* MCLK Source Select Bit: 0 */
#define SELM1_L             (0x0002u)   /* MCLK Source Select Bit: 1 */
#define SELM2_L             (0x0004u)   /* MCLK Source Select Bit: 2 */
#define SELS0_L             (0x0010u)   /* SMCLK Source Select Bit: 0 */
#define SELS1_L             (0x0020u)   /* SMCLK Source Select Bit: 1 */
#define SELS2_L             (0x0040u)   /* SMCLK Source Select Bit: 2 */
#define SELA0_H             (0x0001u)   /* ACLK Source Select Bit: 0 */
#define SELA1_H             (0x0002u)   /* ACLK Source Select Bit: 1 */
#define SELA2_H             (0x0004u)   /* ACLK Source Select Bit: 2 */

#define SELM_0              (0x0000u)   /* MCLK Source Select 0 */
#define SELM_1              (0x0001u)   /* MCLK Source Select 1 */
#define SELM_2              (0x0002u)   /* MCLK Source Select 2 */
#define SELM_3              (0x0003u)   /* MCLK Source Select 3 */
#define SELM_4              (0x0004u)   /* MCLK Source Select 4 */
#define SELM_5              (0x0005u)   /* MCLK Source Select 5 */
#define SELM_6              (0x0006u)   /* MCLK Source Select 6 */
#define SELM_7              (0x0007u)   /* MCLK Source Select 7 */
#define SELM__XT1CLK        (0x0000u)   /* MCLK Source Select XT1CLK */
#define SELM__VLOCLK        (0x0001u)   /* MCLK Source Select VLOCLK */
#define SELM__REFOCLK       (0x0002u)   /* MCLK Source Select REFOCLK */
#define SELM__DCOCLK        (0x0003u)   /* MCLK Source Select DCOCLK */
#define SELM__DCOCLKDIV     (0x0004u)   /* MCLK Source Select DCOCLKDIV */

#define SELS_0              (0x0000u)   /* SMCLK Source Select 0 */
#define SELS_1              (0x0010u)   /* SMCLK Source Select 1 */
#define SELS_2              (0x0020u)   /* SMCLK Source Select 2 */
#define SELS_3              (0x0030u)   /* SMCLK Source Select 3 */
#define SELS_4              (0x0040u)   /* SMCLK Source Select 4 */
#define SELS_5              (0x0050u)   /* SMCLK Source Select 5 */
#define SELS_6              (0x0060u)   /* SMCLK Source Select 6 */
#define SELS_7              (0x0070u)   /* SMCLK Source Select 7 */
#define SELS__XT1CLK        (0x0000u)   /* SMCLK Source Select XT1CLK */
#define SELS__VLOCLK        (0x0010u)   /* SMCLK Source Select VLOCLK */
#define SELS__REFOCLK       (0x0020u)   /* SMCLK Source Select REFOCLK */
#define SELS__DCOCLK        (0x0030u)   /* SMCLK Source Select DCOCLK */
#define SELS__DCOCLKDIV     (0x0040u)   /* SMCLK Source Select DCOCLKDIV */

#define SELA_0              (0x0000u)   /* ACLK Source Select 0 */
#define SELA_1              (0x0100u)   /* ACLK Source Select 1 */
#define SELA_2              (0x0200u)   /* ACLK Source Select 2 */
#define SELA_3              (0x0300u)   /* ACLK Source Select 3 */
#define SELA_4              (0x0400u)   /* ACLK Source Select 4 */
#define SELA_5              (0x0500u)   /* ACLK Source Select 5 */
#define SELA_6              (0x0600u)   /* ACLK Source Select 6 */
#define SELA_7              (0x0700u)   /* ACLK Source Select 7 */
#define SELA__XT1CLK        (0x0000u)   /* ACLK Source Select XT1CLK */
#define SELA__VLOCLK        (0x0100u)   /* ACLK Source Select VLOCLK */
#define SELA__REFOCLK       (0x0200u)   /* ACLK Source Select REFOCLK */
#define SELA__DCOCLK        (0x0300u)   /* ACLK Source Select DCOCLK */
#define SELA__DCOCLKDIV     (0x0400u)   /* ACLK Source Select DCOCLKDIV */

/* UCSCTL5 Control Bits */
#define DIVM0_L             (0x0001u)   /* MCLK Divider Bit: 0 */
#define DIVM1_L             (0x0002u)   /* MCLK Divider Bit: 1 */
#define DIVM2_L             (0x0004u)   /* MCLK Divider Bit: 2 */
#define DIVS0_L             (0x0010u)   /* SMCLK Divider Bit: 0 */
#define DIVS1_L             (0x0020u)   /* SMCLK Divider Bit: 1 */
#define DIVS2_L             (0x0040u)   /* SMCLK Divider Bit: 2 */
#define DIVA0_H             (0x0001u)   /* ACLK Divider Bit: 0 */
#define DIVA1_H             (0x0002u)   /* ACLK Divider Bit: 1 */
#define DIVA2_H             (0x0004u)   /* ACLK Divider Bit: 2 */
#define DIVPA0_H            (0x0010u)   /* ACLK from Pin Divider Bit: 0 */
#define DIVPA1_H            (0x0020u)   /* ACLK from Pin Divider Bit: 1 */
#define DIVPA2_H            (0x0040u)   /* ACLK from Pin Divider Bit: 2 */

#define DIVM_0              (0x0000u)    /* MCLK Source Divider 0 */
#define DIVM_1              (0x0001u)    /* MCLK Source Divider 1 */
#define DIVM_2              (0x0002u)    /* MCLK Source Divider 2 */
#define DIVM_3              (0x0003u)    /* MCLK Source Divider 3 */
#define DIVM_4              (0x0004u)    /* MCLK Source Divider 4 */
#define DIVM_5              (0x0005u)    /* MCLK Source Divider 5 */
#define DIVM_6              (0x0006u)    /* MCLK Source Divider 6 */
#define DIVM_7              (0x0007u)    /* MCLK Source Divider 7 */
#define DIVM__1             (0x0000u)    /* MCLK Source Divider f(MCLK)/1 */
#define DIVM__2             (0x0001u)    /* MCLK Source Divider f(MCLK)/2 */
#define DIVM__4             (0x0002u)    /* MCLK Source Divider f(MCLK)/4 */
#define DIVM__8             (0x0003u)    /* MCLK Source Divider f(MCLK)/8 */
#define DIVM__16            (0x0004u)    /* MCLK Source Divider f(MCLK)/16 */
#define DIVM__32            (0x0005u)    /* MCLK Source Divider f(MCLK)/32 */

#define DIVS_0              (0x0000u)    /* SMCLK Source Divider 0 */
#define DIVS_1              (0x0010u)    /* SMCLK Source Divider 1 */
#define DIVS_2              (0x0020u)    /* SMCLK Source Divider 2 */
#define DIVS_3              (0x0030u)    /* SMCLK Source Divider 3 */
#define DIVS_4              (0x0040u)    /* SMCLK Source Divider 4 */
#define DIVS_5              (0x0050u)    /* SMCLK Source Divider 5 */
#define DIVS_6              (0x0060u)    /* SMCLK Source Divider 6 */
#define DIVS_7              (0x0070u)    /* SMCLK Source Divider 7 */
#define DIVS__1             (0x0000u)    /* SMCLK Source Divider f(SMCLK)/1 */
#define DIVS__2             (0x0010u)    /* SMCLK Source Divider f(SMCLK)/2 */
#define DIVS__4             (0x0020u)    /* SMCLK Source Divider f(SMCLK)/4 */
#define DIVS__8             (0x0030u)    /* SMCLK Source Divider f(SMCLK)/8 */
#define DIVS__16            (0x0040u)    /* SMCLK Source Divider f(SMCLK)/16 */
#define DIVS__32            (0x0050u)    /* SMCLK Source Divider f(SMCLK)/32 */

#define DIVA_0              (0x0000u)    /* ACLK Source Divider 0 */
#define DIVA_1              (0x0100u)    /* ACLK Source Divider 1 */
#define DIVA_2              (0x0200u)    /* ACLK Source Divider 2 */
#define DIVA_3              (0x0300u)    /* ACLK Source Divider 3 */
#define DIVA_4              (0x0400u)    /* ACLK Source Divider 4 */
#define DIVA_5              (0x0500u)    /* ACLK Source Divider 5 */
#define DIVA_6              (0x0600u)    /* ACLK Source Divider 6 */
#define DIVA_7              (0x0700u)    /* ACLK Source Divider 7 */
#define DIVA__1             (0x0000u)    /* ACLK Source Divider f(ACLK)/1 */
#define DIVA__2             (0x0100u)    /* ACLK Source Divider f(ACLK)/2 */
#define DIVA__4             (0x0200u)    /* ACLK Source Divider f(ACLK)/4 */
#define DIVA__8             (0x0300u)    /* ACLK Source Divider f(ACLK)/8 */
#define DIVA__16            (0x0400u)    /* ACLK Source Divider f(ACLK)/16 */
#define DIVA__32            (0x0500u)    /* ACLK Source Divider f(ACLK)/32 */

#define DIVPA_0             (0x0000u)    /* ACLK from Pin Source Divider 0 */
#define DIVPA_1             (0x1000u)    /* ACLK from Pin Source Divider 1 */
#define DIVPA_2             (0x2000u)    /* ACLK from Pin Source Divider 2 */
#define DIVPA_3             (0x3000u)    /* ACLK from Pin Source Divider 3 */
#define DIVPA_4             (0x4000u)    /* ACLK from Pin Source Divider 4 */
#define DIVPA_5             (0x5000u)    /* ACLK from Pin Source Divider 5 */
#define DIVPA_6             (0x6000u)    /* ACLK from Pin Source Divider 6 */
#define DIVPA_7             (0x7000u)    /* ACLK from Pin Source Divider 7 */
#define DIVPA__1            (0x0000u)    /* ACLK from Pin Source Divider f(ACLK)/1 */
#define DIVPA__2            (0x1000u)    /* ACLK from Pin Source Divider f(ACLK)/2 */
#define DIVPA__4            (0x2000u)    /* ACLK from Pin Source Divider f(ACLK)/4 */
#define DIVPA__8            (0x3000u)    /* ACLK from Pin Source Divider f(ACLK)/8 */
#define DIVPA__16           (0x4000u)    /* ACLK from Pin Source Divider f(ACLK)/16 */
#define DIVPA__32           (0x5000u)    /* ACLK from Pin Source Divider f(ACLK)/32 */

/* UCSCTL6 Control Bits */
#define XT1OFF_L            (0x0001u)    /* High Frequency Oscillator 1 (XT1) disable */
#define SMCLKOFF_L          (0x0002u)    /* SMCLK Off */
#define XCAP0_L             (0x0004u)   /* XIN/XOUT Cap Bit: 0 */
#define XCAP1_L             (0x0008u)   /* XIN/XOUT Cap Bit: 1 */
#define XT1BYPASS_L         (0x0010u)    /* XT1 bypass mode : 0: internal 1:sourced from external pin */
#define XTS_L               (0x0020u)   /* 1: Selects high-freq. oscillator */
#define XT1DRIVE0_L         (0x0040u)    /* XT1 Drive Level mode Bit 0 */
#define XT1DRIVE1_L         (0x0080u)    /* XT1 Drive Level mode Bit 1 */

/* UCSCTL6 Control Bits */
#define XT2OFF_H            (0x0001u)    /* High Frequency Oscillator 2 (XT2) disable */

#define XCAP_0              (0x0000u)    /* XIN/XOUT Cap 0 */
#define XCAP_1              (0x0004u)    /* XIN/XOUT Cap 1 */
#define XCAP_2              (0x0008u)    /* XIN/XOUT Cap 2 */
#define XCAP_3              (0x000Cu)    /* XIN/XOUT Cap 3 */
#define XT1DRIVE_0          (0x0000u)    /* XT1 Drive Level mode: 0 */
#define XT1DRIVE_1          (0x0040u)    /* XT1 Drive Level mode: 1 */
#define XT1DRIVE_2          (0x0080u)    /* XT1 Drive Level mode: 2 */
#define XT1DRIVE_3          (0x00C0u)    /* XT1 Drive Level mode: 3 */

/* UCSCTL7 Control Bits */
#define DCOFFG_L            (0x0001u)    /* DCO Fault Flag */
#define XT1LFOFFG_L         (0x0002u)    /* XT1 Low Frequency Oscillator Fault Flag */
#define XT1HFOFFG_L         (0x0004u)    /* XT1 High Frequency Oscillator 1 Fault Flag */

/* UCSCTL8 Control Bits */
#define ACLKREQEN_L         (0x0001u)    /* ACLK Clock Request Enable */
#define MCLKREQEN_L         (0x0002u)    /* MCLK Clock Request Enable */
#define SMCLKREQEN_L        (0x0004u)    /* SMCLK Clock Request Enable */
#define MODOSCREQEN_L       (0x0008u)    /* MODOSC Clock Request Enable */

/*-------------------------------------------------------------------------
 *   USCI_A0  UART Mode
 *-------------------------------------------------------------------------*/


  /* USCI A0 Control Word Register 0 */
__no_init volatile unsigned short UCA0CTLW0 @ 0x05C0;


__no_init volatile union
{
  unsigned char UCA0CTL0;   /*  */

  struct
  {
    unsigned char UCSYNC          : 1; /* Sync-Mode  0:UART-Mode / 1:SPI-Mode */
    unsigned char UCMODE0         : 1; /* Async. Mode: USCI Mode 0 */
    unsigned char UCMODE1         : 1; /* Async. Mode: USCI Mode 1 */
    unsigned char UCSPB           : 1; /* Async. Mode: Stop Bits  0:one / 1: two */
    unsigned char UC7BIT          : 1; /* Async. Mode: Data Bits  0:8-bits / 1:7-bits */
    unsigned char UCMSB           : 1; /* Async. Mode: MSB first  0:LSB / 1:MSB */
    unsigned char UCPAR           : 1; /* Async. Mode: Parity     0:odd / 1:even */
    unsigned char UCPEN           : 1; /* Async. Mode: Parity enable */
  }UCA0CTL0_bit;
} @0x05C1;


enum {
  UCSYNC          = 0x0001,
  UCMODE0         = 0x0002,
  UCMODE1         = 0x0004,
  UCSPB           = 0x0008,
  UC7BIT          = 0x0010,
  UCMSB           = 0x0020,
  UCPAR           = 0x0040,
  UCPEN           = 0x0080
};


__no_init volatile union
{
  unsigned char UCA0CTL1;   /*  */

  struct
  {
    unsigned char UCSWRST         : 1; /* USCI Software Reset */
    unsigned char UCTXBRK         : 1; /* Send next Data as Break */
    unsigned char UCTXADDR        : 1; /* Send next Data as Address */
    unsigned char UCDORM          : 1; /* Dormant (Sleep) Mode */
    unsigned char UCBRKIE         : 1; /* Break interrupt enable */
    unsigned char UCRXEIE         : 1; /* RX Error interrupt enable */
    unsigned char UCSSEL0         : 1; /* USCI 0 Clock Source Select 0 */
    unsigned char UCSSEL1         : 1; /* USCI 0 Clock Source Select 1 */
  }UCA0CTL1_bit;
} @0x05C0;


enum {
  UCSWRST         = 0x0001,
  UCTXBRK         = 0x0002,
  UCTXADDR        = 0x0004,
  UCDORM          = 0x0008,
  UCBRKIE         = 0x0010,
  UCRXEIE         = 0x0020,
  UCSSEL0         = 0x0040,
  UCSSEL1         = 0x0080
};


  /* USCI A0 Baud Word Rate 0 */
__no_init volatile unsigned short UCA0BRW @ 0x05C6;


  /*  */
__no_init volatile unsigned char UCA0BR0 @ 0x05C6;


  /*  */
__no_init volatile unsigned char UCA0BR1 @ 0x05C7;


__no_init volatile union
{
  unsigned char UCA0MCTL;   /* USCI A0 Modulation Control */

  struct
  {
    unsigned char UCOS16          : 1; /* USCI 16-times Oversampling enable */
    unsigned char UCBRS0          : 1; /* USCI Second Stage Modulation Select 0 */
    unsigned char UCBRS1          : 1; /* USCI Second Stage Modulation Select 1 */
    unsigned char UCBRS2          : 1; /* USCI Second Stage Modulation Select 2 */
    unsigned char UCBRF0          : 1; /* USCI First Stage Modulation Select 0 */
    unsigned char UCBRF1          : 1; /* USCI First Stage Modulation Select 1 */
    unsigned char UCBRF2          : 1; /* USCI First Stage Modulation Select 2 */
    unsigned char UCBRF3          : 1; /* USCI First Stage Modulation Select 3 */
  }UCA0MCTL_bit;
} @0x05C8;


enum {
  UCOS16          = 0x0001,
  UCBRS0          = 0x0002,
  UCBRS1          = 0x0004,
  UCBRS2          = 0x0008,
  UCBRF0          = 0x0010,
  UCBRF1          = 0x0020,
  UCBRF2          = 0x0040,
  UCBRF3          = 0x0080
};


__no_init volatile union
{
  unsigned char UCA0STAT;   /* USCI A0 Status Register */

  struct
  {
    unsigned char UCBUSY          : 1; /* USCI Busy Flag */
    unsigned char UCADDR          : 1; /* USCI Address received Flag */
    unsigned char UCRXERR         : 1; /* USCI RX Error Flag */
    unsigned char UCBRK           : 1; /* USCI Break received */
    unsigned char UCPE            : 1; /* USCI Parity Error Flag */
    unsigned char UCOE            : 1; /* USCI Overrun Error Flag */
    unsigned char UCFE            : 1; /* USCI Frame Error Flag */
    unsigned char UCLISTEN        : 1; /* USCI Listen mode */
  }UCA0STAT_bit;
} @0x05CA;


enum {
  UCBUSY          = 0x0001,
  UCADDR          = 0x0002,
  UCRXERR         = 0x0004,
  UCBRK           = 0x0008,
  UCPE            = 0x0010,
  UCOE            = 0x0020,
  UCFE            = 0x0040,
  UCLISTEN        = 0x0080
};


  /* USCI A0 Receive Buffer */
__no_init volatile unsigned __READ char UCA0RXBUF @ 0x05CC;


  /* USCI A0 Transmit Buffer */
__no_init volatile unsigned char UCA0TXBUF @ 0x05CE;


__no_init volatile union
{
  unsigned char UCA0ABCTL;   /* USCI A0 LIN Control */

  struct
  {
    unsigned char UCABDEN         : 1; /* Auto Baud Rate detect enable */
    unsigned char                : 1;
    unsigned char UCBTOE          : 1; /* Break Timeout error */
    unsigned char UCSTOE          : 1; /* Sync-Field Timeout error */
    unsigned char UCDELIM0        : 1; /* Break Sync Delimiter 0 */
    unsigned char UCDELIM1        : 1; /* Break Sync Delimiter 1 */
  }UCA0ABCTL_bit;
} @0x05D0;


enum {
  UCABDEN         = 0x0001,
  UCBTOE          = 0x0004,
  UCSTOE          = 0x0008,
  UCDELIM0        = 0x0010,
  UCDELIM1        = 0x0020
};


  /* USCI A0 IrDA Transmit Control */
__no_init volatile unsigned short UCA0IRCTL @ 0x05D2;


__no_init volatile union
{
  unsigned char UCA0IRTCTL;   /*  */

  struct
  {
    unsigned char UCIREN          : 1; /* IRDA Encoder/Decoder enable */
    unsigned char UCIRTXCLK       : 1; /* IRDA Transmit Pulse Clock Select */
    unsigned char UCIRTXPL0       : 1; /* IRDA Transmit Pulse Length 0 */
    unsigned char UCIRTXPL1       : 1; /* IRDA Transmit Pulse Length 1 */
    unsigned char UCIRTXPL2       : 1; /* IRDA Transmit Pulse Length 2 */
    unsigned char UCIRTXPL3       : 1; /* IRDA Transmit Pulse Length 3 */
    unsigned char UCIRTXPL4       : 1; /* IRDA Transmit Pulse Length 4 */
    unsigned char UCIRTXPL5       : 1; /* IRDA Transmit Pulse Length 5 */
  }UCA0IRTCTL_bit;
} @0x05D2;


enum {
  UCIREN          = 0x0001,
  UCIRTXCLK       = 0x0002,
  UCIRTXPL0       = 0x0004,
  UCIRTXPL1       = 0x0008,
  UCIRTXPL2       = 0x0010,
  UCIRTXPL3       = 0x0020,
  UCIRTXPL4       = 0x0040,
  UCIRTXPL5       = 0x0080
};


__no_init volatile union
{
  unsigned char UCA0IRRCTL;   /*  */

  struct
  {
    unsigned char UCIRRXFE        : 1; /* IRDA Receive Filter enable */
    unsigned char UCIRRXPL        : 1; /* IRDA Receive Input Polarity */
    unsigned char UCIRRXFL0       : 1; /* IRDA Receive Filter Length 0 */
    unsigned char UCIRRXFL1       : 1; /* IRDA Receive Filter Length 1 */
    unsigned char UCIRRXFL2       : 1; /* IRDA Receive Filter Length 2 */
    unsigned char UCIRRXFL3       : 1; /* IRDA Receive Filter Length 3 */
    unsigned char UCIRRXFL4       : 1; /* IRDA Receive Filter Length 4 */
    unsigned char UCIRRXFL5       : 1; /* IRDA Receive Filter Length 5 */
  }UCA0IRRCTL_bit;
} @0x05D3;


enum {
  UCIRRXFE        = 0x0001,
  UCIRRXPL        = 0x0002,
  UCIRRXFL0       = 0x0004,
  UCIRRXFL1       = 0x0008,
  UCIRRXFL2       = 0x0010,
  UCIRRXFL3       = 0x0020,
  UCIRRXFL4       = 0x0040,
  UCIRRXFL5       = 0x0080
};


  /* USCI A0 Interrupt Enable Register */
__no_init volatile unsigned short UCA0ICTL @ 0x05DC;


__no_init volatile union
{
  unsigned char UCA0IE;   /*  */

  struct
  {
    unsigned char UCRXIE          : 1; /* USCI Receive Interrupt Enable */
    unsigned char UCTXIE          : 1; /* USCI Transmit Interrupt Enable */
  }UCA0IE_bit;
} @0x05DC;


enum {
  UCRXIE          = 0x0001,
  UCTXIE          = 0x0002
};


__no_init volatile union
{
  unsigned char UCA0IFG;   /*  */

  struct
  {
    unsigned char UCRXIFG         : 1; /* USCI Receive Interrupt Flag */
    unsigned char UCTXIFG         : 1; /* USCI Transmit Interrupt Flag */
  }UCA0IFG_bit;
} @0x05DD;


enum {
  UCRXIFG         = 0x0001,
  UCTXIFG         = 0x0002
};


  /* USCI A0 Interrupt Vector Register */
__no_init volatile unsigned short UCA0IV @ 0x05DE;


#define __MSP430_HAS_USCI_A0__       /* Definition to show that Module is available */

#define UCA0CTL1            UCA0CTLW0_L  /* USCI A0 Control Register 1 */
#define UCA0CTL0            UCA0CTLW0_H  /* USCI A0 Control Register 0 */
#define UCA0BR0             UCA0BRW_L /* USCI A0 Baud Rate 0 */
#define UCA0BR1             UCA0BRW_H /* USCI A0 Baud Rate 1 */
#define UCA0IRTCTL          UCA0IRCTL_L  /* USCI A0 IrDA Transmit Control */
#define UCA0IRRCTL          UCA0IRCTL_H  /* USCI A0 IrDA Receive Control */
#define UCA0IE              UCA0ICTL_L  /* USCI A0 Interrupt Enable Register */
#define UCA0IFG             UCA0ICTL_H  /* USCI A0 Interrupt Flags Register */

/*-------------------------------------------------------------------------
 *   USCI_A0  SPI Mode
 *-------------------------------------------------------------------------*/


  /*  */
__no_init volatile unsigned short UCA0CTLW0__SPI @ 0x05C0;


__no_init volatile union
{
  unsigned char UCA0CTL0__SPI;   /*  */

  struct
  {
    unsigned char UCSYNC          : 1; /* Sync-Mode  0:UART-Mode / 1:SPI-Mode */
    unsigned char UCMODE0         : 1; /* Async. Mode: USCI Mode 0 */
    unsigned char UCMODE1         : 1; /* Async. Mode: USCI Mode 1 */
    unsigned char UCMST           : 1; /* Sync. Mode: Master Select */
    unsigned char UC7BIT          : 1; /* Async. Mode: Data Bits  0:8-bits / 1:7-bits */
    unsigned char UCMSB           : 1; /* Async. Mode: MSB first  0:LSB / 1:MSB */
    unsigned char UCCKPL          : 1; /* Sync. Mode: Clock Polarity */
    unsigned char UCCKPH          : 1; /* Sync. Mode: Clock Phase */
  }UCA0CTL0__SPI_bit;
} @0x05C1;


enum {
/*  UCSYNC          = 0x0001, */
/*  UCMODE0         = 0x0002, */
/*  UCMODE1         = 0x0004, */
  UCMST           = 0x0008,
/*  UC7BIT          = 0x0010, */
/*  UCMSB           = 0x0020, */
  UCCKPL          = 0x0040,
  UCCKPH          = 0x0080
};


__no_init volatile union
{
  unsigned char UCA0CTL1__SPI;   /*  */

  struct
  {
    unsigned char UCSWRST         : 1; /* USCI Software Reset */
    unsigned char                : 5;
    unsigned char UCSSEL0         : 1; /* USCI 0 Clock Source Select 0 */
    unsigned char UCSSEL1         : 1; /* USCI 0 Clock Source Select 1 */
  }UCA0CTL1__SPI_bit;
} @0x05C0;


/*
enum {
  UCSWRST         = 0x0001,
  UCSSEL0         = 0x0040,
  UCSSEL1         = 0x0080,
};
*/


  /*  */
__no_init volatile unsigned short UCA0BRW__SPI @ 0x05C6;


  /*  */
__no_init volatile unsigned char UCA0BR0__SPI @ 0x05C6;


  /*  */
__no_init volatile unsigned char UCA0BR1__SPI @ 0x05C7;


  /*  */
__no_init volatile unsigned char UCA0MCTL__SPI @ 0x05C8;


__no_init volatile union
{
  unsigned char UCA0STAT__SPI;   /*  */

  struct
  {
    unsigned char UCBUSY          : 1; /* USCI Busy Flag */
    unsigned char                : 4;
    unsigned char UCOE            : 1; /* USCI Overrun Error Flag */
    unsigned char UCFE            : 1; /* USCI Frame Error Flag */
    unsigned char UCLISTEN        : 1; /* USCI Listen mode */
  }UCA0STAT__SPI_bit;
} @0x05CA;


/*
enum {
  UCBUSY          = 0x0001,
  UCOE            = 0x0020,
  UCFE            = 0x0040,
  UCLISTEN        = 0x0080,
};
*/


  /*  */
__no_init volatile unsigned char UCA0RXBUF__SPI @ 0x05CC;


  /*  */
__no_init volatile unsigned char UCA0TXBUF__SPI @ 0x05CE;


/*-------------------------------------------------------------------------
 *   USCI_B0  SPI Mode
 *-------------------------------------------------------------------------*/


  /*  */
__no_init volatile unsigned short UCB0CTLW0__SPI @ 0x05E0;


__no_init volatile union
{
  unsigned char UCB0CTL0__SPI;   /*  */

  struct
  {
    unsigned char UCSYNC          : 1; /* Sync-Mode  0:UART-Mode / 1:SPI-Mode */
    unsigned char UCMODE0         : 1; /* Async. Mode: USCI Mode 0 */
    unsigned char UCMODE1         : 1; /* Async. Mode: USCI Mode 1 */
    unsigned char UCMST           : 1; /* Sync. Mode: Master Select */
    unsigned char UC7BIT          : 1; /* Async. Mode: Data Bits  0:8-bits / 1:7-bits */
    unsigned char UCMSB           : 1; /* Async. Mode: MSB first  0:LSB / 1:MSB */
    unsigned char UCCKPL          : 1; /* Sync. Mode: Clock Polarity */
    unsigned char UCCKPH          : 1; /* Sync. Mode: Clock Phase */
  }UCB0CTL0__SPI_bit;
} @0x05E1;


/*
enum {
  UCSYNC          = 0x0001,
  UCMODE0         = 0x0002,
  UCMODE1         = 0x0004,
  UCMST           = 0x0008,
  UC7BIT          = 0x0010,
  UCMSB           = 0x0020,
  UCCKPL          = 0x0040,
  UCCKPH          = 0x0080,
};
*/


__no_init volatile union
{
  unsigned char UCB0CTL1__SPI;   /*  */

  struct
  {
    unsigned char UCSWRST         : 1; /* USCI Software Reset */
    unsigned char                : 5;
    unsigned char UCSSEL0         : 1; /* USCI 0 Clock Source Select 0 */
    unsigned char UCSSEL1         : 1; /* USCI 0 Clock Source Select 1 */
  }UCB0CTL1__SPI_bit;
} @0x05E0;


/*
enum {
  UCSWRST         = 0x0001,
  UCSSEL0         = 0x0040,
  UCSSEL1         = 0x0080,
};
*/


  /*  */
__no_init volatile unsigned short UCB0BRW__SPI @ 0x05E6;


  /*  */
__no_init volatile unsigned char UCB0BR0__SPI @ 0x05E6;


  /*  */
__no_init volatile unsigned char UCB0BR1__SPI @ 0x05E7;


__no_init volatile union
{
  unsigned char UCB0STAT__SPI;   /*  */

  struct
  {
    unsigned char UCBUSY          : 1; /* USCI Busy Flag */
    unsigned char                : 4;
    unsigned char UCOE            : 1; /* USCI Overrun Error Flag */
    unsigned char UCFE            : 1; /* USCI Frame Error Flag */
    unsigned char UCLISTEN        : 1; /* USCI Listen mode */
  }UCB0STAT__SPI_bit;
} @0x05EA;


/*
enum {
  UCBUSY          = 0x0001,
  UCOE            = 0x0020,
  UCFE            = 0x0040,
  UCLISTEN        = 0x0080,
};
*/


  /*  */
__no_init volatile unsigned char UCB0RXBUF__SPI @ 0x05EC;


  /*  */
__no_init volatile unsigned char UCB0TXBUF__SPI @ 0x05EE;


  /* USCI B0 Interrupt Enable Register */
__no_init volatile unsigned short UCB0ICTL @ 0x05FC;


__no_init volatile union
{
  unsigned char UCB0IE;   /*  */

  struct
  {
    unsigned char UCRXIE          : 1; /* USCI Receive Interrupt Enable */
    unsigned char UCTXIE          : 1; /* USCI Transmit Interrupt Enable */
    unsigned char UCSTTIE         : 1; /* START Condition interrupt enable */
    unsigned char UCSTPIE         : 1; /* STOP Condition interrupt enable */
    unsigned char UCALIE          : 1; /* Arbitration Lost interrupt enable */
    unsigned char UCNACKIE        : 1; /* NACK Condition interrupt enable */
  }UCB0IE_bit;
} @0x05FC;


enum {
/*  UCRXIE          = 0x0001, */
/*  UCTXIE          = 0x0002, */
  UCSTTIE         = 0x0004,
  UCSTPIE         = 0x0008,
  UCALIE          = 0x0010,
  UCNACKIE        = 0x0020
};


__no_init volatile union
{
  unsigned char UCB0IFG;   /*  */

  struct
  {
    unsigned char UCRXIFG         : 1; /* USCI Receive Interrupt Flag */
    unsigned char UCTXIFG         : 1; /* USCI Transmit Interrupt Flag */
    unsigned char UCSTTIFG        : 1; /* START Condition interrupt Flag */
    unsigned char UCSTPIFG        : 1; /* STOP Condition interrupt Flag */
    unsigned char UCALIFG         : 1; /* Arbitration Lost interrupt Flag */
    unsigned char UCNACKIFG       : 1; /* NAK Condition interrupt Flag */
  }UCB0IFG_bit;
} @0x05FD;


enum {
/*  UCRXIFG         = 0x0001, */
/*  UCTXIFG         = 0x0002, */
  UCSTTIFG        = 0x0004,
  UCSTPIFG        = 0x0008,
  UCALIFG         = 0x0010,
  UCNACKIFG       = 0x0020
};


  /* USCI B0 Interrupt Vector Register */
__no_init volatile unsigned short UCB0IV @ 0x05FE;


/*-------------------------------------------------------------------------
 *   USCI_B0  I2C Mode
 *-------------------------------------------------------------------------*/


  /* USCI B0 Control Word Register 0 */
__no_init volatile unsigned short UCB0CTLW0 @ 0x05E0;


__no_init volatile union
{
  unsigned char UCB0CTL0;   /*  */

  struct
  {
    unsigned char UCSYNC          : 1; /* Sync-Mode  0:UART-Mode / 1:SPI-Mode */
    unsigned char UCMODE0         : 1; /* Async. Mode: USCI Mode 0 */
    unsigned char UCMODE1         : 1; /* Async. Mode: USCI Mode 1 */
    unsigned char UCMST           : 1; /* Sync. Mode: Master Select */
    unsigned char                : 1;
    unsigned char UCMM            : 1; /* Multi-Master Environment */
    unsigned char UCSLA10         : 1; /* 10-bit Slave Address Mode */
    unsigned char UCA10           : 1; /* 10-bit Address Mode */
  }UCB0CTL0_bit;
} @0x05E1;


enum {
/*  UCSYNC          = 0x0001, */
/*  UCMODE0         = 0x0002, */
/*  UCMODE1         = 0x0004, */
/*  UCMST           = 0x0008, */
  UCMM            = 0x0020,
  UCSLA10         = 0x0040,
  UCA10           = 0x0080
};


__no_init volatile union
{
  unsigned char UCB0CTL1;   /*  */

  struct
  {
    unsigned char UCSWRST         : 1; /* USCI Software Reset */
    unsigned char UCTXSTT         : 1; /* Transmit START */
    unsigned char UCTXSTP         : 1; /* Transmit STOP */
    unsigned char UCTXNACK        : 1; /* Transmit NACK */
    unsigned char UCTR            : 1; /* Transmit/Receive Select/Flag */
    unsigned char                : 1;
    unsigned char UCSSEL0         : 1; /* USCI 0 Clock Source Select 0 */
    unsigned char UCSSEL1         : 1; /* USCI 0 Clock Source Select 1 */
  }UCB0CTL1_bit;
} @0x05E0;


enum {
/*  UCSWRST         = 0x0001, */
  UCTXSTT         = 0x0002,
  UCTXSTP         = 0x0004,
  UCTXNACK        = 0x0008,
  UCTR            = 0x0010
/*  UCSSEL0         = 0x0040, */
/*  UCSSEL1         = 0x0080, */
};


  /* USCI B0 Baud Word Rate 0 */
__no_init volatile unsigned short UCB0BRW @ 0x05E6;


  /*  */
__no_init volatile unsigned char UCB0BR0 @ 0x05E6;


  /*  */
__no_init volatile unsigned char UCB0BR1 @ 0x05E7;


__no_init volatile union
{
  unsigned char UCB0STAT;   /* USCI B0 Status Register */

  struct
  {
    unsigned char                : 4;
    unsigned char UCBBUSY         : 1; /* Bus Busy Flag */
    unsigned char UCGC            : 1; /* General Call address received Flag */
    unsigned char UCSCLLOW        : 1; /* SCL low */
    unsigned char UCLISTEN        : 1; /* USCI Listen mode */
  }UCB0STAT_bit;
} @0x05EA;


enum {
  UCBBUSY         = 0x0010,
  UCGC            = 0x0020,
  UCSCLLOW        = 0x0040
/*  UCLISTEN        = 0x0080, */
};


  /* USCI B0 Receive Buffer */
__no_init volatile unsigned __READ char UCB0RXBUF @ 0x05EC;


  /* USCI B0 Transmit Buffer */
__no_init volatile unsigned char UCB0TXBUF @ 0x05EE;


__no_init volatile union
{
  unsigned short UCB0I2COA;   /* USCI B0 I2C Own Address */

  struct
  {
    unsigned short UCOA0           : 1; /* I2C Own Address 0 */
    unsigned short UCOA1           : 1; /* I2C Own Address 1 */
    unsigned short UCOA2           : 1; /* I2C Own Address 2 */
    unsigned short UCOA3           : 1; /* I2C Own Address 3 */
    unsigned short UCOA4           : 1; /* I2C Own Address 4 */
    unsigned short UCOA5           : 1; /* I2C Own Address 5 */
    unsigned short UCOA6           : 1; /* I2C Own Address 6 */
    unsigned short UCOA7           : 1; /* I2C Own Address 7 */
    unsigned short UCOA8           : 1; /* I2C Own Address 8 */
    unsigned short UCOA9           : 1; /* I2C Own Address 9 */
    unsigned short                : 5;
    unsigned short UCGCEN          : 1; /* I2C General Call enable */
  }UCB0I2COA_bit;
} @0x05F0;


enum {
  UCOA0           = 0x0001,
  UCOA1           = 0x0002,
  UCOA2           = 0x0004,
  UCOA3           = 0x0008,
  UCOA4           = 0x0010,
  UCOA5           = 0x0020,
  UCOA6           = 0x0040,
  UCOA7           = 0x0080,
  UCOA8           = 0x0100,
  UCOA9           = 0x0200,
  UCGCEN          = 0x8000
};


__no_init volatile union
{
  unsigned short UCB0I2CSA;   /* USCI B0 I2C Slave Address */

  struct
  {
    unsigned short UCSA0           : 1; /* I2C Slave Address 0 */
    unsigned short UCSA1           : 1; /* I2C Slave Address 1 */
    unsigned short UCSA2           : 1; /* I2C Slave Address 2 */
    unsigned short UCSA3           : 1; /* I2C Slave Address 3 */
    unsigned short UCSA4           : 1; /* I2C Slave Address 4 */
    unsigned short UCSA5           : 1; /* I2C Slave Address 5 */
    unsigned short UCSA6           : 1; /* I2C Slave Address 6 */
    unsigned short UCSA7           : 1; /* I2C Slave Address 7 */
    unsigned short UCSA8           : 1; /* I2C Slave Address 8 */
    unsigned short UCSA9           : 1; /* I2C Slave Address 9 */
  }UCB0I2CSA_bit;
} @ 0x05F2;


enum {
  UCSA0           = 0x0001,
  UCSA1           = 0x0002,
  UCSA2           = 0x0004,
  UCSA3           = 0x0008,
  UCSA4           = 0x0010,
  UCSA5           = 0x0020,
  UCSA6           = 0x0040,
  UCSA7           = 0x0080,
  UCSA8           = 0x0100,
  UCSA9           = 0x0200
};


#define __MSP430_HAS_USCI_B0__        /* Definition to show that Module is available */

#define UCB0CTL1            UCB0CTLW0_L  /* USCI B0 Control Register 1 */
#define UCB0CTL0            UCB0CTLW0_H  /* USCI B0 Control Register 0 */
#define UCB0BR0             UCB0BRW_L /* USCI B0 Baud Rate 0 */
#define UCB0BR1             UCB0BRW_H /* USCI B0 Baud Rate 1 */
#define UCB0ICTL_           (0x05FCu)  /* USCI B0 Interrupt Enable Register */
#define UCB0IE              UCB0ICTL_L  /* USCI B0 Interrupt Enable Register */
#define UCB0IFG             UCB0ICTL_H  /* USCI B0 Interrupt Flags Register */
#define UCB0IV_             (0x05FEu)  /* USCI B0 Interrupt Vector Register */
#define UCMODE_0            (0x00)    /* Sync. Mode: USCI Mode: 0 */
#define UCMODE_1            (0x02)    /* Sync. Mode: USCI Mode: 1 */
#define UCMODE_2            (0x04)    /* Sync. Mode: USCI Mode: 2 */
#define UCMODE_3            (0x06)    /* Sync. Mode: USCI Mode: 3 */
#define UCSSEL_0            (0x00)    /* USCI 0 Clock Source: 0 */
#define UCSSEL_1            (0x40)    /* USCI 0 Clock Source: 1 */
#define UCSSEL_2            (0x80)    /* USCI 0 Clock Source: 2 */
#define UCSSEL_3            (0xC0)    /* USCI 0 Clock Source: 3 */
#define UCSSEL__UCLK        (0x00)    /* USCI 0 Clock Source: UCLK */
#define UCSSEL__ACLK        (0x40)    /* USCI 0 Clock Source: ACLK */
#define UCSSEL__SMCLK       (0x80)    /* USCI 0 Clock Source: SMCLK */

#define UCBRF_0             (0x00)    /* USCI First Stage Modulation: 0 */
#define UCBRF_1             (0x10)    /* USCI First Stage Modulation: 1 */
#define UCBRF_2             (0x20)    /* USCI First Stage Modulation: 2 */
#define UCBRF_3             (0x30)    /* USCI First Stage Modulation: 3 */
#define UCBRF_4             (0x40)    /* USCI First Stage Modulation: 4 */
#define UCBRF_5             (0x50)    /* USCI First Stage Modulation: 5 */
#define UCBRF_6             (0x60)    /* USCI First Stage Modulation: 6 */
#define UCBRF_7             (0x70)    /* USCI First Stage Modulation: 7 */
#define UCBRF_8             (0x80)    /* USCI First Stage Modulation: 8 */
#define UCBRF_9             (0x90)    /* USCI First Stage Modulation: 9 */
#define UCBRF_10            (0xA0)    /* USCI First Stage Modulation: A */
#define UCBRF_11            (0xB0)    /* USCI First Stage Modulation: B */
#define UCBRF_12            (0xC0)    /* USCI First Stage Modulation: C */
#define UCBRF_13            (0xD0)    /* USCI First Stage Modulation: D */
#define UCBRF_14            (0xE0)    /* USCI First Stage Modulation: E */
#define UCBRF_15            (0xF0)    /* USCI First Stage Modulation: F */

#define UCBRS_0             (0x00)    /* USCI Second Stage Modulation: 0 */
#define UCBRS_1             (0x02)    /* USCI Second Stage Modulation: 1 */
#define UCBRS_2             (0x04)    /* USCI Second Stage Modulation: 2 */
#define UCBRS_3             (0x06)    /* USCI Second Stage Modulation: 3 */
#define UCBRS_4             (0x08)    /* USCI Second Stage Modulation: 4 */
#define UCBRS_5             (0x0A)    /* USCI Second Stage Modulation: 5 */
#define UCBRS_6             (0x0C)    /* USCI Second Stage Modulation: 6 */
#define UCBRS_7             (0x0E)    /* USCI Second Stage Modulation: 7 */
#define UCIDLE              (0x02)    /* USCI Idle line detected Flag */

/* UCBxI2COA Control Bits */
#define UCOA7_L             (0x0080u)  /* I2C Own Address 7 */
#define UCOA6_L             (0x0040u)  /* I2C Own Address 6 */
#define UCOA5_L             (0x0020u)  /* I2C Own Address 5 */
#define UCOA4_L             (0x0010u)  /* I2C Own Address 4 */
#define UCOA3_L             (0x0008u)  /* I2C Own Address 3 */
#define UCOA2_L             (0x0004u)  /* I2C Own Address 2 */
#define UCOA1_L             (0x0002u)  /* I2C Own Address 1 */
#define UCOA0_L             (0x0001u)  /* I2C Own Address 0 */

/* UCBxI2COA Control Bits */
#define UCGCEN_H            (0x0080u)  /* I2C General Call enable */
#define UCOA9_H             (0x0002u)  /* I2C Own Address 9 */
#define UCOA8_H             (0x0001u)  /* I2C Own Address 8 */

/* UCBxI2CSA Control Bits */
#define UCSA7_L             (0x0080u)  /* I2C Slave Address 7 */
#define UCSA6_L             (0x0040u)  /* I2C Slave Address 6 */
#define UCSA5_L             (0x0020u)  /* I2C Slave Address 5 */
#define UCSA4_L             (0x0010u)  /* I2C Slave Address 4 */
#define UCSA3_L             (0x0008u)  /* I2C Slave Address 3 */
#define UCSA2_L             (0x0004u)  /* I2C Slave Address 2 */
#define UCSA1_L             (0x0002u)  /* I2C Slave Address 1 */
#define UCSA0_L             (0x0001u)  /* I2C Slave Address 0 */

/* UCBxI2CSA Control Bits */
#define UCSA9_H             (0x0002u)  /* I2C Slave Address 9 */
#define UCSA8_H             (0x0001u)  /* I2C Slave Address 8 */

/* USCI Definitions */
#define USCI_NONE           (0x0000u)    /* No Interrupt pending */
#define USCI_UCRXIFG        (0x0002u)    /* USCI UCRXIFG */
#define USCI_UCTXIFG        (0x0004u)    /* USCI UCTXIFG */
#define USCI_I2C_UCALIFG    (0x0002u)    /* USCI I2C Mode: UCALIFG */
#define USCI_I2C_UCNACKIFG  (0x0004u)    /* USCI I2C Mode: UCNACKIFG */
#define USCI_I2C_UCSTTIFG   (0x0006u)    /* USCI I2C Mode: UCSTTIFG*/
#define USCI_I2C_UCSTPIFG   (0x0008u)    /* USCI I2C Mode: UCSTPIFG*/
#define USCI_I2C_UCRXIFG    (0x000Au)    /* USCI I2C Mode: UCRXIFG */
#define USCI_I2C_UCTXIFG    (0x000Cu)    /* USCI I2C Mode: UCTXIFG */

/*-------------------------------------------------------------------------
 *   Watchdog Timer
 *-------------------------------------------------------------------------*/


__no_init volatile union
{
  unsigned short WDTCTL;   /* Watchdog Timer Control */

  struct
  {
    unsigned short WDTIS0          : 1; /* WDT - Timer Interval Select 0 */
    unsigned short WDTIS1          : 1; /* WDT - Timer Interval Select 1 */
    unsigned short WDTIS2          : 1; /* WDT - Timer Interval Select 2 */
    unsigned short WDTCNTCL        : 1; /* WDT - Timer Clear */
    unsigned short WDTTMSEL        : 1; /* WDT - Timer Mode Select */
    unsigned short WDTSSEL0        : 1; /* WDT - Timer Clock Source Select 0 */
    unsigned short WDTSSEL1        : 1; /* WDT - Timer Clock Source Select 1 */
    unsigned short WDTHOLD         : 1; /* WDT - Timer hold */
  }WDTCTL_bit;
} @ 0x015C;


enum {
  WDTIS0          = 0x0001,
  WDTIS1          = 0x0002,
  WDTIS2          = 0x0004,
  WDTCNTCL        = 0x0008,
  WDTTMSEL        = 0x0010,
  WDTSSEL0        = 0x0020,
  WDTSSEL1        = 0x0040,
  WDTHOLD         = 0x0080
};


#define __MSP430_HAS_WDT_A__          /* Definition to show that Module is available */


/* WDTCTL Control Bits */
#define WDTIS0_L            (0x0001u)  /* WDT - Timer Interval Select 0 */
#define WDTIS1_L            (0x0002u)  /* WDT - Timer Interval Select 1 */
#define WDTIS2_L            (0x0004u)  /* WDT - Timer Interval Select 2 */
#define WDTCNTCL_L          (0x0008u)  /* WDT - Timer Clear */
#define WDTTMSEL_L          (0x0010u)  /* WDT - Timer Mode Select */
#define WDTSSEL0_L          (0x0020u)  /* WDT - Timer Clock Source Select 0 */
#define WDTSSEL1_L          (0x0040u)  /* WDT - Timer Clock Source Select 1 */
#define WDTHOLD_L           (0x0080u)  /* WDT - Timer hold */

#define WDTPW               (0x5A00u)

#define WDTIS_0           (0*0x0001u)  /* WDT - Timer Interval Select: /2G */
#define WDTIS_1           (1*0x0001u)  /* WDT - Timer Interval Select: /128M */
#define WDTIS_2           (2*0x0001u)  /* WDT - Timer Interval Select: /8192k */
#define WDTIS_3           (3*0x0001u)  /* WDT - Timer Interval Select: /512k */
#define WDTIS_4           (4*0x0001u)  /* WDT - Timer Interval Select: /32k */
#define WDTIS_5           (5*0x0001u)  /* WDT - Timer Interval Select: /8192 */
#define WDTIS_6           (6*0x0001u)  /* WDT - Timer Interval Select: /512 */
#define WDTIS_7           (7*0x0001u)  /* WDT - Timer Interval Select: /64 */
#define WDTIS__2G         (0*0x0001u)  /* WDT - Timer Interval Select: /2G */
#define WDTIS__128M       (1*0x0001u)  /* WDT - Timer Interval Select: /128M */
#define WDTIS__8192K      (2*0x0001u)  /* WDT - Timer Interval Select: /8192k */
#define WDTIS__512K       (3*0x0001u)  /* WDT - Timer Interval Select: /512k */
#define WDTIS__32K        (4*0x0001u)  /* WDT - Timer Interval Select: /32k */
#define WDTIS__8192       (5*0x0001u)  /* WDT - Timer Interval Select: /8192 */
#define WDTIS__512        (6*0x0001u)  /* WDT - Timer Interval Select: /512 */
#define WDTIS__64         (7*0x0001u)  /* WDT - Timer Interval Select: /64 */

#define WDTSSEL_0         (0*0x0020u)  /* WDT - Timer Clock Source Select: SMCLK */
#define WDTSSEL_1         (1*0x0020u)  /* WDT - Timer Clock Source Select: ACLK */
#define WDTSSEL_2         (2*0x0020u)  /* WDT - Timer Clock Source Select: VLO_CLK */
#define WDTSSEL_3         (3*0x0020u)  /* WDT - Timer Clock Source Select: reserved */
#define WDTSSEL__SMCLK    (0*0x0020u)  /* WDT - Timer Clock Source Select: SMCLK */
#define WDTSSEL__ACLK     (1*0x0020u)  /* WDT - Timer Clock Source Select: ACLK */
#define WDTSSEL__VLO      (2*0x0020u)  /* WDT - Timer Clock Source Select: VLO_CLK */

/* WDT is clocked by fSMCLK (assumed 1MHz) */
#define WDT_MDLY_32         (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2)                         /* 32ms interval (default) */
#define WDT_MDLY_8          (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTIS0)                  /* 8ms     " */
#define WDT_MDLY_0_5        (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTIS1)                  /* 0.5ms   " */
#define WDT_MDLY_0_064      (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTIS1+WDTIS0)           /* 0.064ms " */

/* WDT is clocked by fACLK (assumed 32KHz) */
#define WDT_ADLY_1000       (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTSSEL0)                /* 1000ms  " */
#define WDT_ADLY_250        (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTSSEL0+WDTIS0)         /* 250ms   " */
#define WDT_ADLY_16         (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTSSEL0+WDTIS1)         /* 16ms    " */
#define WDT_ADLY_1_9        (WDTPW+WDTTMSEL+WDTCNTCL+WDTIS2+WDTSSEL0+WDTIS1+WDTIS0)  /* 1.9ms   " */

/* WDT is clocked by fSMCLK (assumed 1MHz) */
#define WDT_MRST_32         (WDTPW+WDTCNTCL+WDTIS2)                                  /* 32ms interval (default) */
#define WDT_MRST_8          (WDTPW+WDTCNTCL+WDTIS2+WDTIS0)                           /* 8ms     " */
#define WDT_MRST_0_5        (WDTPW+WDTCNTCL+WDTIS2+WDTIS1)                           /* 0.5ms   " */
#define WDT_MRST_0_064      (WDTPW+WDTCNTCL+WDTIS2+WDTIS1+WDTIS0)                    /* 0.064ms " */

/* WDT is clocked by fACLK (assumed 32KHz) */
#define WDT_ARST_1000       (WDTPW+WDTCNTCL+WDTSSEL0+WDTIS2)                         /* 1000ms  " */
#define WDT_ARST_250        (WDTPW+WDTCNTCL+WDTSSEL0+WDTIS2+WDTIS0)                  /* 250ms   " */
#define WDT_ARST_16         (WDTPW+WDTCNTCL+WDTSSEL0+WDTIS2+WDTIS1)                  /* 16ms    " */
#define WDT_ARST_1_9        (WDTPW+WDTCNTCL+WDTSSEL0+WDTIS2+WDTIS1+WDTIS0)           /* 1.9ms   " */




#pragma language=restore
#endif  /* __IAR_SYSTEMS_ICC__  */


/************************************************************
* Timer A interrupt vector value
************************************************************/

/* Compability definitions */

#define TAIV_CCIFG1         TAIV_TACCR1       /* Capture/compare 1 */
#define TAIV_CCIFG2         TAIV_TACCR2       /* Capture/compare 2 */
#define TAIV_CCIFG3         TAIV_6            /* Capture/compare 3 */
#define TAIV_CCIFG4         TAIV_8            /* Capture/compare 4 */

/************************************************************
* Interrupt Vectors (offset from 0xFF80)
************************************************************/

#define PORT2_VECTOR        (45 * 2u) /* 0xFFDA Port 2 */
#define PORT1_VECTOR        (46 * 2u) /* 0xFFDC Port 1 */
#define TIMER1_D1_VECTOR    (47 * 2u) /* 0xFFDE Timer1_D3 CC1-2, TA1 */
#define TIMER1_D0_VECTOR    (48 * 2u) /* 0xFFE0 Timer1_D3 CC0 */
#define TEC1_VECTOR         (49 * 2u) /* 0xFFF2 Timer Event Controller 1 */
#define DMA_VECTOR          (50 * 2u) /* 0xFFE4 DMA */
#define TIMER0_A1_VECTOR    (51 * 2u) /* 0xFFE6 Timer0_A3 CC1-2, TA */
#define TIMER0_A0_VECTOR    (52 * 2u) /* 0xFFE8 Timer0_A3 CC0 */
#define ADC10_VECTOR        (53 * 2u) /* 0xFFEA ADC */
#define USCI_B0_VECTOR      (54 * 2u) /* 0xFFEC USCI B0 Receive/Transmit */
#define USCI_A0_VECTOR      (55 * 2u) /* 0xFFEE USCI A0 Receive/Transmit */
#define WDT_VECTOR          (56 * 2u) /* 0xFFF0 Watchdog Timer */
#define TIMER0_D1_VECTOR    (57 * 2u) /* 0xFFE2 Timer0_D3 CC1-2, TA */
#define TIMER0_D0_VECTOR    (58 * 2u) /* 0xFFE4 Timer0_D3 CC0 */
#define TEC0_VECTOR         (59 * 2u) /* 0xFFF6 Timer Event Controller 0 */
#define COMP_B_VECTOR       (60 * 2u) /* 0xFFF8 Watchdog Timer */
#define UNMI_VECTOR         (61 * 2u) /* 0xFFFA User Non-maskable */
#define SYSNMI_VECTOR       (62 * 2u) /* 0xFFFC System Non-maskable */
#define RESET_VECTOR        (63 * 2u) /* 0xFFFE Reset [Highest Priority] */


#endif /* __IO430xxxx */



