
#include "io430F5152.h"
#include "hi_msp430.h"
#include "io430.h"
#include "intrinsics.h"
//#include "msp430x5xx.h"


u8 keyCnt = 0;      //记录按下次数
u8 key_value = KEY_NONE;   //有效的按键标志
u8 key_down  = KEY_NONE;    //按下的按键

#define KEY_TIME 40
u8 key_time = KEY_TIME;

/*
* LED funtion:  
* 硬件连接：red-->p34       green-->p35
*/
static inline void ith_led_init(void)
{
    P3DIR |= (BIT4 | BIT5);
    P3OUT &= ~(BIT4 | BIT5);
}

void inline ith_Rled_on(void)
{
    P3OUT |= BIT4;
}
void inline ith_Rled_off(void)
{
    P3OUT &= ~BIT4;
}

void inline ith_Gled_on(void)
{
    P3OUT |= BIT5;
}

void inline ith_Gled_off(void)
{
    P3OUT &= ~BIT5;
}

/*
* KEY funtion:  
* 硬件连接：s3-->p22 ,s4-->p24 ,s2-->p26
*/
void ith_key_init(void)
{
    P2DIR &= ~BIT2; //输入模式
    //P2OUT |=  BIT2;
    #if 1
    P2REN |=  BIT2; //内部上拉
    P2IES |=  BIT2;  //下降沿触发中断
    P2IFG &= ~BIT2;
    P2IE  |=  BIT2;  //使能中断
    __bis_SR_register(GIE);
    __no_operation();

    //_EINT();         //开总中断
    //_enable_interrupts();
    
    #endif
}

/*
*   向中断向量表注册port1函数
*   硬件连接：s3-->p22 ,s4-->p24 ,s2-->p26
*/
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    ith_Gled_off();
    if((P2IFG & BIT2) == BIT2)
    {
        delay(5);
        if((P2IFG & BIT2) == BIT2)
        {
            ith_Rled_on();
            P2IFG &= ~BIT2;
        }
        
    }
}

#if 0
u8 ith_get_KeyVale(void)
{
    u8 ret_vaule = 0;

    if((P2IN&BIT2) == 0)
    {
        delay(5);
        if((P2IN&BIT2) == 0)
        {
            ith_Rled_on();
            ret_vaule = 1;
        }
    }
    return ret_vaule;
}
#endif

int ith_gpio_init(void)
{
    ith_led_init();
    ith_key_init();
    return 0;
}

/*---------time-定时器---------*/
/*
* time funtion:  
* 
*/
#if 0
void ith_time_init(void)
{
    TACTL  = TASSEL1 + TACLR + MC_0; //选择时钟源：ACLK = 32.768k
    TACCR0 = TIME_20MS;     //设置定时时间
    TACCR0 = CCIE;          //允许定时器中断
}
#endif 
/*
* 1-->enable,  0-->disable
*/
#if 0
void ith_go_time(u8 t_flag)
{
    if(t_flag == ENABLE)
    {
        TACTL |= MC_1 + TACLR;      //开定时器，向上计数
    }
    else if(t_flag == DISABLE)
    {
        TACTL &= ~MC0;          //关定时器
    }
}
#endif

/*
* delay funtion:  
* 
*/
void delay(u32 time)
{
    int i,j;

    for(i = 1000; i>0; i--)
    {
        for(j = time; j>0; j--)
            ;//nothing
    }
    
}


