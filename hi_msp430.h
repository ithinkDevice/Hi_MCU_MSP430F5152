
#ifndef _HI_MSP430_H
#define _HI_MSP430_H


/* Definition of types */
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef signed int s32;

//key value
typedef enum{
    KEY_NONE    = 0,
    KEY_S2,     /*p26*/
    KEY_S3,     /*p22*/ 
    KEY_S4,     /*p24*/
}KEY_VALUE_T;
    
#define TIME_20MS 655 //��˵20ms
#define ENABLE      1
#define DISABLE     0

//extern 

/*-----------LED------------*/
int  ith_gpio_init(void);
void ith_Gled_off(void);
void ith_Gled_on(void);
void ith_Rled_off(void);
void ith_Rled_on(void);
//void ith_led_trigger(void);
/*-----------delay-------------*/
void delay(u32 time);

/*=========key==============*/
u8 ith_get_KeyVale(void);
void ith_go_keyIn(u8 k_flag);


/*---------time-��ʱ��---------*/
void ith_time_init(void);
void ith_go_time(u8 t_flag);


#endif

